//
//  JSONParserTest.swift
//  SmokingOff
//
//  Created by DfStudio on 29.04.2021.
//

import Foundation

class JSONParserManager {

    
    func fetchDataJsonTest(file: String, completion: @escaping (Tests) -> ()) {
        
        let path = Bundle.main.path(forResource: file, ofType: "json")

        let url = URL(fileURLWithPath: path!)

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }

            guard error == nil else { return }
                        
            do {
                let tests = try JSONDecoder().decode(Tests.self, from: data)

                DispatchQueue.main.async {
                    completion(tests)
                }
            } catch {
                print(error)
            }
        }.resume()
        return
    }
    
    func fetchDataJsonWeek(file: String, completion: @escaping (Weeks) -> ()) {
        
        let path = Bundle.main.path(forResource: file, ofType: "json")
//        let path = Bundle.main.path(forResource: "weeksEn", ofType: "json")
        let url = URL(fileURLWithPath: path!)

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }

            guard error == nil else { return }
                        
            do {
                let weeks = try JSONDecoder().decode(Weeks.self, from: data)

                DispatchQueue.main.async {
                    completion(weeks)
                }
            } catch {
                print(error)
            }
        }.resume()
        return
    }
    
}
