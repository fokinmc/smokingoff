//
//  GreetingViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 05.04.2021.
//

import UIKit

class GreetingViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    let useDef = UserDefManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Appearance.navigationDecor(vc: self)

        let name = useDef.getName()
        let history = useDef.getHistory()
        
        nameLabel.text = name
        if useDef.getStage() == 1 || useDef.getStage() == 2 {
            greetingLabel.text = NSLocalizedString("You have been smoking for \(history) and our course will help you to quit smoking very soon.", comment: "")
        } else if useDef.getStage() == 3 {
            greetingLabel.text = NSLocalizedString("You have already quit smoking and we will help you to completely give up cigarettes forever.", comment: "")
            nextButton.setTitle("Okay!", for: .normal)
        }

    }
    

    @IBAction func nextAction(_ sender: Any) {
        guard let nextVC = storyboard?.instantiateViewController(identifier: "qouteVC") else { return }
//        present(nextVC, animated: true, completion: nil)
        navigationController?.pushViewController(nextVC, animated: true)

    }
    

}
