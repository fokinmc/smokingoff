//
//  SetTriggerView.swift
//  SmokingOff
//
//  Created by DfStudio on 19.06.2021.
//

import UIKit

class SetTriggerView: UIView {

//    @IBOutlet weak var title: UILabel!
//    @IBOutlet weak var textLabel: UILabel!

    var timerAnime: Timer!
    var namesButton = ["RoundButton0", "RoundButton1", "RoundButton2", "RoundButton3", "RoundButton4", "RoundButton5", "RoundButton6", "RoundButton7", "RoundButton8", "RoundButton9", "RoundButton12", "RoundButton10", "RoundButton11",  "RoundButton12"]
    
//    var tapButton: (() -> ())?
    var button = UIButton()
    let size = CGSize(width: 60, height: 60)

    
//    var titleText: String {
//        get {
//            return title.text!
//        }
//        set(titleLabelText) {
//            title.text = titleText
//        }
//    }
    
    var view: UIView!
    var nibName: String = "SetTriggerView"
    
    var toX: CGFloat!
    var toY: CGFloat!
    var c: Int = 0
    var i: CGFloat = 0
    var j: CGFloat = 0

     
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

    func setup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 10
        view.clipsToBounds = false
//        shadow(view: view)
        addSubview(view)
    }
    

    
    func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.1
            view.layer.shadowOffset = CGSize(width: 0, height: 6)
            view.layer.shadowRadius = 8
    }
    
     
    func createButton() {
//        namesButton = namesButton.shuffled()
        let x: CGFloat = view.frame.width / 2 - 30
        let y: CGFloat = view.frame.height / 2 - 45

        let point = CGPoint(x: x, y: y)
        button = UIButton(frame: CGRect(origin: point, size: size))
        button.setImage(UIImage(named: namesButton[Int(c)]), for: .normal)
        self.view.addSubview(button)
        
        let multiple = view.frame.width / 4.1410
        toX = -view.frame.width  / 2 + 45 + multiple * i
        toY = -view.frame.height / 2 + 60
        i += 1
        c += 1

        if j > 0 {
            toY = -view.frame.height  / 2 + 60 + multiple * j
        }
        
        if i > 3 {
            i = 0
            j += 1
            if j == 3 { i += 1 }
        }

        
        
        UIView.animate(withDuration: 0) { [self] in
            button.transform = CGAffineTransform(scaleX: 0, y: 0)
        } completion: {_ in
            UIView.animate(withDuration: 2) { [self] in
                button.transform = CGAffineTransform(scaleX: 2, y: 2)
            } completion: {_ in
                UIView.animate(withDuration: 1) { [self] in
                    button.transform = CGAffineTransform(a: 1, b: 0.0, c: 0.0, d: 1, tx: toX, ty: toY)
                } completion: {_ in

                }
            }
        }
    }
    
    //actions
    
}
