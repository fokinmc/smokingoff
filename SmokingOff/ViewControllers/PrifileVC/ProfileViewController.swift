//
//  ProfileViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 21.04.2021.
//

import UIKit
import MessageUI
import MessageUI

@IBDesignable

class ProfileViewController: UIViewController, MFMailComposeViewControllerDelegate {
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    let systemVersion = UIDevice.current.systemVersion
    let modelName = UIDevice.modelName
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var smokingOffDate: UITextField!
    @IBOutlet weak var nullifyButton: UIButton!
    @IBOutlet weak var scrollVeiw: UIScrollView!
    
    
    let useDef = UserDefManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupContent()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupContent()
        setupInterface()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        guard let name = nameTextField.text else { return }
        useDef.saveName(name: name)
        
        guard let email = emailTextField.text else { return }
        useDef.saveEmail(email: email)
        
        guard let smokingOff = smokingOffDate.text else { return }
        useDef.saveSmokingOff(smokingOff: smokingOff)
        
    }
    
    func setupContent() {
        Appearance.navigationDecor(vc: self)
        title = "Profile"
        nameTextField.text = useDef.getName()
        emailTextField.text = useDef.getEmail()
        smokingOffDate.text = useDef.getSmokingOff()
        
        guard let url = useDef.getImageUrl() else { return }
        profileImage.image = useDef.getImage(url: url)
    }
    
    func setupInterface() {
        tabBarController?.tabBar.isHidden = false
        scrollVeiw.alwaysBounceHorizontal = false
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = #colorLiteral(red: 0.0978866443, green: 0.7687712312, blue: 0.4901840091, alpha: 1)
        profileImage.clipsToBounds = true
        
        nullifyButton.layer.cornerRadius = nullifyButton.frame.height / 2
        
    }
    
    func sendMail(theme: String) {
        let appVer = appVersion!
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@________.com"])
            mail.setSubject("SmokeOff (\(appVer), iOS \(systemVersion), \(modelName))")
            mail.setMessageBody(theme, isHTML: false)
            present(mail, animated: true)
        } else {
            print("fail")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        title = ""

        if let email = emailTextField.text {
            useDef.saveEmail(email: email)
        }
        if let name = nameTextField.text  {
            useDef.saveName(name: name)
        }
        
        if segue.identifier == "toSettings" {
            let vc = segue.destination as! SettingsViewController
            vc.appVersion = appVersion
        }
        
    }
    
    //action
    @IBAction func resetAction(_ sender: Any) {
        useDef.saveSmokingOff(smokingOff: "")
        smokingOffDate.text = ""
    }
    
    @IBAction func restorePurchases(_ sender: Any) {
    }
    
    @IBAction func fbAction(_ sender: Any) {
        let url = URL(string: "fb://fokinmc")!  //вроде рабочая
        UIApplication.shared.open(url)
    }
    
    @IBAction func instaAction(_ sender: Any) {
        let url = URL(string: "https://instagram.com/artizumina?igshid=m9ximnrnsbb2")!
        UIApplication.shared.open(url)
    }
    
    
    @IBAction func twitterAction(_ sender: Any) {
        let url = URL(string: "twitter://user?id=dr6566728")!
        //https://stackoverflow.com/questions/10424275/how-can-i-open-a-twitter-tweet-using-the-native-twitter-app-on-ios/10424346#10424346
        UIApplication.shared.open(url)
    }
    
    @IBAction func supportAction(_ sender: Any) {
        sendMail(theme: "support")
    }
    
    @IBAction func sendMailAction(_ sender: Any) {
        sendMail(theme: "просто письмо")
    }
    
    @IBAction func offerAction(_ sender: Any) {
        sendMail(theme: "I have offer")
    }
    
    
}


