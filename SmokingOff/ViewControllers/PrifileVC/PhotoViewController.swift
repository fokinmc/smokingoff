//
//  PhotoViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 21.04.2021.
//

import UIKit

class PhotoViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    let useDef = UserDefManager.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupImageView()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        saveImage()

        
    }
    
    func setupImageView() {
        imageView.layer.cornerRadius = 74
        imageView.clipsToBounds = true
        // Fetch Image Data
        guard let url = useDef.getImageUrl() else { return }
        imageView.image = useDef.getImage(url: url)
    }

   
   
//   @IBAction func saveButtonPressed(_ sender: UIButton) {
//       if nameImageText.text == "Enter the name for background" {
//
//           let ac = UIAlertController(title: "Add name!", message: "Please entet name for new background", preferredStyle: .actionSheet)
//           let cancel = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
//           ac.addAction(cancel)
//           present(ac, animated: true, completion: nil)
//       } else {
//
//           // добираемся до контекста для работы с Core Data (AppDelegate - изменили - добавили еще строки)
//           if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext {
//
//               let background = Background(context: context) // имя
//               background.name = nameImageText.text
//               if let image = imageView.image {
//                   background.image = UIImagePNGRepresentation(image) as NSData? // картинка
//               } // в итоге Экземпляр BackGround сохранён в coreData
//
//               do {
//                   try context.save()
//                   print("Экземпляр BackGround сохранён в coreData")
//
//               } catch let error as NSError {
//                   print("Не удалось сохранить данные \(error), \(error.userInfo) ")
//               }
//           }
//           saveButtonLabel.isHidden = true
//           nameImageText.isHidden = true
//           addPhotoLabel.isHidden = true
//       }
//   }

   
   @IBAction func pressedTakePhoto(_ sender: UIButton) {
    showImageMenu()
   }
    

}

// take photo
extension PhotoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showImageMenu() {
        let ac = UIAlertController(title: "Add photo", message: "You can take or choose the photo", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.chooseImagePickerAction(source: .camera)
        })
        let photoAction = UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            self.chooseImagePickerAction(source: .photoLibrary)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        ac.addAction(cameraAction)
        ac.addAction(photoAction)
        ac.addAction(cancel)
        present(ac, animated: true, completion: nil)
    }
    
    func chooseImagePickerAction(source: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true //масштабирование и обрезание
//            imagePicker.cameraCaptureMode = UIImagePickerController.CameraCaptureMode(rawValue: 0)!
            imagePicker.sourceType = source
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
        
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            imageView.image = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = originalImage
        }
        dismiss(animated: true, completion: nil)
    }
}


// save photo
extension PhotoViewController {
    
    func saveImage() {
        if let data = imageView.image?.pngData() {
            
            // Create URL
            let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let url = documents.appendingPathComponent("progileImage.png")
            
            do {
                // Write to Disk
                try data.write(to: url)
                
                // Store URL in User Defaults
                useDef.saveImage(url: url)
                UserDefaults.standard.set(url, forKey: "background")
                
            } catch {
                print("Unable to Write Data to Disk (\(error))")
            }
        }
    }
}
