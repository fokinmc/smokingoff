//
//  Mult.swift
//  SmokingOff
//
//  Created by DfStudio on 10.05.2021.
//

import Foundation
import Lottie

class Mult {
//    static let shared = Mult()
    
    let animationView = AnimationView()
    var name: String
    func startAnime(addTo view: UIView, mode: LottieLoopMode, isCenter: Bool) {
        
//        let name = nameAnime
        let animation = Animation.named(name)

        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
//        animationView.contentMode = .scaleAspectFill
//        animationView.contentMode = .scaleToFill
        
        
        view.addSubview(animationView)
        animationView.backgroundColor = .clear
        
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.translatesAutoresizingMaskIntoConstraints = false
        
        // расположение по центру
        if isCenter { 
            animationView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
            animationView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        } else {
            // на всю view
            animationView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            animationView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            animationView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            //        animationView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: correctCenter).isActive = true
            animationView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        animationView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
        
        animationView.play(fromProgress: 0,
                           toProgress: 1,
                           loopMode: mode,
                           completion: { (finished) in
                            if finished {
                                self.animationView.removeFromSuperview()
                                NotificationCenter.default.post(name: NSNotification.Name("animFinish"), object: nil)
                                print("Animation Complete end remove from superView")
                            } else {
                                NotificationCenter.default.post(name: NSNotification.Name("animCancel"), object: nil)
//                                print("Animation cancelled")
                            }
        })
        
    }
    
    init(name: String) {
        self.name = name
    }
    
//    private init() {
//    }
}
