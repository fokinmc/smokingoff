//
//  LessonDescriptionViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 17.06.2021.
//

import UIKit

class LessonDescriptionView: UIView {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var tapButton: (() -> ())?

    
    var view: UIView!
    var nibName: String = "LessonDescriptionView"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }


    func setup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 10
        view.clipsToBounds = false
//        shadow(view: view)
        
        addSubview(view)
    }
    
    func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.1
            view.layer.shadowOffset = CGSize(width: 0, height: 6)
            view.layer.shadowRadius = 8
    }
    
    func disableButton() {
        nextButton.setBackgroundImage(UIImage(named: "buttonCrey"), for: .normal)
        nextButton.isEnabled = false
        nextButton.setTitle("ВЫБРАЛ", for: .normal)
    }
    func enableButton() {
        nextButton.setBackgroundImage(UIImage(named: "buttonCreen"), for: .normal)
        nextButton.isEnabled = true
        nextButton.setTitle("NEXT", for: .normal)
    }
            
    
    @IBAction func tapNext(_ sender: Any) {
        tapButton?()
    }
}
