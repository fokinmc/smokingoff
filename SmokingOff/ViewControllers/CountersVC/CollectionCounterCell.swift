//
//  collectionCounterCell.swift
//  SmokingOff
//
//  Created by DfStudio on 10.08.2021.
//

import UIKit


class CollectionCounterCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var image: UIImageView!

    @IBOutlet weak var widthSizeBack: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        GlobalFunc.shadow(view: backView)
        
    }
    
    func setupSize(selfWidth: CGFloat) {
        widthSizeBack.constant = selfWidth / 4.8
    }
}
