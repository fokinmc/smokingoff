//
//  tViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 15.05.2021.
//

import UIKit

class ExplanationViewController: UIViewController {

    @IBOutlet weak var blurVeiw: UIView!
    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var qLabel: UILabel!
    
    var titleQ: String!
    var text: String!
    
    weak var delegate: FirstViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: blurVeiw.frame.width + 40, height: blurVeiw.frame.height)
        blurVeiw.addSubview(blurEffectView)
        
        titileLabel.text = titleQ
        qLabel.text = text
    }
    
    deinit {
        delegate?.closeExplanation()
    }


    @IBAction func nextActions(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
