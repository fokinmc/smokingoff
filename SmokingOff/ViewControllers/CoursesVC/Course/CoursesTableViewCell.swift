//
//  CoursesTableViewCell.swift
//  SmokingOff
//
//  Created by DfStudio on 13.04.2021.
//

import UIKit

class CoursesTableViewCell: UITableViewCell {

    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var weekNumber: UILabel!
    @IBOutlet weak var line: UIView!
    
    
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    @IBOutlet weak var greenWidth: NSLayoutConstraint!
    @IBOutlet weak var numberWeekToNameWeek: NSLayoutConstraint!
    
    @IBOutlet weak var menPositionTop: NSLayoutConstraint!
    @IBOutlet weak var animPositionTop: NSLayoutConstraint!
    @IBOutlet weak var weekNumberVertical: NSLayoutConstraint!

    
    @IBOutlet weak var menSize: NSLayoutConstraint!
    @IBOutlet weak var animSize: NSLayoutConstraint!
    
    
    @IBOutlet weak var currentLesson: UILabel!
    @IBOutlet weak var currentWeekNumber: UILabel!
    
    
    @IBOutlet weak var manImage: UIImageView!
    @IBOutlet var animView: UIView!
    @IBOutlet weak var leadinLine: NSLayoutConstraint!
    @IBOutlet weak var weekToInside: NSLayoutConstraint!
    @IBOutlet weak var insideToTop: NSLayoutConstraint!
    
    
    @IBOutlet weak var labelTop: NSLayoutConstraint!
    @IBOutlet weak var labelCenter: NSLayoutConstraint!
    
    //    var manImage: UIImageView!
    
    var mult: Mult! = nil
    var alreadyMove = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.layer.cornerRadius = 18
        contentView.clipsToBounds = false
        
//        manImage = UIImageView(frame: CGRect(
//                                x: contentView.frame.maxX - 88,
//                                y: weekLabel.frame.maxY - 48,
//                                width: 78,
//                                height: 78))
//        manImage.contentMode = .scaleAspectFit
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(status: Int, multName: String, indexPath: IndexPath, progress: CGFloat) {
//        weekLabel.text = Constants.weeks[indexPath.row]
        weekNumber.text = NSLocalizedString("Неделя \(indexPath.row + 1)", comment: "")
        
        manImage.image = UIImage(named: Constants.imgWeeks[indexPath.row])
        shadow(view: backView)
        
        
        
        switch status {
        case 0:
            labelCenter.isActive = false
            manImage.isHidden = false
            animView.isHidden = true
            statusImage.image = #imageLiteral(resourceName: "okGreen")
            backView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//            addSubview(manImage)
            line.backgroundColor = #colorLiteral(red: 0.4542214274, green: 0.8181859851, blue: 0.7386319637, alpha: 1)
            heightOfView.constant = 0
            currentLesson.isHidden = true
            currentWeekNumber.isHidden = true
            greenWidth.constant = 0
            insideView.isHidden = true
            
            weekNumberVertical.constant = 3
            weekToInside.isActive = false

        case 1:
            labelTop.isActive = false
            animView.isHidden = false
            self.layer.zPosition = 10
          
            if !alreadyMove {
                mult = Mult(name: multName)
//                mult = Mult(name: "Hi")
                mult.startAnime(addTo: animView, mode: .loop, isCenter: true)
                alreadyMove = true
            }
            

            menPositionTop.constant = -18
            animPositionTop.constant = -48

            menSize.constant = 18
            animSize.constant = 18
            
            
            insideView.isHidden = false
            weekLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            weekNumber.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7451565976)
            weekNumber.text = NSLocalizedString("Успеваемость", comment: "")
            currentWeekNumber.text = NSLocalizedString("Урок 3: Дофамин", comment: "")
            line.backgroundColor = #colorLiteral(red: 0.9765664303, green: 0.9765664303, blue: 0.9765664303, alpha: 0.4584071684)
            
//            manImage.frame = CGRect(x: contentView.frame.maxX - 108,
//                                    y: weekLabel.frame.maxY - 88,
//                                    width: 118,
//                                    height: 118)
//            animView.frame = CGRect(x: contentView.frame.maxX - 108,
//                                    y: weekLabel.frame.maxY - 88,
//                                    width: 218,
//                                    height: 218)
//            addSubview(manImage)
            leadinLine.constant = 38
            statusImage.image = #imageLiteral(resourceName: "playWhite")
            backView.backgroundColor = #colorLiteral(red: 0.4542214274, green: 0.8181859851, blue: 0.7386319637, alpha: 1)
            currentLesson.isHidden = false
            currentWeekNumber.isHidden = false
            
            greenWidth.constant = line.frame.width * progress
            manImage.isHidden = true
//            animView.frame = manImage.frame



        case 2:
            labelCenter.isActive = false

            manImage.isHidden = false
            animView.isHidden = true
            weekNumberVertical.constant = 3

            statusImage.image = #imageLiteral(resourceName: "lockYellow")
            backView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            line.backgroundColor = #colorLiteral(red: 0.9600024819, green: 0.9368915558, blue: 0.919480145, alpha: 1)

//            addSubview(manImage)
            heightOfView.constant = 0
            currentLesson.isHidden = true
            currentWeekNumber.isHidden = true
            greenWidth.constant = 0
            insideView.isHidden = true
            weekToInside.isActive = false

        default:
            labelCenter.isActive = false

            manImage.isHidden = false
            animView.isHidden = true
            weekNumberVertical.constant = 3

            statusImage.image = #imageLiteral(resourceName: "lockYellow")
            backView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            line.backgroundColor = #colorLiteral(red: 0.9600024819, green: 0.9368915558, blue: 0.919480145, alpha: 1)
            insideView.isHidden = true

//            addSubview(manImage)
            heightOfView.constant = 0
            currentLesson.isHidden = true
            currentWeekNumber.isHidden = true
            greenWidth.constant = 0
            weekToInside.isActive = false

        }
    }
    
    func shadow(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.13
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowRadius = 4.8
    }
}
