//
//  TowCounterVC.swift
//  SmokingOff
//
//  Created by DfStudio on 11.08.2021.
//

import UIKit

class TowCounterVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageHeader: UIImageView!

    var name: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = name
        imageHeader.image = UIImage(named: name)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TowCounterVCCell
        
        return cell
    }


    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
