//
//  TargetViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 02.04.2021.
//

import UIKit
import Toaster

class CurrentStageViewController: UIViewController {

    @IBOutlet weak var fistButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var description1: UILabel!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var description2: UILabel!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var description3: UILabel!


    @IBOutlet weak var ok1: UIImageView!
    @IBOutlet weak var ok2: UIImageView!
    @IBOutlet weak var ok3: UIImageView!

    
    private let useDef = UserDefManager.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Appearance.navigationDecor(vc: self)


    }
    
    func buttonSelect(buttonActive: UIButton, button1: UIButton, button2: UIButton, image: UIImageView, images: [UIImageView], names: [UILabel], descriptions: [UILabel]) {
        
        buttonActive.backgroundColor = #colorLiteral(red: 0.9878367782, green: 0.6144195199, blue: 0.3506464958, alpha: 1)
        images[0].image = UIImage(named: "")
        images[1].image = UIImage(named: "")
        image.image = UIImage(named: "okYellow")
        names[0].textColor = .white
        names[1].textColor = .black
        names[2].textColor = .black

        descriptions[0].textColor = .white
        descriptions[1].textColor = .black
        descriptions[2].textColor = .black

        
        button1.backgroundColor = #colorLiteral(red: 0.9640881419, green: 0.9686412215, blue: 0.9772089124, alpha: 1)
        image.image = UIImage(named: "okYellow")

        button2.backgroundColor = #colorLiteral(red: 0.9640881419, green: 0.9686412215, blue: 0.9772089124, alpha: 1)
    }
    
    @IBAction func firstAction(_ sender: Any) {
        useDef.saveStage(value: 1)
        buttonSelect(buttonActive: fistButton, button1: secondButton, button2: thirdButton, image: ok1, images: [ok2, ok3], names: [name1, name2, name3], descriptions: [description1, description2, description3])
    }
    @IBAction func secondAction(_ sender: Any) {
        useDef.saveStage(value: 2)
        buttonSelect(buttonActive: secondButton, button1: fistButton, button2: thirdButton, image: ok2, images: [ok1, ok3], names: [name2, name3, name1], descriptions: [description2, description1, description3])
    }
    @IBAction func thirdAction(_ sender: Any) {
        useDef.saveStage(value: 3)
        buttonSelect(buttonActive: thirdButton, button1: fistButton, button2: secondButton, image: ok3, images: [ok2, ok1], names: [name3, name2, name1], descriptions: [description3, description2, description1])
    }

    
    
    @IBAction func nextAction(_ sender: Any) {
        if useDef.getStage() != 0 {
            guard let nextVC = storyboard?.instantiateViewController(identifier: "historyVC") else { return }
//            present(nextVC, animated: true, completion: nil)
            navigationController?.pushViewController(nextVC, animated: true)

        } else {
            Toast(text: NSLocalizedString("Choose your situation", comment: ""), delay: 1, duration: 3).start()
        }
    }
}
