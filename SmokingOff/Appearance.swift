//
//  File.swift
//  SmokingOff
//
//  Created by DfStudio on 09.05.2021.
//

import UIKit


class Appearance {

    
    class func navigationDecor(vc: UIViewController) {
 
        vc.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        vc.navigationController!.navigationBar.shadowImage = UIImage()
        vc.navigationController!.navigationBar.isTranslucent = false
        vc.navigationController!.view.backgroundColor = UIColor.white
        vc.navigationController!.navigationBar.tintColor = .black

//        UITabBar.appearance().barTintColor = UIColor.black
//        UITabBar.appearance().backgroundImage = UIImage()
        
//        vc.navigationController?.navigationBar.topItem?.title = ""
    }
    
    class func navigationClear(vc: UIViewController) {

        vc.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        vc.navigationController!.navigationBar.shadowImage = UIImage()
        vc.navigationController!.navigationBar.isTranslucent = true
        vc.navigationController!.view.backgroundColor = UIColor.clear
        vc.navigationController!.navigationBar.tintColor = .black

//        UITabBar.appearance().barTintColor = UIColor.black
//        UITabBar.appearance().backgroundImage = UIImage()
        
//        vc.navigationController?.navigationBar.topItem?.title = ""
    }
    
}
