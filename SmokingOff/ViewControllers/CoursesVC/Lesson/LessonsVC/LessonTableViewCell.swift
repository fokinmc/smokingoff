//
//  LessonTableViewCell.swift
//  SmokingOff
//
//  Created by DfStudio on 14.04.2021.
//

import UIKit

class LessonTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var nameLesson: UILabel!
    @IBOutlet weak var numberLesson: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var manImage: UIImageView!
    @IBOutlet var animView: UIView!

    var mult: Mult! = nil
    let constants = Constants()

    let animeName = ["action plan", "Addiction", "Dopamine", "Professor and the monkey", "Hero and disaster", "Test"]
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func configure(isFinished: Int, index: IndexPath, numberWeek: Int) {
        
        shadow(view: backView)
        
        
            /*
            // начало рабочей версии для храниния статуса
        if isFinished {
            statusImage.image = #imageLiteral(resourceName: "okGreen")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        } else {
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        }
            */
        
        switch isFinished {
        case 1:
            statusImage.image = #imageLiteral(resourceName: "okGreen")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        case 2:
            self.layer.zPosition = 10
            statusImage.image = #imageLiteral(resourceName: "playGreen")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
            manImage.isHidden = true

//            mult = Mult(name: constants.mainAinimeArray[0])
            if mult == nil {
                mult = Mult(name: animeName[index.row])
                mult.startAnime(addTo: animView, mode: .loop, isCenter: true)
            }
        case 3:
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        case 4:
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        case 5:
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        default:
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
        }
        
        if index.row == 5 {//Constants.lessons[numberWeek][index.row].count - 1 {
            backView.backgroundColor = #colorLiteral(red: 0.4542214274, green: 0.8181859851, blue: 0.7386319637, alpha: 1)
            nameLesson.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            numberLesson.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7493844948)
            statusImage.image = #imageLiteral(resourceName: "lockGreen")
            numberLesson.text = NSLocalizedString("ФИНАЛЬНЫЙ УРОК", comment: "")
        }
//        manImage.image = UIImage(named: Constants.imgLessons[index.row])

    }
    /*
    func configure(status: Int, index: IndexPath, numberWeek: Int) {
        
        shadow(view: backView)
        switch status {
        case 1:
            statusImage.image = #imageLiteral(resourceName: "okGreen")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        case 2:
            statusImage.image = #imageLiteral(resourceName: "playGreen")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
            manImage.isHidden = true
            
            mult = Mult(name: constants.mainAinimeArray[0])
            mult = Mult(name: "more sand")
            mult.startAnime(addTo: animView, mode: .loop, isCenter: true)

        case 3:
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        case 4:
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        case 5:
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
            numberLesson.text = NSLocalizedString("УРОК \(numberWeek)", comment: "")
        default:
            statusImage.image = #imageLiteral(resourceName: "lockYellow")
//            nameLesson.text = Constants.lessons[numberWeek][index.row]
        }
        
        if index.row == 5 {//Constants.lessons[numberWeek][index.row].count - 1 {
            backView.backgroundColor = #colorLiteral(red: 0.4542214274, green: 0.8181859851, blue: 0.7386319637, alpha: 1)
            nameLesson.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            numberLesson.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7493844948)
            statusImage.image = #imageLiteral(resourceName: "lockGreen")
            numberLesson.text = NSLocalizedString("ФИНАЛЬНЫЙ УРОК", comment: "")
        }
//        manImage.image = UIImage(named: Constants.imgLessons[index.row])

    }
    */
    
//    func setupAnimation() {
//        mult.startAnime(addTo: self.animView, mode: .loop, nameAnime: constants.mainAinimeArray[indexPath.row])
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func shadow(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowRadius = 6
    }

}
