//
//  UserDefManager.swift
//  SmokingOff
//
//  Created by DfStudio on 02.04.2021.
//

import UIKit

enum UseDefKeys {
    static let name = "name"
    static let email = "email"
    static let smokingOff = "smokingOff"
    static let stage = "stageOfSmoking"
    static let history = "smokingHistory"
    static let alreadyGreeting = "alreadyGreeting"

    static let numberInPack = "numberInPack"
    static let pricePack = "pricePack"
    static let countOfDay = "countOfDay"
    static let profileImage = "profileImage"
    static let numberOfBotMessage = "numberOfBotMessage"
    static let numberOfSection = "numberOfSection"
    static let numberOfBotDay = "numberOfBotDay"
    static let countOfCigarrete = "countOfCigarrete"
    static let startDateOfSmoking = "startDateOfSmoking"
    static let finishLessonId = "finishLessonId"
    static let currentLessonId = "currentLessonId"


    
//    static let smokingOffDate = "smokingOffDate"



    
}

class UserDefManager {
    
    static let shared = UserDefManager()
    let userDef = UserDefaults.standard

    
    //name
    func saveName(name: String) {
        userDef.set(name, forKey: UseDefKeys.name)
    }
    func getName() -> String {
        return userDef.string(forKey: UseDefKeys.name) ?? ""
    }
    
    //email manager
    func saveEmail(email: String) {
        userDef.set(email, forKey: UseDefKeys.email)
    }
    func getEmail() -> String {
        return userDef.string(forKey: UseDefKeys.email) ?? ""
    }
    
    //smokingOff manager
    func saveSmokingOff(smokingOff: String) {
        userDef.set(smokingOff, forKey: UseDefKeys.smokingOff)
    }
    func getSmokingOff() -> String {
        return userDef.string(forKey: UseDefKeys.smokingOff) ?? ""
    }
    
    //stage of smoking
    func saveStage(value: Int) {
        userDef.set(value, forKey: UseDefKeys.stage)
    }
    func getStage() -> Int {
        return userDef.integer(forKey: UseDefKeys.stage)
    }
    
    //smoking histoty
    func saveHistory(name: String) {
        userDef.set(name, forKey: UseDefKeys.history)
    }
    func getHistory() -> String {
        return userDef.string(forKey: UseDefKeys.history) ?? ""
    }
    
    //already greeting
    func alreadyGreeting() {
        userDef.set(true, forKey: UseDefKeys.alreadyGreeting)
    }
    func getGreetingStatus() -> Bool {
        return userDef.bool(forKey: UseDefKeys.alreadyGreeting)
    }
    
    //finishLessonId
    func finishLessonId(lessonId: Int) {
        userDef.set(true, forKey: UseDefKeys.finishLessonId + "\(lessonId)")
    }
    func getFinishLessonId(lessonId: Int) -> Bool {
        return userDef.bool(forKey: UseDefKeys.finishLessonId + "\(lessonId)")
    }
    
    // орп currentLessonId
    func currentLessonId(lessonId: Int) {
        userDef.set(lessonId, forKey: UseDefKeys.currentLessonId + "\(lessonId)")
    }
    func getcurrentLessonId(lessonId: Int) -> Bool {
        return (userDef.integer(forKey: UseDefKeys.currentLessonId + "\(lessonId)") != 0)
    }
    
    // ++++++++++++++++++++==  Bot
    func saveNumberOfBotMessage(numberOfBotMessage: Int) {
        userDef.set(numberOfBotMessage, forKey: UseDefKeys.numberOfBotMessage)
    }
    func getnumberOfBotMessage() -> Int {
        return userDef.integer(forKey: UseDefKeys.numberOfBotMessage)
    }
    
    func saveNumberOfSection(numberOfSection: Int) {
        userDef.set(numberOfSection, forKey: UseDefKeys.numberOfSection)
    }
    func getNumberOfSection() -> Int {
        return userDef.integer(forKey: UseDefKeys.numberOfSection)
    }
    func saveNumberOfBotDay(numberOfDay: Int) {
        userDef.set(numberOfDay, forKey: UseDefKeys.numberOfBotDay)
    }
    func getnumberOfBotDay() -> Int {
        return userDef.integer(forKey: UseDefKeys.numberOfBotDay)
    }
    func saveCountOfCigarrete(count: Int) {
        userDef.set(count, forKey: UseDefKeys.countOfCigarrete)
    }
    func getCountOfCigarrete() -> Int {
        return userDef.integer(forKey: UseDefKeys.countOfCigarrete)
    }
     
    //profile manager
    /*
    func savesmokingOffDate(number: Int) {
        userDef.set(number, forKey: UseDefKeys.smokingOffDate)
    }
    func getsmokingOffDate() -> Int {
        return userDef.integer(forKey: UseDefKeys.numberInPack)
    }
    */
    
    func saveNumberInPack(number: Int) {
        userDef.set(number, forKey: UseDefKeys.numberInPack)
    }
    func getNumberInPack() -> Int {
        return userDef.integer(forKey: UseDefKeys.numberInPack)
    }
    
    func savePricePack(number: Int) {
        userDef.set(number, forKey: UseDefKeys.pricePack)
    }
    func getPricePack() -> Int {
        return userDef.integer(forKey: UseDefKeys.pricePack)
    }
    
    func saveCountOfDay(number: Int) {
        userDef.set(number, forKey: UseDefKeys.countOfDay)
    }
    func getCountOfDay() -> Int {
        return userDef.integer(forKey: UseDefKeys.countOfDay)
    }
    
    func saveImage(url: URL) {
        userDef.set(url, forKey: UseDefKeys.profileImage)
    }
    func getImageUrl() -> URL? {
        return userDef.url(forKey: UseDefKeys.profileImage)
    }
    func getImage(url: URL) -> UIImage? {
        
        if let data = try? Data(contentsOf: url) {
            // Create Image and Update Image View
            let image = UIImage(data: data) ?? #imageLiteral(resourceName: "lesson1")
            return image
        } else {
            return nil
        }
    }
    
    
    
    // Counters
    
    func saveStartDateOfSmoking(date: String) {
        userDef.set(date, forKey: UseDefKeys.startDateOfSmoking)
    }
    func getStartDateOfSmoking() -> String {
        return userDef.string(forKey: UseDefKeys.startDateOfSmoking) ?? ""
    }
    
    
    private init() {}
}
