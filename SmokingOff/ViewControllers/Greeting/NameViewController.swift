//
//  ViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 02.04.2021.
//

import UIKit
import Toaster



class NameViewController: UIViewController {

    @IBOutlet weak var nameFIeld: UITextField!
    
    
    let useDef = UserDefManager.shared
    var keyBoardActive = false

    override func viewDidLoad() {
        super.viewDidLoad()
        Appearance.navigationDecor(vc: self)

        navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.isHidden = true
        
        
        updateKeyboardPosition()
        hideKeyboard()
        

    }
    
    func updateKeyboardPosition() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        
        let dict: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize: NSValue = dict.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let frameKeyboardSize = keyboardSize.cgRectValue
        
        if !keyBoardActive {
            UIView.animate(withDuration: 0.3) {
                self.view.frame.origin.y -= frameKeyboardSize.height
                
//                let scrollVeiwOffSet = CGPoint(x: 0, y: self.chatScrollView.contentSize.height - self.chatScrollView.bounds.size.height)
//                self.chatScrollView.setContentOffset(scrollVeiwOffSet, animated: true)
            }
        }
        keyBoardActive = true
    }
    
    func hideKeyboard() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapScroll))
        tapGestureRecognizer.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tapGestureRecognizer)

//        chatScrollView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func didTapScroll() {
        self.nameFIeld.endEditing(true)
        keyBoardActive = false
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        let dict: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize: NSValue = dict.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let frameKeyboardSize = keyboardSize.cgRectValue
        
        UIView.animate(withDuration: 0) {
            self.view.frame.origin.y += frameKeyboardSize.height
        }
    }
    
    
    
    @IBAction func nextAction(_ sender: Any) {
        if nameFIeld.text != "" {
            useDef.saveName(name: nameFIeld.text!)
            guard let nextVC = storyboard?.instantiateViewController(identifier: "targetVC") else { return }
            navigationController?.pushViewController(nextVC, animated: true)
            
//            present(nextVC, animated: true, completion: nil)
        } else {
            Toast(text: NSLocalizedString("Enter your name", comment: ""), delay: 1, duration: 3).start()
        }
    }
}


