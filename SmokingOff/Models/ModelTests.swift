//
//  ModelTests.swift
//  SmokingOff
//
//  Created by DfStudio on 29.04.2021.
//

import Foundation

struct Tests: Decodable {
    var tests: [Test]
}

struct Test: Decodable {
    var test: [Question]
}

struct Question: Decodable {
    var id: Int 
    var text: String
    var answers: [Answer]
    var explanation: String
}

struct Answer: Decodable {
    var text: String
    var correct: Bool
}
