//
//  MainViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 06.04.2021.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let useDef = UserDefManager.shared
    
    

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if !useDef.getGreetingStatus() {
            let greetingVC = storyboard?.instantiateViewController(identifier: "nameVC")
            navigationController?.pushViewController(greetingVC!, animated: true)
        }

    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mainCell", for: indexPath) as! MainCollectionViewCell
        
        

        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOneWeek" {
            print("toOneWeek")
            
        }
    }

}


extension MainViewController {

}
