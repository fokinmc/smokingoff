//
//  TowCounterVCCell.swift
//  SmokingOff
//
//  Created by DfStudio on 11.08.2021.
//

import UIKit

class TowCounterVCCell: UITableViewCell {

    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var imageOk: UIImageView!

//    let nameCellArray = ["You haven't smoked 20 cigarettes ", "You haven't smoked 50 cigarettes ", "You haven't smoked 100 cigarettes", "", "", "", ""]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
