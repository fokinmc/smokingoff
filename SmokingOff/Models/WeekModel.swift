//
//  WeekModel.swift
//  SmokingOff
//
//  Created by DfStudio on 19.08.2021.
//

import Foundation


struct Weeks: Decodable {
    let weeks: [Week]
}

struct Week: Decodable {
    var weekName: String
    var lessons: [Lesson]
}


struct Lesson: Decodable {
    var id: Int
    var name: String
    var quoteAuthtor: String?
    var quoteText: String?
    var greetingText: String
    var important: String
    var remember: [Remember]
    var task: String
    var taskDescription: String
    var html: String

}

struct Remember: Decodable {
    var text: String
}
