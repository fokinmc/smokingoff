//
//  HelperViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 30.06.2021.
//

import UIKit

enum OptionBtn {
    case oneBtn
    case twoBtn
    case threeBtn
    case onlyMessage
    case messageBtn
    case twoBtnEndScores
    case fiveBtn
}



class HelperViewController: UIViewController, UITextViewDelegate {
    
    
    
    @IBOutlet weak var chatScrollView: UIScrollView!
    
    @IBOutlet weak var userBlockView: UIView!
    @IBOutlet weak var topButton: UIButton!
    @IBOutlet weak var midleButton: UIButton!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var viewScores: UIView!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var heightBtnView: NSLayoutConstraint!
    
    //util
    let coreDataManager = CoreDataManager()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let useDef = UserDefManager.shared
    var keyBoardActive = false
    
    //size
    var hieghtMessageView: CGFloat!
    var widthMessageView: CGFloat!
    var messageMarginY: CGFloat = 27
    
    //content
    var numberOfSection: Int!
    var numberOfBotMessage: Int!
    var numberOfBotDay: Int!
    
    var messageArray: [Message]!
    var currentBotMessage: BotMessage!
    var isBotMessage: Bool!
    var optionBtn: OptionBtn!
    //    var currentAction: NextAction?
    var endOfSection: Bool?
    var score: Int!
    var countOfCigarrete: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        widthMessageView = self.view.frame.width - 148
        isBotMessage = false
        updateKeyboardPosition()
        hideKeyboard()
        
        numberOfBotMessage = useDef.getnumberOfBotMessage()
        numberOfSection = useDef.getNumberOfSection()
        numberOfBotDay = useDef.getnumberOfBotDay()
        countOfCigarrete = useDef.getCountOfCigarrete()
        updateChat()
    }
    
    func saveOneSigarrete() {
        countOfCigarrete = useDef.getCountOfCigarrete()
        countOfCigarrete += 1
        useDef.saveCountOfCigarrete(count: countOfCigarrete)
        print(countOfCigarrete!, "++++++++++ SAVE сигарет за сегодня \n")
    }
    
    func backToStartOfChoose() {
        numberOfSection = 20
        numberOfBotMessage = 0
        useDef.saveNumberOfSection(numberOfSection: numberOfSection)
        userSomeAction(textForSave: currentBotMessage.buttomBtnName)
    }
    func backBeginAddSigarette() {
        numberOfSection = 21
        numberOfBotMessage = 0
        useDef.saveNumberOfSection(numberOfSection: numberOfSection)
        userSomeAction(textForSave: currentBotMessage.buttomBtnName)
    }
    
    func updateChat() {
        messageArray = coreDataManager.getCoreData()
        
        for i in 0..<messageArray.count {
            if messageArray[i].isBot {
                addBotMessage(numberMessage: i)
            } else {
                addUserMessage(numberMessage: i)
            }
        }
        addNewBotMessage()
        
        let width = self.view.frame.width
        self.chatScrollView.contentSize = CGSize(width: width, height: messageMarginY)
        
        let scrollVeiwOffSet = CGPoint(x: 0, y: self.chatScrollView.contentSize.height - self.chatScrollView.bounds.size.height)
        self.chatScrollView.setContentOffset(scrollVeiwOffSet, animated: true)
    }
    
    func closeChat() {
        chatScrollView.isHidden = true
    }
    
    func addNewBotMessage() {
        
        currentBotMessage = NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage]
        
        var textNexMessage: String?
        var nameB1: String?
        var nameB2: String?
        var nameB3: String?
        
        textNexMessage = currentBotMessage.text
        
        if countOfCigarrete == 0 && numberOfSection == 21 && numberOfBotMessage == 0 {
            textNexMessage = "За сегодня записей нет. Хочешь добавить сигарету?"
        } else if countOfCigarrete > 0 && numberOfSection == 21 && numberOfBotMessage == 0 {
            textNexMessage = """
                На данный момент выкурено сигарет: \(countOfCigarrete!).
                Хочешь добавить сигарету?
                """
        } else if numberOfSection == 22 && numberOfBotMessage == 0 {
            textNexMessage = """
                Закрываем День \(numberOfBotDay! + 1). На данный момент выкурено сигарет: \(countOfCigarrete!).
                                            
                Всё правильно или мы что-то забыли? Можем добавить сейчас!
                """
        }
        
        
        
        optionBtn = currentBotMessage.optionBtn
        
        nameB1 = currentBotMessage.buttomBtnName
        nameB2 = currentBotMessage.middleBtnName
        nameB3 = currentBotMessage.topBtnName
        
        let nameLabel = createLabel(
            messageMarginY: 18,
            color: #colorLiteral(red: 0.9878367782, green: 0.6144195199, blue: 0.3506464958, alpha: 1), text: "Помощник",
            fontSize: 17)
        nameLabel.font = UIFont(name: "Avenir Next Demi Bold", size: 17)
        
        let dateLabel = createLabel(
            messageMarginY: 44,
            color: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), text: getDate(),
            fontSize: 12)
        
        let messageLabel = createLabel(
            messageMarginY: 68,
            color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), text: textNexMessage!,
            fontSize: 17)
        
        setupMessageView(isBot: true, nameLabel: nameLabel, dateLabel: dateLabel, messageLabel: messageLabel)
        setupButtons(nameBtn1: nameB1, nameBtn2: nameB2, nameBtn3: nameB3)
        
        //        newBotMessage = BotMessage(optionBtn: optionBtn, text: messageLabel.text!, endOfSection: endOfSection)
        
    }
    
    
    func userSomeAction(textForSave: String?) {
        let isBot = false
        
        coreDataManager.saveNewMessage(isBot: true, name: "Помощник", date: getDate(), text: currentBotMessage.text)
        
        switch optionBtn {
        case .oneBtn:
            if NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].buttomIsSend! {
                coreDataManager.saveNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
                didTapScroll()
                addNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
                textField.text = ""
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.addNewBotMessage()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.addNewBotMessage()
                }
            }
        case .twoBtn:
            if NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].middleIsSend! {
                coreDataManager.saveNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
                didTapScroll()
                addNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
                textField.text = ""
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.addNewBotMessage()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.addNewBotMessage()
                }
            }
        case .threeBtn:
            if NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].buttomIsSend! {
                coreDataManager.saveNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
                didTapScroll()
                addNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
                textField.text = ""
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.addNewBotMessage()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.addNewBotMessage()
                }
            }
        case .onlyMessage:
            break
        case .messageBtn:
            //            if textField.text!.isEmpty {
            //                print("text is empty")
            //            } else {
            coreDataManager.saveNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textField.text!)
            didTapScroll()
            addNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
            textField.text = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.addNewBotMessage()
            }
        //            }
        case .twoBtnEndScores:
            if NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].buttomIsSend! {
                coreDataManager.saveNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
                didTapScroll()
                addNewMessage(isBot: isBot, name: useDef.getName(), date: getDate(), text: textForSave!)
                textField.text = ""
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.addNewBotMessage()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.addNewBotMessage()
                }
            }
        default:
            break
        }
        
        useDef.saveNumberOfBotMessage(numberOfBotMessage: numberOfBotMessage)
        
    }
    
    //action
    @IBAction func topAction(_ sender: Any) {
        
        if numberOfBotDay == 18 {
            numberOfSection = 21
            numberOfBotMessage = 22
            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
            userSomeAction(textForSave: currentBotMessage.topBtnName)
            
        } else if numberOfSection == 20 && numberOfBotMessage == 0 { //topButton.title(for: .normal) == "ДОБАВИТЬ СИГАРЕТУ" { //запуск цикла
            numberOfSection = 21
            switch numberOfBotDay {
            case 0:
                numberOfBotMessage = 0
            default:
                break
            }
            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
            userSomeAction(textForSave: currentBotMessage.topBtnName)
            
        } else {
            userSomeAction(textForSave: NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].buttomBtnName)
        }
    }
    
    
    
    // +++++++++++++++++++++++++++++++++++++
    // ++++++++       средняя          +++++++++++++++++++++++++++++
    // +++++++++++++++++++++++++++++++++++++
    func from1screen() {
        print("ДОБАВИТЬ ОДНУ", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
        userSomeAction(textForSave: NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].middleBtnName)
        
        useDef.saveNumberOfSection(numberOfSection: numberOfSection)
        
        switch numberOfBotDay { // дни
        case 0:
            numberOfBotMessage = 2
        case 1:
            numberOfBotMessage = 3
        case 2,3,4,5,6,7,8:
            numberOfBotMessage = 4
        case 9,10,11,12,13,14,15,16,17:
            numberOfBotMessage = 5
        case 18:
            numberOfBotMessage = 24
        default:
            break
        }
        useDef.saveNumberOfBotMessage(numberOfBotMessage: numberOfBotMessage)
    }
    
    func from2screen() {
        print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
        
        userSomeAction(textForSave: NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].middleBtnName)
        useDef.saveNumberOfSection(numberOfSection: numberOfSection)
        
        switch numberOfBotDay { //переход на клавиатуру
        case 0,1:
            numberOfBotMessage = 0
            saveOneSigarrete()
        case 2,3:
            numberOfBotMessage = 8
            saveOneSigarrete()
        case 4,5,6,7:
            numberOfBotMessage = 9
            
        case 8:
            numberOfBotMessage = 13
        case 9,10,11,12,13,14,15,16,17:
            numberOfBotMessage = 15
        default:
            break
        }
        
        useDef.saveNumberOfBotMessage(numberOfBotMessage: numberOfBotMessage)
    }
    
    @IBAction func middleAction(_ sender: Any) {
        if numberOfSection == 21 {// midleButton.title(for: .normal) == "ДОБАВИТЬ ОДНУ" { //переход с 1 экрана цикла
            
            switch numberOfBotMessage {
            
            case 0: //
                from1screen()
            case 1:
                print("0")
            case 2:
                from2screen()
            case 3:
                from2screen()
            case 4:
                from2screen()
            case 5:
                from2screen()
            case 6:
                print("0")
            case 7:
                print("0")
            case 8:
                print("0")
            case 9:
                print("0")
            case 10:
                print("0")
            case 11:
                print("0")
            case 12:
                print("0")
            case 13:
                if score != nil {
                    numberOfSection = 21
                    
                    numberOfBotMessage = 14
                    
                    if numberOfBotDay == 9 || numberOfBotDay == 10 { // возможно индекс дня (-1)
                        numberOfBotMessage = 16
                    }
                    
                    useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                    var text = ""
                    switch score {
                    case 1:
                        text = "балл"
                    case 2:
                        text = "балла"
                    case 3:
                        text = "балла"
                    case 4:
                        text = "балла"
                    case 5:
                        text = "балов"
                    default:
                        break
                    }
                    userSomeAction(textForSave: "Хочу курить на \(score!) \(text)")
                    scoreBtn1.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
                    scoreBtn2.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
                    scoreBtn3.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
                    scoreBtn4.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
                    scoreBtn5.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
                    score = nil
                    saveOneSigarrete()
                }
            case 14:
                print("0")
            case 15:
                print("0")
            case 16:
                print("0")
            case 17:
                print("0")
            case 18:
                print("0")
            case 19:
                print("пойду", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
                numberOfSection = 21
                numberOfBotMessage = 20
                saveOneSigarrete()

                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.middleBtnName)
            case 20:
                print("0")
            case 21:
                print("0")
            case 22:
                print("0")
            case 23:
                numberOfSection = 21
                numberOfBotMessage = 24
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.middleBtnName)
            case 24:
                print("0")
            case 25:
                print("0")
            case 26:
                print("0")
            case 27:
                print("0")
                
            default:
                break
            }
            print("текущая позиция switch средняя", numberOfBotMessage!)
            

        } else if numberOfSection == 22 && numberOfBotMessage == 0 {//"Не все добавил"
            
            numberOfSection = 21
            numberOfBotMessage = 0
            
            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
            userSomeAction(textForSave: currentBotMessage.middleBtnName)
//        } else if numberOfSection == 21 && numberOfBotMessage == 13 { //"подтверждение оценки"
            
//            if score != nil {
//                numberOfSection = 21
//
//                numberOfBotMessage = 14
//
//                if numberOfBotDay == 9 || numberOfBotDay == 10 { // возможно индекс дня (-1)
//                    numberOfBotMessage = 16
//                }
//
//                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//                var text = ""
//                switch score {
//                case 1:
//                    text = "балл"
//                case 2:
//                    text = "балла"
//                case 3:
//                    text = "балла"
//                case 4:
//                    text = "балла"
//                case 5:
//                    text = "балов"
//                default:
//                    break
//                }
//                userSomeAction(textForSave: "Хочу курить на \(score!) \(text)")
//                scoreBtn1.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
//                scoreBtn2.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
//                scoreBtn3.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
//                scoreBtn4.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
//                scoreBtn5.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
//                score = nil
//            }
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 19 {//"пойду курить 15 день"
//            closeChat()
//            print("пойду", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            numberOfSection = 21
//            numberOfBotMessage = 20
//
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.middleBtnName)
        }
        

        
    }
    // ++++++++++++++++++++++++++++++++++++++++++
    // нижняя
    // ++++++++++++++++++++++++++++++++++++++++++
    
    @IBAction func bottomAction(_ sender: Any) {
        
        print("нижняя кнопка общее",
              "numberOfBotDay", numberOfBotDay!,
              "numberOfSection", numberOfSection!,
              "numberOfBotMessage", numberOfBotMessage!)
        
        
        if numberOfSection == 20 && numberOfBotMessage == 0 {//"ЗАКРЫТЬ ДЕНЬ" {
            print("ЗАКРЫТЬ ДЕНЬ", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
            numberOfSection = 22
            numberOfBotMessage = 0
            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
        } else if currentBotMessage.endOfSection { // "ПОНЯЛ" Запуск startOfChoose выбора из 3х
            numberOfSection = 20
            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
            numberOfBotMessage = 0
            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            

//        } else if numberOfSection == 21 && numberOfBotMessage == 11 { // конец сообщение 6 день
//            print("конец сообщение 6 день", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 12 { // конец сообщение 6 день
//            print("конец сообщение 6 день", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 13 { // ПЕРЕДУМАЛ на баллах
//            print("передумал на баллах", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 14 { // ХОРОШО после оценки на баллах
//            print("ХОРОШО после оценки на баллах", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 15 { // ПЕРЕДУМАЛ на 9 дне про награду
//            print("передумал на 9 дне про награду", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 16 { // 10 день "про отдых"
//            print("передумал на 9 дне про награду", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//
//            numberOfSection = 21
//            numberOfBotMessage = 14
//            if numberOfBotDay == 10 {
//                numberOfBotMessage = 17
//            } else if numberOfBotDay == 13 {
//                numberOfBotMessage = 18
//            }
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 17 { // 11 день "про осознанность"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 18 { // 13 день "про осознанность"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 19 { // 15 день "пойду не пойду"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//
//            numberOfSection = 21
//            numberOfBotMessage = 21
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 20 { // 15 день "пошел курить выход на новый цикл"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 21 { // 15 день "пошел курить выход на новый цикл"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//            closeChat()
//
//            numberOfSection = 21
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 22 { // 15 день "пошел курить выход на новый цикл"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//
//            numberOfSection = 21
//            numberOfBotMessage = 23
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 23 { // 15 день "пошел курить выход на новый цикл"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//
//            numberOfSection = 21
//            numberOfBotMessage = 22
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 24 { // 15 день "пошел курить выход на новый цикл"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//
//            numberOfSection = 21
//            numberOfBotMessage = 25
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 25 { // 15 день "пошел курить выход на новый цикл"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//
//            numberOfSection = 21
//            numberOfBotMessage = 26
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//        } else if numberOfSection == 21 && numberOfBotMessage == 26 { // 15 день "пошел курить выход на новый цикл"
//            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
//
//            numberOfSection = 19
//            numberOfBotMessage = 0
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
            
        } else if numberOfSection == 21 { // общее условие для 21
            
            switch numberOfBotMessage {
            case 0: //
                backToStartOfChoose()
            case 1:
                print("0") //не используемое значение кол-ва сигарет
            case 6:
                print("0")
            case 7:
                print("0")
            case 2,3,4,5,8,9,10,11,12,13,14,15,17,18,20,21:
                backBeginAddSigarette()
            case 16:
                print("передумал на 9 дне про награду", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)

                numberOfSection = 21
                numberOfBotMessage = 14
                if numberOfBotDay == 10 {
                    numberOfBotMessage = 17
                } else if numberOfBotDay == 13 {
                    numberOfBotMessage = 18
                }
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.buttomBtnName)
//            case 17,18:
//                backBeginAddSigarette()
            case 19:
                print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
                
                numberOfSection = 21
                numberOfBotMessage = 21
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.buttomBtnName)
//            case 20,21:
//                backBeginAddSigarette()
            case 22:
                print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
                
                numberOfSection = 21
                numberOfBotMessage = 23
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            case 23:
                print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
                
                numberOfSection = 21
                numberOfBotMessage = 22
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            case 24:
                numberOfSection = 21
                numberOfBotMessage = 25
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            case 25:
                numberOfSection = 21
                numberOfBotMessage = 26
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            case 26:
                numberOfSection = 19
                numberOfBotMessage = 0
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            case 27:
                print("0")
                
            default:
                break
            }
            print("текущая позиция switch нижняя", numberOfBotMessage!)
            
            /*
             выход на новую жизнь
             print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
             
             numberOfSection = 19
             numberOfBotMessage = 0
             useDef.saveNumberOfSection(numberOfSection: numberOfSection)
             userSomeAction(textForSave: currentBotMessage.buttomBtnName)
             */
            
            
            
            //        } else if bottomButton.title(for: .normal) == "НАЗАД" { //выход из цикла
            //            print("numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
            //            numberOfSection = 20
            //            numberOfBotMessage = 0
            //            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
            //            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
        } else if numberOfSection == 22 && numberOfBotMessage == 0 { // закрытие дня
            print("закрытие дня", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
            
            numberOfSection = 22
            numberOfBotMessage = 1
            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            
//            numberOfBotMessage = 0
//            numberOfBotDay = useDef.getnumberOfBotDay()
//            numberOfBotDay += 1
//            countOfCigarrete = 0
//            useDef.saveCountOfCigarrete(count: countOfCigarrete)
//            numberOfSection = numberOfBotDay
//
//
//            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
//            useDef.saveNumberOfBotDay(numberOfDay: numberOfBotDay)
//            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
        } else if numberOfSection == 22 && numberOfBotMessage == 1 { // закрытие дня
            print("закрытие дня", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
            
            numberOfBotMessage = 0
            numberOfBotDay = useDef.getnumberOfBotDay()
            numberOfBotDay += 1
            countOfCigarrete = 0
            useDef.saveCountOfCigarrete(count: countOfCigarrete)
            numberOfSection = numberOfBotDay
             
            
            useDef.saveNumberOfSection(numberOfSection: numberOfSection)
            useDef.saveNumberOfBotDay(numberOfDay: numberOfBotDay)
            userSomeAction(textForSave: currentBotMessage.buttomBtnName)
        } else {
            print("!!!!!!!!!!! сработала не определенная нижняя кнопка")
            userSomeAction(textForSave: NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].buttomBtnName)
            //            nextAction = NewBotMessage.messagesAllDays[numberOfSection][numberOfBotMessage].nextAction
            numberOfBotMessage += 1
        }
        
    }
    
    @IBAction func sendAction(_ sender: Any) {
        print("выход из сообщеия", "numberOfSection", numberOfSection!, "numberOfBotMessage", numberOfBotMessage!)
        
        
        if textField.text!.isEmpty {
            print("text is empty")
        } else {
            print(
                "numberOfBotDay", numberOfBotDay!,
                "numberOfSection", numberOfSection!,
                "numberOfBotMessage", numberOfBotMessage!)
            
            
            if numberOfBotDay == 4 {//} numberOfSection == 21 && numberOfBotMessage == 9 {//"1 выход из сообщения 5 день"
                
                numberOfSection = 21
                numberOfBotMessage = 10
                saveOneSigarrete()

                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
                //                userSomeAction(textForSave: currentBotMessage.buttomBtnName)
            } else if numberOfBotDay == 5 { //1 выход из сообщения 6 день"
                saveOneSigarrete()

                numberOfSection = 21
                numberOfBotMessage = 11
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            } else if numberOfBotDay == 6 { //"1 выход из сообщения 7 день"
                saveOneSigarrete()

                numberOfSection = 21
                numberOfBotMessage = 12
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            } else if numberOfBotDay == 7 { //"1 выход из сообщения 8 день переход на оценку"
                print("1 сообщение 0 день")
                
                numberOfSection = 21
                numberOfBotMessage = 13
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            } else if numberOfBotDay == 9 { //" 10 отправлка награды"
                
                numberOfSection = 21
                numberOfBotMessage = 13
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            } else if numberOfBotDay == 10 { //" 11 отправлка награды"
                
                numberOfSection = 21
                numberOfBotMessage = 13
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            } else if numberOfBotDay == 11 { //" 12 отправка награды без оценки"
                saveOneSigarrete()

                numberOfSection = 21
                numberOfBotMessage = 16
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            }
            else if numberOfBotDay == 12 { //" 13 отправка награды без оценки"
                saveOneSigarrete()

                numberOfSection = 21
                numberOfBotMessage = 16
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            }
            else if numberOfBotDay == 13 { //" 14 отправка награды без оценки"
                saveOneSigarrete()

                numberOfSection = 21
                numberOfBotMessage = 16
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            }
            else if numberOfBotDay == 14 || numberOfBotDay == 15 || numberOfBotDay == 16 || numberOfBotDay == 17  { //" 15 отправка награды без оценки"
                
                numberOfSection = 21
                numberOfBotMessage = 19
                
                useDef.saveNumberOfSection(numberOfSection: numberOfSection)
                userSomeAction(textForSave: textField.text!)
            }
        }
    }
    
    @IBOutlet weak var scoreBtn1: UIButton!
    @IBOutlet weak var scoreBtn2: UIButton!
    @IBOutlet weak var scoreBtn3: UIButton!
    @IBOutlet weak var scoreBtn4: UIButton!
    @IBOutlet weak var scoreBtn5: UIButton!
    
    @IBAction func score1(_ sender: Any) {
        score = 1
        setupScoresBtn(btn: scoreBtn1)
    }
    @IBAction func score2(_ sender: Any) {
        score = 2
        setupScoresBtn(btn: scoreBtn2)
    }
    @IBAction func score3(_ sender: Any) {
        score = 3
        setupScoresBtn(btn: scoreBtn3)
    }
    @IBAction func score4(_ sender: Any) {
        score = 4
        setupScoresBtn(btn: scoreBtn4)
    }
    @IBAction func score5(_ sender: Any) {
        score = 5
        setupScoresBtn(btn: scoreBtn5)
    }
    func setupScoresBtn(btn: UIButton) {
        scoreBtn1.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
        scoreBtn2.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
        scoreBtn3.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
        scoreBtn4.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
        scoreBtn5.backgroundColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
        
        btn.backgroundColor = #colorLiteral(red: 0.9878367782, green: 0.6144195199, blue: 0.3506464958, alpha: 1)
    }
    
    
    
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


extension HelperViewController {
    
    //    func createNextAction(numberOfSection: Int, isNextSection: Bool) {
    //        self.numberOfSection = numberOfSection
    //        if isNextSection {
    //            numberOfBotMessage = 0
    //        } else {
    //            numberOfBotMessage += 1
    //        }
    //        useDef.saveNumberOfSection(numberOfSection: numberOfSection)
    //        userSomeAction(textForSave: currentBotMessage.buttomBtnName)
    //    }
    
    func updateKeyboardPosition() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func hideKeyboard() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapScroll))
        tapGestureRecognizer.numberOfTouchesRequired = 1
        chatScrollView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func didTapScroll() {
        self.textField.endEditing(true)
        keyBoardActive = false
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        
        let dict: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize: NSValue = dict.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let frameKeyboardSize = keyboardSize.cgRectValue
        
        if !keyBoardActive {
            UIView.animate(withDuration: 0) {
                self.view.frame.origin.y -= frameKeyboardSize.height - 88
                let scrollVeiwOffSet = CGPoint(x: 0, y: self.chatScrollView.contentSize.height - self.chatScrollView.bounds.size.height)
                self.chatScrollView.setContentOffset(scrollVeiwOffSet, animated: true)
            }
        }
        keyBoardActive = true
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        let dict: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize: NSValue = dict.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let frameKeyboardSize = keyboardSize.cgRectValue
        
        UIView.animate(withDuration: 0) {
            self.view.frame.origin.y += frameKeyboardSize.height - 88
        }
    }
    
    func getDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let currentDate = formatter.string(from: date)
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        let currentTime = timeFormatter.string(from: date)
        let saveDate = currentDate + " в " + currentTime
        return saveDate
    }
    
    func addUserMessage(numberMessage: Int) {
        let nameLabel = createLabel(
            messageMarginY: 18,
            color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), text: useDef.getName(),
            fontSize: 15)
        nameLabel.font = UIFont(name: "Avenir Next Demi Bold", size: 17)
        
        let dateLabel = createLabel(
            messageMarginY: 44,
            color: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), text: messageArray[numberMessage].date!,
            fontSize: 12)
        
        let messageLabel = createLabel(
            messageMarginY: 68,
            color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), text: messageArray[numberMessage].text! + ".",
            fontSize: 17)
        
        setupMessageView(isBot: false, nameLabel: nameLabel, dateLabel: dateLabel, messageLabel: messageLabel)
    }
    
    func addBotMessage(numberMessage: Int) {
        let nameLabel = createLabel(
            messageMarginY: 18,
            color: #colorLiteral(red: 0.9878367782, green: 0.6144195199, blue: 0.3506464958, alpha: 1), text: "Помощник",
            fontSize: 15)
        nameLabel.font = UIFont(name: "Avenir Next Demi Bold", size: 17)
        
        let dateLabel = createLabel(
            messageMarginY: 44,
            color: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), text: messageArray[numberMessage].date!,
            fontSize: 12)

        let messageLabel = createLabel(
            messageMarginY: 68,
            color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), text: messageArray[numberMessage].text!,
            fontSize: 17)
        
        setupMessageView(isBot: true, nameLabel: nameLabel, dateLabel: dateLabel, messageLabel: messageLabel)
    }
    
    func addNewMessage(isBot: Bool, name: String, date: String, text: String) {
        let nameLabel = createLabel(
            messageMarginY: 18,
            color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), text: name,
            fontSize: 15)
        nameLabel.font = UIFont(name: "Avenir Next Demi Bold", size: 17)
        
        let dateLabel = createLabel(
            messageMarginY: 44,
            color: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), text: date,
            fontSize: 12)
        
        let messageLabel = createLabel(
            messageMarginY: 68,
            color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), text: text +  ".",
            fontSize: 17)

        setupMessageView(isBot: isBot, nameLabel: nameLabel, dateLabel: dateLabel, messageLabel: messageLabel)
    }
    
    func setupMessageView(isBot: Bool, nameLabel: UILabel, dateLabel: UILabel, messageLabel: UILabel) {
        let messageVeiw = UIView()
        hieghtMessageView = nameLabel.frame.height + dateLabel.frame.height + messageLabel.frame.height + 48
        
        shadow(view: messageVeiw)
        messageVeiw.layer.cornerRadius = 8
        messageVeiw.backgroundColor = .white
        messageVeiw.sizeToFit()
//        messageVeiw.roundCorners([.bottomLeft, .bottomRight, .topLeft], radius: 8)
//        messageVeiw.clipsToBounds = true
        if !isBot {
            messageVeiw.frame = CGRect(
                x: self.view.frame.width - self.view.frame.width + 128 - 32,
                y: 0,
                width: self.view.frame.width - 128,
                height: hieghtMessageView)
            
            messageVeiw.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]

            var image = UIImage()
            if let url = useDef.getImageUrl() {
                image = useDef.getImage(url: url)!
            } else {
                image = UIImage(named: "imageUser")!
            }
            
            let avatar = UIImageView(image: image)
            avatar.frame = CGRect(x: 32, y: 28, width: 38, height: 38)
            avatar.frame.origin.y = messageMarginY
            
            var read = UIImageView(image: UIImage(named: "readMen"))
            read.frame = CGRect(x: messageVeiw.frame.width - 28, y: 18, width: 12, height: 9)
            self.chatScrollView.addSubview(avatar)
            messageVeiw.addSubview(read)

        } else {
            messageVeiw.frame = CGRect(
                x: 32,
                y: 0,
                width: self.view.frame.width - 128,
                height: hieghtMessageView)
            
            messageVeiw.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
            
            let avatar = UIImageView(image: UIImage(named: "avatarBot"))
            avatar.frame = CGRect(x: messageVeiw.frame.width + 48, y: 28, width: 38, height: 38)
            avatar.frame.origin.y = messageMarginY
            
            let read = UIImageView(image: UIImage(named: "readBot"))
            read.frame = CGRect(x: messageVeiw.frame.width - 28, y: 18, width: 16, height: 9)
            
            self.chatScrollView.addSubview(avatar)
            messageVeiw.addSubview(read)

        }
        
        messageVeiw.frame.origin.y = messageMarginY
        messageMarginY += messageVeiw.frame.height + 30
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(580)) {
            self.chatScrollView.addSubview(messageVeiw)
            messageVeiw.addSubview(nameLabel)
            messageVeiw.addSubview(dateLabel)
            messageVeiw.addSubview(messageLabel)
            
            let scrollVeiwOffSet = CGPoint(x: 0, y: self.chatScrollView.contentSize.height - self.chatScrollView.bounds.size.height)
            self.chatScrollView.setContentOffset(scrollVeiwOffSet, animated: true)
        }
        
        let width = self.view.frame.width
        self.chatScrollView.contentSize = CGSize(width: width, height: messageMarginY)
        
    }
    
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    func capitalize(string: String, positions: IndexSet) -> String {
        let range = (0..<string.count)
        guard string.count > 0,
            positions.allSatisfy({ range ~= $0 }) else {
            fatalError("Couldn't capitalize")
        }

        return String(string.enumerated().map{ (index, char) in
            return positions.contains(index) ? String(char).capitalized.first! : char

        })
    }

    
    
    func createLabel(messageMarginY: CGFloat, color: UIColor, text: String, fontSize: CGFloat) -> UILabel {
        
        let font = UIFont(name: "Avenir Next Regular", size: fontSize)
        let height = heightForView(text: text, font: font!, width: widthMessageView)
        let label = UILabel()
        print(text)
        let lowercaseText = text.lowercased()

        label.text = capitalize(string: lowercaseText, positions: [0])
        label.textColor = color
        label.frame = CGRect(x: 18, y: 18, width: widthMessageView - 8, height: height)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = font
        label.frame.origin.y = messageMarginY
        return label
    }
    
    func setupButtons(nameBtn1: String?, nameBtn2: String?, nameBtn3: String?) {
        setupUsersView()
        
        if !bottomButton.isHidden {
            bottomButton.setTitle(nameBtn1, for: .normal)
        }
        if !midleButton.isHidden {
            midleButton.setTitle(nameBtn2, for: .normal)
        }
        if !topButton.isHidden {
            topButton.setTitle(nameBtn3, for: .normal)
        }
    }
    
    func shadow(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 6)
        view.layer.shadowRadius = 8
    }
    
    func setupUsersView() {
        switch optionBtn {
        case .oneBtn:
            topButton.isHidden = true
            midleButton.isHidden = true
            bottomButton.isHidden = false
            viewMessage.isHidden = true
            viewScores.isHidden = true
            heightBtnView.constant = 48
        case .twoBtn:
            topButton.isHidden = true
            midleButton.isHidden = false
            bottomButton.isHidden = false
            viewMessage.isHidden = true
            viewScores.isHidden = true
            heightBtnView.constant = 118
            
        case .threeBtn:
            topButton.isHidden = false
            midleButton.isHidden = false
            bottomButton.isHidden = false
            viewMessage.isHidden = true
            viewScores.isHidden = true
            heightBtnView.constant = 188
            
        case .onlyMessage:
            topButton.isHidden = true
            midleButton.isHidden = true
            bottomButton.isHidden = true
            viewMessage.isHidden = false
            viewScores.isHidden = true
            heightBtnView.constant = 48
            
        case .messageBtn:
            topButton.isHidden = true
            midleButton.isHidden = true
            bottomButton.isHidden = false
            viewMessage.isHidden = false
            viewScores.isHidden = true
            heightBtnView.constant = 118
            
        case .twoBtnEndScores:
            topButton.isHidden = true
            midleButton.isHidden = false
            bottomButton.isHidden = false
            viewMessage.isHidden = true
            viewScores.isHidden = false
            heightBtnView.constant = 188
            
        default:
            break
        }
    }
}
