//
//  FinishLessonVC.swift
//  SmokingOff
//
//  Created by DfStudio on 31.08.2021.
//

import UIKit

class FinishLessonVC: UIViewController {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var finishLessonLabel: UILabel!
    @IBOutlet weak var finishDescriprionLabel: UILabel!

    
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!

    
    @IBOutlet weak var imageVeiw: UIImageView!
    @IBOutlet weak var imageVeiw2: UIImageView!
    var lessonId: Int!
    let useDef = UserDefManager.shared
    let tempText = """
        В следующем уроке Вы сможете пройти тестирование и точно это узнать!

        Также мы узнаем, почему так сложно бросить курить.
        """
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.isHidden = true
        finishLessonLabel.isHidden = true
        finishDescriprionLabel.isHidden = true
        imageVeiw2.isHidden = true
    }
    
    func yesNoTap() {
        descriptionLabel.text = tempText
        yesButton.isHidden = true
        noButton.isHidden = true
        nextButton.isHidden = false
    }
    
    

    @IBAction func yesAction(_ sender: Any) {
        yesNoTap()
    }

    @IBAction func noAction(_ sender: Any) {
        yesNoTap()
    }
    
    var finishLesson = false
    
    @IBAction func finishAction(_ sender: Any) {
        if !finishLesson {
            nextButton.setBackgroundImage(nil, for: .normal)
            descriptionLabel.font = UIFont(name: "Avenir Next Demi Bold", size: 24)
            descriptionLabel.text = "Поздравляем!"
            finishLessonLabel.isHidden = false
            finishDescriprionLabel.isHidden = false
            finishLesson = true
            imageVeiw2.isHidden = false
            imageVeiw.isHidden = true

        } else {
            print("go to start")
            useDef.finishLessonId(lessonId: lessonId)
            useDef.currentLessonId(lessonId: lessonId + 1)
        }
    }
    
}
