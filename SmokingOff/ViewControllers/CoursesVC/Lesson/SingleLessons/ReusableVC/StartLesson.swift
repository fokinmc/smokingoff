//
//  TemplateViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 21.07.2021.
//

import UIKit

class StartLesson: UIViewController {
    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var quoteAuthor: UILabel!
    @IBOutlet weak var nameStartLabel: UILabel!
    @IBOutlet weak var textStartLabel: UILabel!
 
    var lesson: Lesson!
    var lessonNumber: Int!
    var quoteText: String!
    var authorQuote: String!
    var nameStart: String!
    var textStart: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Appearance.navigationClear(vc: self)
        tabBarController?.tabBar.isHidden = true
        
        quoteText = lesson.quoteText
        authorQuote = lesson.quoteAuthtor
        nameStart = lesson.name
        textStart = lesson.greetingText
        

        navigationController?.navigationBar.topItem?.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 0.78

        quoteLabel.attributedText = NSMutableAttributedString(string: quoteText, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        quoteLabel.textAlignment = .center
        quoteAuthor.text = authorQuote
        nameStartLabel.text = nameStart
        textStartLabel.text = textStart
        
//        navigationController?.n`avigationBar.topItem?.backBarButtonItem?.isEnabled = false
//        navigationController?.navigationBar.topItem?.backBarButtonIt`em?.tintColor = .clear
        
        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        switch lessonNumber {
        case 0:
            guard let vc = storyboard?.instantiateViewController(identifier: "Lesson_1_day") as? Lesson_1_day else { return }
            navigationController?.pushViewController(vc, animated: true)
            vc.lesson = lesson
        case 1:
            guard let vc = storyboard?.instantiateViewController(identifier: "oneLesson") as? LessonViewController else { return }
            navigationController?.pushViewController(vc, animated: true)
//            vc.lesson = lesson
        case 5 :
            guard let vc = storyboard?.instantiateViewController(identifier: "beginTest") as? BeginTestViewController else { return }
            navigationController?.pushViewController(vc, animated: true)
//            vc.lesson = lesson
        default:
            break
        }

    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
