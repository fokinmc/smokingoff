//
//  Constants.swift
//  SmokingOff
//
//  Created by DfStudio on 13.04.2021.
//

import Foundation


class Constants {
    
//    static let sourseBotMessages 

    
    static let weeks = ["Week of preparation.", "Positive week.","Awareness Week.","New Life Week."]
    
    static let lessons = [["Action Plan.",
                           "Addiction.",
                           "Dopamine.",
                           "The Professor and the Monkey.",
                           "Hero and Disaste r.",
                           "Test 1"],
                          
                          ["Trigger.",
                           "Smoky Rewards.",
                           "Mythbusters.",
                           "Pleasant Alternatives.",
                           "The Gourmet of Joy.",
                           "Test 2"],
                          
                          ["Greetings from the future",
                           "Deliberate Pause",
                           "More Sand!",
                           "Total Delight!",
                           "Be honest.",
                           "Test 3"],
                          
                          ["Just One Cigarette.",
                           "Rescue from Disruptions.",
                           "The most extreme case.",
                           "Lifebuoy.",
                           "New Me."]
    ]
    
    static let imgWeeks = ["week1", "week2", "week3", "week4"]
    static let imgLessons = ["lesson1", "lesson2", "lesson3", "lesson4", "lesson5", "test"]
    
    
    let mainAinimeArray = ["more sand", "Action Plan", "more sand", "more sand", "more sand", "more sand"]
//    let breathAnim = ["Front_Idle"]
    let professorTriggerPresentation = ["triggerProfessor",]
}

enum WeekStatus {
    static let finished = 1
    static let current = 2
    static let newest = 3
}




