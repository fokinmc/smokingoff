//
//  TestViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 29.04.2021.
//

import UIKit

protocol FirstViewControllerDelegate: class {
    func closeExplanation()
}

class TestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FirstViewControllerDelegate {
    
    
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var explanationLabel: UILabel!
    @IBOutlet weak var progressLine: UIProgressView!
    @IBOutlet weak var countOfWeekLabel: UILabel!
    @IBOutlet weak var explanationButtonAnchor: NSLayoutConstraint!
    
    
    var expView: UIView!
    var endView: UIImageView!
    let question: String! = nil

    var test: [Question]!
    var currentQuestion: Question!
    var currentTestNumber: Int!
    
    var currentQustionNumber: Int!
    var countOfCurrentAnswer = 0
    var isCorrectAnswer: Bool!
    
    var coutnOfTry = 1
    var backColor = #colorLiteral(red: 0.9640881419, green: 0.9686412215, blue: 0.9772089124, alpha: 1)
    var textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    var okImage: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
         let locale = NSLocale.preferredLanguages.first!
         print(locale)
         if locale.hasPrefix("en") {
         print(locale)
         }
         */
        tabBarController?.tabBar.isHidden = true
        
        currentQustionNumber = 0
        currentQuestion = test[currentQustionNumber]
        questionLabel.text = currentQuestion.text // доделать текущий тест его номер из страницы недели
        table.separatorStyle = .none
        progressLine.setProgress(Float(currentQustionNumber) / Float(10) + 0.1, animated: true)
        countOfWeekLabel.text = "\(currentQustionNumber + 1) of 10"

    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TestTableViewCell
        
        cell.answerLable.text = currentQuestion.answers[indexPath.row].text
        cell.backView.backgroundColor = backColor
        cell.answerLable.textColor = textColor
        cell.okImage.image = okImage
        cell.numberOfAnswer.text = "\(indexPath.row + 1)."
        cell.numberOfAnswer.textColor = textColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        for i in 0..<currentQuestion.answers.count {
            if currentQuestion.answers[i].correct {
                backColor = #colorLiteral(red: 0.4542214274, green: 0.8181859851, blue: 0.7386319637, alpha: 1)
                textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                okImage = UIImage(named: "okGreen-1")
                table.reloadRows(at: [IndexPath(row: i, section: 0) ], with: .fade)
            }
        }
        
        if currentQuestion.answers[indexPath.row].correct {
            isCorrectAnswer = true
            backColor = #colorLiteral(red: 0.4542214274, green: 0.8181859851, blue: 0.7386319637, alpha: 1)
            textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            okImage = UIImage(named: "okGreen-1")
        } else {
            isCorrectAnswer = false
            backColor = #colorLiteral(red: 0.8633355498, green: 0.4232113659, blue: 0.4224595428, alpha: 1)
            textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            okImage = UIImage(named: "wrong")
        }
//        textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        table.reloadRows(at: [indexPath], with: .fade)
        chekCurrentAnswer()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            openExplanationVC()
            okImage = UIImage(named: "")
        }
        
        print("попытка №", coutnOfTry, "Правильных ответов с 1 - ", countOfCurrentAnswer)
        coutnOfTry += 1
        
        return indexPath
    }
    
    func openExplanationVC() {
        let vc = storyboard?.instantiateViewController(identifier: "explanationVC") as! ExplanationViewController
        vc.titleQ = currentQuestion.text
        vc.text = currentQuestion.explanation
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func closeExplanation() {
            nextQustion()
            coutnOfTry = 1
            navigationController?.navigationBar.isHidden = false

    }
    
    
    
    @objc func nextAction() {
        nextQustion()
        coutnOfTry = 1
        navigationController?.navigationBar.isHidden = false
    }
    
    @objc func finishAction() {
        navigationController?.popViewController(animated: true)
    }
    
    func nextQustion() {
        
        backColor = #colorLiteral(red: 0.9640881419, green: 0.9686412215, blue: 0.9772089124, alpha: 1)
        textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        if currentQustionNumber < test.count - 1 {
            currentQustionNumber += 1
            currentQuestion = test[currentQustionNumber]
            questionLabel.text = currentQuestion.text
            table.reloadData()
        } else {
            openResultVC()
        }
        progressLine.setProgress(Float(currentQustionNumber) / Float(10) + 0.1, animated: true)
        countOfWeekLabel.text = "\(currentQustionNumber + 1) of 10"
    }
    
    func chekCurrentAnswer() {
        if coutnOfTry == 1 && isCorrectAnswer {
            countOfCurrentAnswer += 1
        }
    }
    
    func openResultVC() {
        let vc = storyboard?.instantiateViewController(identifier: "ResultTestViewController") as! ResultTestViewController
        vc.countOfCurrentAnswer = countOfCurrentAnswer
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showFinishDialog() {
        var text = ""
        if countOfCurrentAnswer >= 8 {
            text = "Your test result: - \(countOfCurrentAnswer) of 10"
        } else {
            text = "Your test result: - \(countOfCurrentAnswer) of 10"
        }
        
        
        endView = UIImageView(frame: self.view.frame)
        endView.image = UIImage(named: "bigBack")
        endView.contentMode = .scaleToFill
        
        
        let label = UILabel(frame: CGRect(x: 100, y: 100, width: 234, height: 233))
        label.text = text
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let nextBtn = UIButton(frame: CGRect(x: 250, y: 500, width: 128, height: 58))
        nextBtn.setTitle("Next", for: .normal)
        nextBtn.setTitleColor(#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), for: .normal)
        nextBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        nextBtn.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        
        let finishBtn = UIButton(frame: CGRect(x: 50, y: 500, width: 128, height: 58))
        finishBtn.setTitle("Finish", for: .normal)
        finishBtn.setTitleColor(#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), for: .normal)
        finishBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        finishBtn.addTarget(self, action: #selector(finishAction), for: .touchUpInside)
        
        
        endView.addSubview(label)
        
        endView.addSubview(finishBtn)
        self.view.addSubview(endView)
        
    }
}
