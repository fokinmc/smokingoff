//
//  ModelMessage.swift
//  SmokingOff
//
//  Created by DfStudio on 05.07.2021.
//

import Foundation

struct BotMessage {
    var optionBtn: OptionBtn
    var text: String
    var topBtnName: String?
    var middleBtnName: String?
    var buttomBtnName: String?

    var nextSection: Int
    
    var topIsSend: Bool?
    var middleIsSend: Bool?
    var buttomIsSend: Bool?
    var endOfSection: Bool
//    var nextAction: NextAction?

}
 // может выбирать следующую секцию из 3 вариантов по кнопкам

enum NextAction {
    case startNewDay
    case next
    case endMainMessage
    case addSigarette
    case forgetSigarette
    case closeDay
}
