//
//  RoundAboutButton.swift
//  SmokingOff
//
//  Created by DfStudio on 16.06.2021.
//

import UIKit

class RoundAboutButton: UIView {
    
    var timerAnime: Timer!
    var animButtonAbout = ()
    var button: UIButton! = nil
    var isStoped = false
    var array = [UIButton]()
    let size = CGSize(width: 60, height: 60)
    var namesButton = ["RoundButton0", "RoundButton1", "RoundButton2", "RoundButton3", "RoundButton4", "RoundButton5", "RoundButton6", "RoundButton7", "RoundButton8", "RoundButton9", "RoundButton12", "RoundButton10", "RoundButton11",  "RoundButton12"]
    
    func createButton(runAnim: Bool) {
        namesButton = namesButton.shuffled()
        var x: CGFloat = 0
        if runAnim {
            x = 0
        } else {
            x = self.frame.width
        }
        
        let y = self.size.height - 44
        
        for i in 0...4 {
            let point = CGPoint(x: x, y: y)
            button = UIButton(frame: CGRect(origin: point, size: size))
            x = x + button.frame.width + CGFloat(23)
            button.setImage(UIImage(named: namesButton[i]), for: .normal)
            array.append(button)
            self.addSubview(button)
        }
        if runAnim {
            addOneButton()
            timer()
        } else {
            animStop(buttons: array)
        }

    }
    
    func timer() {
//        let timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector (anim3), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(withTimeInterval: 0.35, repeats: false) { [self] (timer) in
            anim3(buttons: array)
        }
    }
    
    
    /*
    func animStart(button: UIButton) {
        
//            UIView.animate(withDuration: 25) {
        UIView.animate(withDuration: 25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveLinear) {
                button.transform = CGAffineTransform(translationX: -1200, y: 0)
            } completion: { [self] (true) in
        }
    }
    */
    
    func anim1(button: UIButton) {
        
//            UIView.animate(withDuration: 25) {
        UIView.animate(withDuration: 15, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveLinear) {
                button.transform = CGAffineTransform(translationX: -950, y: 0)
            } completion: { (true) in
        }
    }
    
    func addOneButton() {
        let y = self.size.height - 44
        var i = 0
        timerAnime = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true) { [self] (timer) in
            i += 1
            if i == 13 { i = 0}
            self.button = UIButton(frame: CGRect(origin: CGPoint(x: 450, y: y), size: size))
//            self.button.backgroundColor = .red
            self.button.setImage(UIImage(named: namesButton[i]), for: .normal)
            anim1(button: self.button)
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                if !array.isEmpty && !isStoped {
                    array.first?.removeFromSuperview()
                    array.remove(at: 0)
                }
            }
            array.append(self.button)
            self.addSubview(self.button)
        }
    }
    
    
    @objc func anim3(buttons: [UIButton]) {

        buttons.forEach { (button) in
            animButtonAbout = UIView.animate(withDuration: 100, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveLinear) {
//                button.transform = CGAffineTransform(translationX: -1500, y: 0)
                var t = CGAffineTransform.identity
                t = t.translatedBy(x: -1000, y: 0)
                button.transform = t
            } completion: { (true) in
                button.removeFromSuperview()
            }
        }
    }
    
    func animStop(buttons: [UIButton]) {
        var xx: CGFloat = -28
        
        for button in 0..<buttons.count {
            animButtonAbout = UIView.animate(withDuration: 4, delay: 0.37, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveLinear) {
                var t = CGAffineTransform.identity
                t = t.translatedBy(x: -self.frame.width + xx, y: 0)
                xx = xx + self.frame.width / 26
               buttons[button].transform = t
            } completion: { (true) in
                if button == 0 {
                    self.chengeToRed(buttons: buttons)
                }
            }
        }
    }
    
    func chengeToRed(buttons: [UIButton]) {
        for i in 0..<buttons.count {
            if i == 1 || i == 3 {
                let redButton = UIButton(frame: buttons[i].frame)
                redButton.setImage(UIImage(named: "redSigarets"), for: .normal)
                redButton.layer.cornerRadius = 8
                buttons[i].removeFromSuperview()
                buttons[i].setImage(UIImage(named: "greySigarets"), for: .normal)
                addSubview(redButton)
                addSubview(buttons[i])
                if i == 1 {
                    UIView.animate(withDuration: 1) {
                        buttons[i].transform = CGAffineTransform(translationX: -self.frame.width, y: 8)
                    } completion: { (tru) in
                        UIView.animate(withDuration: 1) {
                            
                            buttons[i].setImage(UIImage(named: "redSigarets"), for: .normal)
                            redButton.setImage(UIImage(named: "greySigarets"), for: .normal)
                        }
                    }
                } else {
                    UIView.animate(withDuration: 1) {
                        buttons[i].transform = CGAffineTransform(translationX: -self.frame.width + 28, y: 8)
                    } completion: { (tru) in
                        buttons[i].setImage(UIImage(named: "redSigarets"), for: .normal)
                        redButton.setImage(UIImage(named: "greySigarets"), for: .normal)
                    }
                }
            }
        }
    }
    
    func stopButtonAbout() {
        isStoped = true
        array.removeAll()
        timerAnime.invalidate()
        createButton(runAnim: false)
    }

    
}
