//
//  BitMaskCategory.swift
//  SmokingOff
//
//  Created by DfStudio on 23.06.2021.
//

import Foundation


import SpriteKit

extension SKPhysicsBody {
    var category: BitMaskCategory {
        get {
            return BitMaskCategory(rawValue: self.categoryBitMask)
        }
        set {
            self.categoryBitMask = newValue.rawValue
        }
    }
}

struct BitMaskCategory: OptionSet {

    let rawValue: UInt32
    
    static let line = BitMaskCategory(rawValue: 1 << 0)
    static let triger1 = BitMaskCategory(rawValue: 1 << 1)
    static let triger2 = BitMaskCategory(rawValue: 1 << 2)
    static let triger3 = BitMaskCategory(rawValue: 1 << 3)
    static let triger4 = BitMaskCategory(rawValue: 1 << 4)
    static let triger5 = BitMaskCategory(rawValue: 1 << 5)
    static let triger6 = BitMaskCategory(rawValue: 1 << 6)
    static let triger7 = BitMaskCategory(rawValue: 1 << 7)
    static let triger8 = BitMaskCategory(rawValue: 1 << 8)
    
    static let bitMasksTriger = [triger1, triger2, triger3, triger4, triger5, triger6, triger7, triger8]
}
