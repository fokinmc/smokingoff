//
//  OneWeekViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 06.04.2021.
//

import UIKit



class OneWeekViewController: UIViewController, UIScrollViewDelegate {

    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var images = ["avatarBot", "avatarBot", "avatarBot", "avatarBot", "avatarBot", "avatarBot"]
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var contentWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        pageControl.transform = CGAffineTransform(scaleX: 3, y: 3)
        pageControl.numberOfPages = images.count
        title = "toOneWeek toOneWeek toOneWeek"
        
        for image in 0..<images.count {
            let imageToDisplay = UIImage(named: images[image])
            let imageView = UIImageView(image: imageToDisplay)
            imageView.contentMode = .scaleAspectFit

            scrollView.addSubview(imageView)
            contentWidth += view.frame.width
            
            let xCoordinate = view.frame.minX + (view.frame.width - 40) * CGFloat(image)
            
            imageView.frame = CGRect(x: xCoordinate, y: 0, width: scrollView.frame.height + 40, height: view.frame.width)

            
        }
        
        scrollView.contentSize = CGSize(width: contentWidth, height: view.frame.height / 3)
        /*
        pageControl.numberOfPages = images.count
        
        for index in 0..<images.count {
            frame.origin.x = scrollView.frame.size.width * CGFloat(index)
            frame.size = scrollView.frame.size
            
            let imageView = UIImageView(frame: frame)
            imageView.image = UIImage(named: images[index])
            self.scrollView.addSubview(imageView)
        }
        scrollView.contentSize = CGSize(width: (scrollView.frame.size.width * CGFloat(images.count)), height: scrollView.frame.size.height)
        
        */
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / CGFloat(375))
    }
    
    /*
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.width
        pageControl.currentPage = Int(pageNumber)
    }
    */


}
