//
//  SettingsViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 15.05.2021.
//

import UIKit

class SettingsViewController: UIViewController {
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var notVeiw: UIView!
    @IBOutlet weak var viewDisNot: UIView!
    @IBOutlet weak var otherView: UIView!
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var appVersionLabel: UILabel!
    
    @IBOutlet weak var editName: UIButton!
    @IBOutlet weak var editEmail: UIButton!
    @IBOutlet weak var editDate: UIButton!

    let useDef = UserDefManager.shared
    var appVersion: String!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppearence()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        guard let name = nameField.text else { return }
        useDef.saveName(name: name)
        
        guard let email = emailField.text else { return }
        useDef.saveEmail(email: email)
        
        guard let smokingOff = dateField.text else { return }
        useDef.saveSmokingOff(smokingOff: smokingOff)
    }
    
    func setupAppearence() {
        title = "Settings"
        shadow(view: viewLabel)
        shadow(view: notVeiw)
        shadow(view: viewDisNot)
        shadow(view: otherView)
        
        nameField.text = useDef.getName()
        emailField.text = useDef.getEmail()
        appVersionLabel.text = appVersion
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupAppearence()
    }
    
    
    
    func shadow(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowRadius = 5
    }
    
    @IBAction func editNameAction(_ sender: Any) {
        nameField.isEnabled = true
        nameField.isHighlighted = true
    }
    
    @IBAction func editEmailAction(_ sender: Any) {
        emailField.isEnabled = true
    }
    
    @IBAction func editDateAction(_ sender: Any) {
        dateField.isEnabled = true
    }
    
}
