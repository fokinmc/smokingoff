//
//  WithTime.swift
//  SmokingOff
//
//  Created by DfStudio on 25.06.2021.
//


import UIKit

class WithTime: UIView {

    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!

    @IBOutlet weak var btn11: UIButton!
    @IBOutlet weak var btn22: UIButton!
    @IBOutlet weak var btn33: UIButton!
    @IBOutlet weak var btn44: UIButton!

    var timer: Timer!
    var tapInWithTime: (() -> ())?
    var i = 0
    var countTap = 0
    var button = UIButton()
    let size = CGSize(width: 60, height: 60)

    
    var view: UIView!
    var nibName: String = "WithTime"
    
     
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

    func setup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 10
        view.clipsToBounds = false
        
//        shadow(view: view)
        addSubview(view)
        
        btn11.isHidden = true
        btn22.isHidden = true
        btn33.isHidden = true
        btn44.isHidden = true

    }
    
    
    func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.1
            view.layer.shadowOffset = CGSize(width: 0, height: 6)
            view.layer.shadowRadius = 8
    }
    
     
    func nextView() {
        countTap += 1
        if countTap == 3 {
            btn1.isHidden = true
            btn2.isHidden = true
            btn3.isHidden = true
            
            btn11.isHidden = false
            btn22.isHidden = false
            btn33.isHidden = false
            btn44.isHidden = false
            
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(updateButton), userInfo: nil, repeats: true)
        }
        
    }
    
    @objc func updateButton() {
        let array = [btn11, btn22, btn33, btn44]
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            
            array.forEach { btn in
                btn?.isEnabled = false
                if self.i == 0 {
                    self.btn11.isEnabled = true
                }
            }
            
            if self.i > 0 && self.i < 4 {
                array[self.i]!.isEnabled = true
            }
            if self.i > 0 && self.i < 4 {
                array[self.i - 1]!.isEnabled = false
            }
            self.i += 1
        }
        if i == 4 {
            timer.invalidate()
        }
    }
    
    //actions
    @IBAction func tap1(_ sender: Any) {
        btn1.setImage(UIImage(named: "redSigarets"), for: .normal)
        tapInWithTime?()
        nextView()
    }
    
    @IBAction func tap2(_ sender: Any) {
        btn2.setImage(UIImage(named: "redSigarets"), for: .normal)
        tapInWithTime?()
        nextView()
    }
    
    @IBAction func tap3(_ sender: Any) {
        btn3.setImage(UIImage(named: "redSigarets"), for: .normal)
        tapInWithTime?()
        nextView()
    }

    
}
