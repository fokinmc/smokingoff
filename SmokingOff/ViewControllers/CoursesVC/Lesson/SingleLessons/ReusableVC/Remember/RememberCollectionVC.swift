//
//  RememberCollectionVC.swift
//  SmokingOff
//
//  Created by DfStudio on 04.08.2021.
//

import UIKit
import AdvancedPageControl


class RememberCollectionVC: UIViewController, UICollectionViewDataSource,
                            UICollectionViewDelegate,
                            UICollectionViewDelegateFlowLayout {
    var lesson: Lesson!

    //    var text = ["Вы покуриваете, потому что состоите в гнёте иллюзий, поэтому не видите настоящую картину происходящего. Но в ближайшее время, Вы уже начнёте по-другому на это смотреть.",
//                "Для того, чтобы бросить курить легко, следует  усилить осознанность по отношению к сигаретам и изменить представление о себе.",
//                "Ваши прежние неудачные попытки с бросанием  курить не имеют никакого значения. Вы просто выбрали неверный подход",
//                "Не пытайтесь надавить на себя: вы не обязаны бросать курить. Не водите  себя за нос: вы хотите курить.",
//                "Вы идёте осознанно к жизни без сигарет, следовательно не нужно ставить себе запретов, и бороться с собой. Добровольно отказаться— вот что нужно Вам."]
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageControl1: AdvancedPageControlView!
    
    func numberOfSections(in _: UICollectionView) -> Int {
        return 5
    }

    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RememberViewCell
//        cell.textLabel.text = lesson.remember[indexPath.section].text
        cell.setupCell(view: self.view)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout _: UICollectionViewLayout,
                        sizeForItemAt _: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: self.view.frame.height / 1.28)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width

        pageControl1.setPage(Int(round(offSet / width)))

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl1.drawer = ExtendedDotDrawer(numberOfPages: 5,
                                                height: 8,
                                                width: 8,
                                                space: 8,
                                                indicatorColor: #colorLiteral(red: 0.9878367782, green: 0.6144195199, blue: 0.3506464958, alpha: 1),
                                                dotsColor: #colorLiteral(red: 0.9014499187, green: 0.9057919979, blue: 0.9187776446, alpha: 1),
                                                isBordered: false,
                                                borderWidth: 0.0,
                                                indicatorBorderColor: .clear,
                                                indicatorBorderWidth: 0.0)
                                                    
//        pageControl1.drawer = ExtendedDotDrawer(numberOfPages: <#T##Int?#>, height: <#T##CGFloat?#>, width: <#T##CGFloat?#>, space: <#T##CGFloat?#>, raduis: <#T##CGFloat?#>, currentItem: <#T##CGFloat?#>, indicatorColor: <#T##UIColor?#>, dotsColor: <#T##UIColor?#>, isBordered: <#T##Bool#>, borderColor: <#T##UIColor#>, borderWidth: <#T##CGFloat#>, indicatorBorderColor: <#T##UIColor#>, indicatorBorderWidth: <#T##CGFloat#>)
        

    }
    
    func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.1
            view.layer.shadowOffset = CGSize(width: 0, height: 6)
            view.layer.shadowRadius = 8
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTask" {
            let vc = segue.destination as! TaskVC
            vc.lesson = lesson
        }
        
    }
    
}
