//
//  QuoteViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 05.04.2021.
//

import UIKit

class QuoteViewController: UIViewController {

    let useDef = UserDefManager.shared

    
    override func viewDidLoad() {
        super.viewDidLoad()
        Appearance.navigationDecor(vc: self)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextAction(_ sender: Any) {
        useDef.alreadyGreeting()
        guard let nextVC = storyboard?.instantiateViewController(identifier: "tabbar") else { return }
//        navigationController?.present(nextVC, animated: true, completion: nil)
//        present(nextVC, animated: true, completion: nil)
        navigationController?.pushViewController(nextVC, animated: true)

    }

}
