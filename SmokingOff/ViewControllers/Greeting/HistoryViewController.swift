//
//  HistoryViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 02.04.2021.
//

import UIKit
import Toaster

class HistoryViewController: UIViewController {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var name4: UILabel!



    @IBOutlet weak var ok1: UIImageView!
    @IBOutlet weak var ok2: UIImageView!
    @IBOutlet weak var ok3: UIImageView!
    @IBOutlet weak var ok4: UIImageView!

    
//    @IBOutlet weak var oneYearLabel: UILabel!
//    @IBOutlet weak var fiveYearLabel: UILabel!
//    @IBOutlet weak var tenYearLabel: UILabel!
//    @IBOutlet weak var moreLabel: UILabel!
//    @IBOutlet weak var sliderView: UISlider!
    
    let useDef = UserDefManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Appearance.navigationDecor(vc: self)

        

    }
    
    func buttonSelect(buttons: [UIButton], images: [UIImageView], names: [UILabel]) {
        buttons[0].backgroundColor = #colorLiteral(red: 0.9878367782, green: 0.6144195199, blue: 0.3506464958, alpha: 1)
        buttons[1].backgroundColor = #colorLiteral(red: 0.9640881419, green: 0.9686412215, blue: 0.9772089124, alpha: 1)
        buttons[2].backgroundColor = #colorLiteral(red: 0.9640881419, green: 0.9686412215, blue: 0.9772089124, alpha: 1)
        buttons[3].backgroundColor = #colorLiteral(red: 0.9640881419, green: 0.9686412215, blue: 0.9772089124, alpha: 1)

        
        images[0].image = UIImage(named: "okYellow")
        images[1].image = UIImage(named: "")
        images[2].image = UIImage(named: "")
        images[3].image = UIImage(named: "")


        names[0].textColor = .white
        names[1].textColor = .black
        names[2].textColor = .black
        names[3].textColor = .black
        
    }

     
    @IBAction func action1(_ sender: Any) {
        useDef.saveHistory(name: NSLocalizedString("1 year", comment: ""))
        buttonSelect(buttons: [button1, button2, button3, button4], images: [ok1, ok2, ok3, ok4], names: [name1, name2, name3, name4])
    }
    
    @IBAction func action2(_ sender: Any) {
        useDef.saveHistory(name: NSLocalizedString("5 years", comment: ""))
        buttonSelect(buttons: [button2, button1, button3, button4], images: [ok2, ok1, ok3, ok4], names: [name2, name1, name3, name4])
    }
    
    @IBAction func action3(_ sender: Any) {
        useDef.saveHistory(name: NSLocalizedString("10 years", comment: ""))
        buttonSelect(buttons: [button3, button2, button1, button4], images: [ok3, ok2, ok1, ok4], names: [name3, name2, name1, name4])
    }
    
    @IBAction func action4(_ sender: Any) {
        useDef.saveHistory(name: NSLocalizedString("more than 10 years.", comment: ""))
        buttonSelect(buttons: [button4, button2, button3, button1], images: [ok4, ok2, ok3, ok1], names: [name4, name2, name3, name1])
    }

    
    
    @IBAction func nextAction(_ sender: Any) {
        print("==\(useDef.getHistory())==")
        if useDef.getHistory() != "" {
        guard let nextVC = storyboard?.instantiateViewController(identifier: "greetingVC") else { return }
            navigationController?.pushViewController(nextVC, animated: true)
        } else {
            Toast(text: NSLocalizedString("Coose your smoking history", comment: ""), delay: 1, duration: 3).start()
        }
    }
    

    
    
    /*
    func updateSlider() -> Float {
        var value: Float = 0
        if sliderView.value > 3.3 {
            value = 3.7
        } else if sliderView.value <= 3.3 && sliderView.value > 2.5 {
            value = 3
        } else if sliderView.value <= 2.5 && sliderView.value > 1.6 {
            value = 2.1
        } else if sliderView.value <= 1.6 && sliderView.value > 0 {
            value = 1.33
        }
        
        
        return value
    }
    
    
    
    @IBAction func sliderAction(_ sender: Any) {
        if sliderView.isTracking {
            if sliderView.value > 3.3 {
                moreLabel.font = UIFont(name: "Montserrat-SemiBold", size: 17)
                useDef.saveHistory(name: "more then 10 years")
            } else if sliderView.value <= 3.3 && sliderView.value > 2.5 {
                tenYearLabel.font = UIFont(name: "Montserrat-SemiBold", size: 17)
                moreLabel.font = UIFont(name: "Montserrat", size: 17)
                useDef.saveHistory(name: "10 years")

            } else if sliderView.value <= 2.5 && sliderView.value > 1.6 {
                fiveYearLabel.font = UIFont(name: "Montserrat-SemiBold", size: 17)
                moreLabel.font = UIFont(name: "Montserrat", size: 17)
                tenYearLabel.font = UIFont(name: "Montserrat", size: 17)
                useDef.saveHistory(name: "5 years")

            } else if sliderView.value <= 1.6 && sliderView.value > 0 {
                oneYearLabel.font = UIFont(name: "Montserrat-SemiBold", size: 17)
                moreLabel.font = UIFont(name: "Montserrat", size: 17)
                tenYearLabel.font = UIFont(name: "Montserrat", size: 17)
                fiveYearLabel.font = UIFont(name: "Montserrat", size: 17)
                useDef.saveHistory(name: "1 years")

            }
            
        } else {
            sliderView.setValue(updateSlider(), animated: true)
        }
    }
    */
}
