//
//  CollectionViewCell.swift
//  SmokingOff
//
//  Created by DfStudio on 04.08.2021.
//

import UIKit

class RememberViewCell: UICollectionViewCell {
    
    @IBOutlet weak var swadowView: UIView!
    @IBOutlet weak var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(view: UIView) {
        shadow(view: swadowView)
        
//        if view.frame.height < 680 {
//            whiteTop.constant = 98
//        }
    }
    
    func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.1
            view.layer.shadowOffset = CGSize(width: 0, height: 6)
            view.layer.shadowRadius = 8
    }

}



