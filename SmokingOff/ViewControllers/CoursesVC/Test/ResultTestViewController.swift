//
//  ResultTestViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 18.05.2021.
//

import UIKit

class ResultTestViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var yourResultLabel: UILabel!
    @IBOutlet weak var menImage: UIImageView!
    @IBOutlet weak var nextButton: UIButton!

    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var starHalf1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var starHalf2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var starHalf3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var starHalf4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var starHalf5: UIImageView!

    
    var text: String!
    var countOfCurrentAnswer: Int!
    
    var stars: [UIImageView]!
    var numberStarForShow = 1
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stars = [starHalf1, star1,
                 starHalf2, star2,
                 starHalf3, star3,
                 starHalf4, star4,
                 starHalf5, star5]
        setupEmptyStar()
        
        tabBarController?.tabBar.isHidden = true
        if countOfCurrentAnswer >= 8 {
            yourResultLabel.text = "Your test result: \(countOfCurrentAnswer!) of 10"
            resultLabel.text = "Congratulations!"
            textLabel.text = "You have successfully completed your week of training!"
            menImage.image = UIImage(named: "winmen")
            nextButton.setTitle("START NEXT WEEK", for: .normal)
            
        } else {
            yourResultLabel.text = "Your test result: \(countOfCurrentAnswer!) of 10"
            resultLabel.text = "Try again..."
            textLabel.text = "You need to read the information from the course again and take the test"
            nextButton.setTitle("REPEAT WEEK", for: .normal)


        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector (starSettings), userInfo: nil, repeats: true)

    }
    
    func setupEmptyStar() {
        for i in 1...10 {
            if i % 2 != 0 {
                stars[i - 1].image = UIImage(named: "")
            }
        }
    }
    
  
    
    
    
    @objc func starSettings() {
        
        if numberStarForShow % 2 == 0 {
            stars[numberStarForShow - 1].image = UIImage(named: "wholeStar")
        } else {
            stars[numberStarForShow - 1].image = UIImage(named: "halfStar")
        }
        
        numberStarForShow += 1
        if numberStarForShow == countOfCurrentAnswer + 1 {
            timer.invalidate()
        }
    }
}
