//
//  CoreDataManager.swift
//  SmokingOff
//
//  Created by DfStudio on 07.07.2021.
//

import UIKit
import CoreData

class CoreDataManager {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var messages: [Message] = []
    var message: Message!
    
    func getCoreData() -> [Message] {
        
//        let stringNumber = personNumber?.description

        let fetchRequest: NSFetchRequest<Message> = Message.fetchRequest()
        
//        if personNumber != nil {
//            fetchRequest.predicate = NSPredicate()
//        }
        
        do {
            messages = try context.fetch(fetchRequest)
        } catch {
            print(error.localizedDescription)
        }
        
        return messages
    }
    
    
    func saveNewMessage(isBot: Bool, name: String, date: String, text: String) {
        //        let count = self.getCountOfPersons()
        
        message = Message(context: context)
        message.isBot = isBot
        message.name = name
        message.date = date
        message.text = text
        
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Error: \(error), userInfo \(error.userInfo)")
        }
        
    }
    
}
