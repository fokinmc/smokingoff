//
//  PresentationTrigger.swift
//  SmokingOff
//
//  Created by DfStudio on 22.06.2021.
//

import UIKit

class PresentationTrigger: UIView {

    
    
    var button = UIButton()
    let size = CGSize(width: 60, height: 60)

    
    var view: UIView!
    var nibName: String = "PresentationTrigger"
    
    var mult: Mult! = nil
    let constants = Constants()

     
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

    func setup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 10
        view.clipsToBounds = false
        mult = Mult(name: constants.professorTriggerPresentation[0])
        mult.startAnime(addTo: view, mode: .loop, isCenter: false)
        
//        shadow(view: view)
        addSubview(view)
    }
    
    func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.1
            view.layer.shadowOffset = CGSize(width: 0, height: 6)
            view.layer.shadowRadius = 8
    }
    
     
   
    
    //actions
    
}
