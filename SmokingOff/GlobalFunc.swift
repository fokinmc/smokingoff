//
//  GlobalFunc.swift
//  SmokingOff
//
//  Created by DfStudio on 26.07.2021.
//

import UIKit

class GlobalFunc {
    var mult: Mult! = nil

    func addAnim(view: UIView, name: String, isRepeat: Bool) {
        
        print("start anime name", name)
        
        mult = Mult(name: name)
        if isRepeat {
            mult.startAnime(addTo: view, mode: .loop, isCenter: false)
        } else {
            mult.startAnime(addTo: view, mode: .playOnce, isCenter: false)
        }
    }
    
    func stopAnim(removeAmine: Bool) {
        print("stop in global")
        if mult != nil {
            mult.animationView.stop()
            
            if removeAmine {
                mult.animationView.removeFromSuperview()
            }
        }
    }
    
    func onlyRemoveAnim() {
        mult.animationView.removeFromSuperview()
    }
    
    func pauseAnim() {
        mult.animationView.pause()
    }
    
    class func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.14
            view.layer.shadowOffset = CGSize(width: 0, height: 4)
            view.layer.shadowRadius = 5
    }
}
