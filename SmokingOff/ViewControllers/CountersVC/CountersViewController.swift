//
//  CountersViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 14.04.2021.
//

import UIKit

class CountersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate {

    

    @IBOutlet weak var leadingCollection: NSLayoutConstraint!
    @IBOutlet weak var men2Center: NSLayoutConstraint!
    @IBOutlet weak var leftBtnV: UIButton!
    @IBOutlet weak var rightBtnV: UIButton!


    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var line: UIView!
    @IBOutlet weak var imageMen1: UIImageView!
    @IBOutlet weak var imageMen2: UIImageView!

    let useDef = UserDefManager.shared
    let timeMeneger = TimeMeneger()
    
    var swipeRightGesture = UISwipeGestureRecognizer()
    var swipeLeftGesture = UISwipeGestureRecognizer()

    let imageArray = ["Time", "Health", "Progress", "Money", "Ecology", "Nicotine", "Lings", "Body", "Welfare"]
    let imageTableArray = ["lesson1", "lesson2", "lesson3", "week2", "lesson5"]
    let nameCellArray = ["DATE OF QUITTING SMOKING:", "MONEY SAVED:", "NOT SMOKED CIGARETTES", "SAVED LIVE TIME:", "HOW MACH DID NOT SPEND:"]
    
    var cellIsOpen = false
    
    var statusCell = [false, false, false, false, false]

    //statistics
    var allMoney: Int!
    var allPack: Int!
    var allSig: Int!


    var sigOfDay: Int!
    var priceOfPack: Int!
    var pricePack: Int!
    var sigInPack: Int!
    
    var elapsedDay: Int!
    var elapsedHour: Int!
    var elapsedMinute: Int!
    var elapsedSecond: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeMeneger.useCalendar()
        
        navigationController?.navigationBar.topItem?.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        table.separatorStyle = .none
        leadingCollection.constant = self.view.frame.width
        men2Center.constant = self.view.frame.width
        
        
        setupSwipe()
        
        view.addGestureRecognizer(swipeRightGesture)
        
        calculateSig()
    }
    
    func setupSwipe() {
        swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeftAction))
        swipeLeftGesture.direction = .left
        
        swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeRightAction))
        swipeRightGesture.direction = .right
        
        view.addGestureRecognizer(swipeRightGesture)
        view.addGestureRecognizer(swipeLeftGesture)

    }
    
    @objc func swipeLeftAction() {
        toCollection()
    }
    
    @objc func swipeRightAction() {
        toTable()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func calculateSig() {
        pricePack = useDef.getPricePack()
        sigOfDay = useDef.getCountOfDay()
        sigInPack = useDef.getNumberInPack()
        
        if sigOfDay == 0 {
            sigInPack = 1
            sigOfDay = 1
        }
        
        let priceOneSig = pricePack / sigInPack
        let moneyOneDay = priceOneSig * sigOfDay
        
        let elapsedArray = timeMeneger.getElapsedTime(time: timeMeneger.dateStart - timeMeneger.dateNow)
        elapsedDay = -elapsedArray[0]
        
        allMoney = elapsedDay * moneyOneDay
        allPack = elapsedDay * (sigInPack / sigOfDay)
        allSig = elapsedDay * sigOfDay
         
        
        print(elapsedDay, "elapsedDay")

        print(priceOneSig, "priceOneSig")
        print(sigOfDay, "sigOfDay")
        print(moneyOneDay, "moneyOneDay")
        print(allMoney, "allMoney")

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "counterTableViewCell", for: indexPath) as! TableCounterViewCell
        
        cell.openImage.image = UIImage(named: "okGreen")
        cell.cellImage.image = UIImage(named: imageTableArray[indexPath.row])
        cell.nameCell.text = nameCellArray[indexPath.row]


        
        switch indexPath.row {
        case 0:
            cell.descriptionCell.text = useDef.getStartDateOfSmoking()

            cell.label11.text = "_"
            cell.label22.text = "_"
            cell.label33.text = "_"
            cell.label44.text = "_"
            
        case 1:
            cell.descriptionCell.text = "\(allMoney ?? 0) rubles"

            cell.label11.text = "\(allPack ?? 0) paks"
            cell.label22.text = "\(allSig ?? 0) cigarettes"
            cell.label33.text = "\(pricePack ?? 0) rubles"
            cell.label44.text = "\(allMoney ?? 0) rubles"
        case 2:
            cell.descriptionCell.text = "\(allSig ?? 0) pieces"
        default:
            cell.descriptionCell.text = useDef.getStartDateOfSmoking()

            cell.label11.text = "_"
            cell.label22.text = "_"
            cell.label33.text = "_"
            cell.label44.text = "_"
        }
        
        if statusCell[indexPath.row] == false {
            cell.close()
        } else {
            cell.open()
        }
        
        return cell
    }
    
    func toTable() {
        UIView.animate(withDuration: 1) { [self] in
            table.transform = CGAffineTransform(translationX: 0, y: 0)
            collection.transform = CGAffineTransform(translationX: 0, y: 0)
            line.transform = CGAffineTransform(translationX: 0, y: 0)
            imageMen1.transform = CGAffineTransform(translationX: 0, y: 0)
            imageMen2.transform = CGAffineTransform(translationX: 0, y: 0)
        }
        rightBtnV.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        leftBtnV.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    }
    
    func toCollection() {
        UIView.animate(withDuration: 1) { [self] in
            table.transform = CGAffineTransform(translationX: -table.frame.width, y: 0)
            collection.transform = CGAffineTransform(translationX: -table.frame.width + 22, y: 0)
            line.transform = CGAffineTransform(translationX: self.view.frame.width / 2, y: 0)
            imageMen1.transform = CGAffineTransform(translationX: -self.view.frame.width, y: 0)
            imageMen2.transform = CGAffineTransform(translationX: -self.view.frame.width, y: 0)
        }
        rightBtnV.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        leftBtnV.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        statusCell[indexPath.row] = !statusCell[indexPath.row]
        table.reloadRows(at: [indexPath], with: .automatic)
    }
    
    @IBAction func rightBtn(_ sender: Any) {
        toCollection()
    }
    
    @IBAction func leftBtn(_ sender: Any) {
        toTable()
    }
}


extension CountersViewController {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "collectionCounterCell", for: indexPath) as! CollectionCounterCell
        cell.image.image = UIImage(named: imageArray[indexPath.row])
        cell.label.text = imageArray[indexPath.row]
        cell.setupSize(selfWidth: self.view.frame.width)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(identifier: "TowCounterVC") as! TowCounterVC
        vc.name = imageArray[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
