//
//  DiscomfortView.swift
//  SmokingOff
//
//  Created by DfStudio on 22.06.2021.
//

import UIKit
import SpriteKit
import GameplayKit

 class Lesson1ViewScene: UIView {

//    @IBOutlet weak var indicator: UIImageView!
//    @IBOutlet weak var indicatorUse: UIImageView!
//    @IBOutlet weak var constreint: NSLayoutConstraint!
    @IBOutlet weak var skView: SKView!
    
    var finishSceneVeiw: (() -> ())?

    var button = UIButton()
    let size = CGSize(width: 60, height: 60)

    
    var view: UIView!
    var nibName: String = "Lesson1ViewScene"
    
    var angle: CGFloat = -60
    var rad: CGFloat!
//    var mult: Mult! = nil
//    let constants = Constants()

     
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

    func setup() {

        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 10
        view.clipsToBounds = false
        
        addSubview(view)
        
//        setupIndicator()
        setupSK()
    }
    
    func setupSK() {
        if let view = skView as! SKView? {
            
            let scene = L1Scene(size: self.view.bounds.size)
            scene.backgroundColor = .clear
            scene.scaleMode = .aspectFill
            
            scene.finishScene = { [self] in
                finishSceneVeiw?()
            }
            
            // Present the scene
            view.presentScene(scene)
            view.ignoresSiblingOrder = true
            view.showsFPS = false
            view.showsNodeCount = false
        }
    }
    
//    func setupIndicator() {
//        rad = angle * (.pi / 180)
//        UIView.animate(withDuration: 1) { [self] in
//            indicator.transform = CGAffineTransform(rotationAngle: rad)
//        }
//        indicator.addSubview(indicatorUse)
//    }
    
    func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.1
            view.layer.shadowOffset = CGSize(width: 0, height: 6)
            view.layer.shadowRadius = 8
    }
    
     
   
    
    //actions
    
    func rotateIndicator() {
        angle = angle - 8
        rad = angle * (.pi / 180)

//        UIView.animate(withDuration: 1) { [self] in
//            indicator.transform = CGAffineTransform(rotationAngle: rad)
//        }
    }
    
   
    
}
