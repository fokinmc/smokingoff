//
//  LessonsViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 14.04.2021.
//

import UIKit
//import Lottie

class LessonsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var weekNameLabel: UILabel!
    @IBOutlet weak var weekNumberLabel: UILabel!
    
    let useDef = UserDefManager.shared
    var weekName: String!
    var weekNumber = Int()
    var manager = CreatingManager()
    var tests: [Test]!
    var nameJSONTests: String!
    var nameJSONWeeks: String!
    var lessons: [Lesson]!
    
    //    var mult: Mult! = nil
    //    let constants = Constants()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Appearance.navigationClear(vc: self)

        self.navigationController?.setNavigationBarHidden(true, animated: false)

        let locale = NSLocale.preferredLanguages.first!
        print(locale)
        if locale.hasPrefix("ru") { //может сделать сюда не выбор по local а оставить только en и перереводить как стандартный текст !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            nameJSONTests = "TestsRU"
        } else {
            nameJSONTests = "TestsRU" //"TestsEN"
        }
        
        
        manager.getTest(file: nameJSONTests, completion: { [weak self] (result) in
            self!.tests = result
        })
        
        
        table.separatorStyle = .none
        weekNameLabel.text = weekName
        weekNumberLabel.text = NSLocalizedString("Неделя \(weekNumber + 1) из 4", comment: "")
        title = ""
        print(tabBarController?.tabBar.frame.height)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lessons.count
    }
    
    let images = ["lesson1", "lesson2", "lesson3", "lesson4", "lesson5", "", ]
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "lessonCell", for: indexPath) as! LessonTableViewCell
        cell.manImage.image = UIImage(named: images[indexPath.row])
        cell.nameLesson.text = lessons[indexPath.row].name
        
//        let isLessonFinished = useDef.getFinishLessonId(lessonId: lessons[indexPath.row].id)
        let isLessonFinished = indexPath.row + 1

        cell.configure(isFinished: isLessonFinished, index: indexPath, numberWeek: indexPath.row + 1)
        //поменять логику присвоения status
        
//        mult = Mult(name: constants.mainAinimeArray[indexPath.row])
//        mult.startAnime(addTo: cell.animView, mode: .loop)

        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 5 { // to test temp
            guard let vc = storyboard?.instantiateViewController(identifier: "beginTest") as? BeginTestViewController else { return }
            navigationController?.pushViewController(vc, animated: true)
            vc.currentTestNumber = weekNumber
            vc.test = tests[weekNumber].test
        } else {
            guard let vc = storyboard?.instantiateViewController(identifier: "startLesson") as? StartLesson else { return }
            navigationController?.pushViewController(vc, animated: true)
            vc.lesson = lessons[indexPath.row]
            vc.lessonNumber = indexPath.row
        }
        

        
        
        
        /* для выбра разных старт экранов
        
        switch indexPath.row {
        case 0:
            guard let vc = storyboard?.instantiateViewController(identifier: "startLesson_1_day") else { return }
            navigationController?.pushViewController(vc, animated: true)
        case 1:
            guard let vc = storyboard?.instantiateViewController(identifier: "StartLesson") else { return }
            navigationController?.pushViewController(vc, animated: true)
        case 2:
            break
        case 3:
            break
        default:
            break
        }
        */
        
//        if indexPath.row == 0 {
//            guard let vc = storyboard?.instantiateViewController(identifier: "StartLesson") else { return }
//            navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    func setupAnimeView(nameAnime: String) {
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "toBeginnerTest" {
//            guard let index = table.indexPathForSelectedRow else { return }
//
//            if index.row == 0 {
//                guard let vc = storyboard?.instantiateViewController(identifier: "oneLesson") else { return }
//                navigationController?.pushViewController(vc, animated: true)
//            } else if index.row == Constants.lessons[weekNumber].count - 1 {
//                let vc = segue.destination as! BeginTestViewController
//                vc.currentTestNumber = weekNumber
//                vc.test = tests[weekNumber].test
//            }
//        }
//    }
    
    /*
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        let cell = table.dequeueReusableCell(withIdentifier: "lessonCell", for: indexPath) as! LessonTableViewCell
        
        /*
        if indexPath.row == 5 {
            guard let vc = storyboard?.instantiateViewController(identifier: "testVc") else { return nil }
            navigationController?.pushViewController(vc, animated: true)
        } else {
            guard let vc = storyboard?.instantiateViewController(identifier: "lessonVc") else { return nil }
            navigationController?.pushViewController(vc, animated: true)
        }
        
        */
        
        let vc = storyboard?.instantiateViewController(identifier: "beginTest") as! BeginTestViewController
        vc.currentTestNumber = weekNumber
        vc.test = tests[weekNumber].test
        navigationController?.pushViewController(vc, animated: true)
        return nil
    }
    */
    

}
