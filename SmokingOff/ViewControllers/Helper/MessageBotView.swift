//
//  MessageView.swift
//  SmokingOff
//
//  Created by DfStudio on 01.07.2021.
//

import UIKit



class MessageBotView: UIView {
    
    var view: UIView!
    var nibName: String = "MessageBotView"
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        print("init")

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

    func setup() {
        print("load")
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 10
        view.clipsToBounds = false
        
        shadow(view: view)
        addSubview(view)
        
        setupLablel()
    }
    
    func setupLablel() {
        var messageMarginX: CGFloat = 35
        var messageMarginY: CGFloat = 27

        nameLabel.text = "asdfad ads "
        nameLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 90, height: CGFloat.greatestFiniteMagnitude)
        nameLabel.numberOfLines = 0
        nameLabel.lineBreakMode = .byWordWrapping
        nameLabel.sizeToFit()
        nameLabel.textAlignment = .left
        nameLabel.font = UIFont(name: "Avenir Next Regular", size: 17)
        nameLabel.textColor = .black
        
        nameLabel.frame.origin.x = self.view.frame.width - nameLabel.frame.width  - messageMarginX
        nameLabel.frame.origin.y = messageMarginY
        messageMarginY += nameLabel.frame.height + 30
    }
    
    
    func shadow(view: UIView) {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.1
            view.layer.shadowOffset = CGSize(width: 0, height: 6)
            view.layer.shadowRadius = 8
    }
    
     


    
    //actions

    
}
