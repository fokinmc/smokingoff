//
//  TemplateViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 21.07.2021.
//

import UIKit

class Lesson_1_day: UIViewController {
    
    let descriptionText = ["Чтобы избавиться от сигарет - нужно подойти к этому с умом.",
                           "Осознать, что происходит с телом и психикой в процессе курения.",
                           "Подготовить фундамент для нового образа жизни.",
                           "Постепенно, бережно и мягко подвести себя к отказу от сигарет",
                           "Проанализировать и изменить образ мыслей, отношение к курению.",
                           """
В ближайшие 3 недели мы будем делать следующее:

1 Ежедневное наблюдение за собой.
2 Ведение статистики.
""",
                           """
В ближайшие 3 недели мы будем делать следующее:

1 Ежедневное наблюдение за собой.
2 Ведение статистики.
""",
                           "Одна из основных задач - повысить осознанность по отношению к курению. Сейчас вы, скорее всего, курите на автопилоте. Захотел - пошёл - покурил. Ключевое препятствие на пути к свободе - ваше представление о себе.",
                           "Сейчас вы видите себя как человека, который курит. Этот образ выстраивался длительное время и он очень крепкий. Вам нужно полностью переключиться на жизнь без сигарет, чтобы от старого образа не осталось и следа.",
                           "Сейчас вы видите себя как человека, который курит. Этот образ выстраивался длительное время и он очень крепкий. Вам нужно полностью переключиться на жизнь без сигарет, чтобы от старого образа не осталось и следа.",
                           "Сейчас вы видите себя как человека, который курит. Этот образ выстраивался длительное время и он очень крепкий. Вам нужно полностью переключиться на жизнь без сигарет, чтобы от старого образа не осталось и следа.",
                           "Сейчас вы видите себя как человека, который курит. Этот образ выстраивался длительное время и он очень крепкий. Вам нужно полностью переключиться на жизнь без сигарет, чтобы от старого образа не осталось и следа.",

                           " Скорее всего у вас уже были попытки бросить курить и что-то пошло не так. Вы можете думать, что у вас не получится. Но это неправда!",
                           "Нет никаких объективных причин, почему вы не можете бросить курить. Просто примите это как данность и всё. Вы можете отказаться от сигарет!",
                           "Вспомните все свои попытки бросить курить.",
                           "У вас сейчас есть 2 тяжелые гири: «Я не хочу больше курить» и «Я должен скорее бросить».",
                           """
                           Но посмотрим правде в глаза:
                           Вы ХОТИТЕ курить (у вас есть такое желание, причём очень сильное, и каждый час оно убедительно даёт о себе знать).  Вы НЕ должны бросать (вы вольны курить, сколько угодно, пока не появится закон, который это запрещает).
                           """,
                           """
                            "Я должен бросить".

                           Давить на себя и обвинять в неправильном поведении - неразумно.
                           Дело не в том, что вы должны или не должны, а в том, что вы выбираете. Просто однажды вы выберете жить без сигарет.
                           """,
                           """
                           “Я не хочу курить".

                           Не обманывайте себя, у вас есть тяга к сигаретам. Но ваши желания - это не вы. Вы можете осознанно выбирать свои желания, чтобы не выбирать те, которые вам вредят.
                           """,
                           "В одном из уроков вы узнаете, как избавиться от таких желаний. Не нужно им сопротивляться. Поэтому если хотите покурить — идите и курите.",
                           "Свобода от сигарет - это когда вы выбираете не курить, потому что вам не хочется. ",
                           "Если вы находитесь среди курящих людей и не хотите курить - это самый настоящий отказ. Это как раз та свобода, которая вам нужна. Вы скоро к этому придете!",
                           "",
                           "",
                           "",
                           "",
    ]
    
    let animationText = [
        "lesson 1_1", // 1
        "lesson 1_2loop", //2+
        "lesson 1_3", //3
        "lesson 1_4loop", //4+
        "lesson 1_6", //5
        "lesson 1_7loop", //6+
        "lesson 1_8", //7
        "lesson 1_9loop", // 7+
        "lesson 1_10", // 8
        "lesson 1_11", // 9
        "lesson 1_11l", // 10
        "lesson 1_12loop", // 11+
        "lesson 1_13",// 12
        "lesson 1_13loop",//12+
        "lesson 1_14",// 13
        "lesson 1_15loop",// 13+
        "lesson 1_16",// 14
        "lesson 1_17",// 14+
        "lesson 1_18",// 15
        "lesson 1_19", //15+  last of tap 17
        "",
        "",
        "lesson 1_20", //20
        "lesson 1_21",
        "lesson 1_22loop",
        "lesson 1_24",
        "lesson 1_25",
//        "lesson 1_25",
//        "lesson 1_25",
        "lesson 1_25_1L", // взрыв левой гири 2х
        "lesson 1_25_1R", // взрыв правой гири 2х
        "lesson 1_25_2L", // взрыв левой гири 1х
        "lesson 1_25_2R", // взрыв правой гири 1х
        "lesson 1_26loop",
        "lesson 1_27"
    ]
    
    let progressText = ["Что мы будем делать?", "Наблюдение и статистика", "Осознанность", "Чем я хуже?", "100 неудачных попыток", "Эмоциональные гири", "Отказ и запрет", ]
    var lesson: Lesson!
    
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var themeLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var animView: UIView!
    @IBOutlet weak var sceneView: Lesson1ViewScene!

    @IBOutlet weak var desctiprtionLabel: UILabel!
    
    @IBOutlet weak var crasBtn: UIButton!
    
    @IBOutlet weak var topText: UILabel!
    @IBOutlet weak var bigText: UILabel!
    @IBOutlet weak var dottedLineImage: UIImageView!

    @IBOutlet weak var toTopFromBig: NSLayoutConstraint!
    @IBOutlet weak var dottedUp: NSLayoutConstraint!
    @IBOutlet weak var dottedDown: NSLayoutConstraint!
    
    
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var countSiggaretLabel: UILabel!
    @IBOutlet weak var lastImage: UIImageView!

    var rbtn = UIButton()
    var lbtn = UIButton()

    let importantView = UIView()
    var tap = UITapGestureRecognizer()
    var coutnOfSigars = 0

    let globalFunc = GlobalFunc()
    
    var numberOfAnime = 0
    var numberOfTextDescription = 0
    
    
//    var numberOfAnime = 22
//    var numberOfTextDescription = 13

    var countTapBob = 0
    var backProgress: Float = 0.1428
    var numberOfProgressText = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(self.view.frame.height, "height self")
        
        Appearance.navigationClear(vc: self)
        tabBarController?.tabBar.isHidden = true
        crasBtn.isHidden = true
        crasBtn.isHidden = true
        
        topText.isHidden = true
        dottedLineImage.isHidden = true
        bigText.isHidden = true

        plusBtn.isHidden = true
        minusBtn.isHidden = true
        countSiggaretLabel.isHidden = true
        lastImage.isHidden = true
         
        navigationController?.navigationBar.topItem?.backBarButtonItem?.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //        navigationController?.n`avigationBar.topItem?.backBarButtonItem?.isEnabled = false
        //        navigationController?.navigationBar.topItem?.backBarButtonIt`em?.tintColor = .clear
        
        progressView.progress = backProgress// Float(1 / backProgress)
        
        updateTextDescription(numberText: numberOfTextDescription)

        toNextScreen()
        
        NotificationCenter.default.addObserver(self, selector: #selector(finishAnime), name: NSNotification.Name("animFinish"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(cancelAnime), name: NSNotification.Name("animCancel"), object: nil)

        
        sceneView.isHidden = true
        
        sceneView.finishSceneVeiw = { [self] in
            globalFunc.onlyRemoveAnim()
            crasBtn.isHidden = true
            nextBtn.setBackgroundImage(UIImage(named: "buttonCreen"), for: .normal)
            sceneView.isHidden = true
            numberOfAnime = 30
            numberOfTextDescription -= 1
            toNextScreen()
            nextBtn.isEnabled = true
            _ = Timer.scheduledTimer(timeInterval: 1.73, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            let yM: CGFloat = 3.32
            let imageL = UIImageView(frame: CGRect(
                                        x: self.view.frame.width / 9.8,//7.98, //7,92
                                        y: self.view.frame.height / yM,//3.32,
                                        width: 128,
                                        height: 53))
            imageL.image = UIImage(named: "refusalOk")
            animView.addSubview(imageL)
            
            let imageR = UIImageView(frame: CGRect(
                                        x: self.view.frame.width / 1.96,
                                        y: self.view.frame.height / yM,//3.32,
                                        width: 128,
                                        height: 53))
            imageR.image = UIImage(named: "prohibitionOk")
            animView.addSubview(imageR)
        }
        
        
        // delete only for go to Remember
//        toRemember()
    
    }
    
    
    func setupProgress() {
        backProgress += 0.1428
        numberOfProgressText += 1
        progressView.setProgress(backProgress, animated: true)
        themeLabel.text = progressText[numberOfProgressText]
    }
    
    func settingButton() {
        plusBtn.isHidden = false
        minusBtn.isHidden = false
        countSiggaretLabel.isHidden = false
        
        plusBtn.layer.cornerRadius = 26
        plusBtn.layer.borderWidth = 2
        plusBtn.layer.borderColor = #colorLiteral(red: 0.9968038201, green: 0.6798890233, blue: 0.421605289, alpha: 1)
        
        minusBtn.layer.cornerRadius = 26
        minusBtn.layer.borderWidth = 2
        minusBtn.layer.borderColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
        
        countSiggaretLabel.layer.cornerRadius = 26
        countSiggaretLabel.layer.borderColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
        countSiggaretLabel.layer.borderWidth = 2
    }
    
    func addTap() {
        crasBtn.isHidden = false
        crasBtn.isHidden = false
        
        tap = UITapGestureRecognizer(target: self, action: #selector(tapOnMen))
        tap.numberOfTapsRequired = 1
        animView.addGestureRecognizer(tap)
    }
    
    @objc func tapOnMen() {
        globalFunc.onlyRemoveAnim()
        //        }
        toNextScreen()
        numberOfAnime += 1
        _ = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
        if numberOfAnime == 18 {
            animView.removeGestureRecognizer(tap)
            nextBtn.isEnabled = true
            nextBtn.setBackgroundImage(UIImage(named: "buttonCreen"), for: .normal)
            tap.isEnabled = false

        }
        
        print("Atap.isEnabled = false")
        tap.isEnabled = false
    }

    @objc func finishAnime() {
        if numberOfAnime < 15 { //original 14
            toNextScreen()
        }
    }
    
    @objc func pauseAnim() {
        globalFunc.pauseAnim()
        print("pauseAnim", numberOfAnime)
        
        if numberOfAnime > 15 && numberOfAnime < 18 { // original 14
            addTap()
        }
    }
//    15 не включилась
    @objc func cancelAnime() {
        print("tap.isEnabled = true")
//        tap.isEnabled = true
    }
    
    func updateTextDescription(numberText: Int) {
        desctiprtionLabel.text = descriptionText[numberText]
    }
    
    func toNextScreen() {
        print(numberOfAnime, "status +++++++++++")
//        if numberOfAnime > 1 && numberOfAnime <= 14 { // original
        if numberOfAnime > 1 && numberOfAnime <= 15 {

            globalFunc.stopAnim(removeAmine: true)
        }
        
        if animationText[numberOfAnime].contains("loop") {
//            print("loop with numver", numberOfAnime)
            globalFunc.addAnim(view: animView, name: animationText[numberOfAnime], isRepeat: true)
            nextBtn.isEnabled = true
            if numberOfAnime == 22 {
                updateTextDescription(numberText: numberOfTextDescription)
            }

        } else {
            nextBtn.isEnabled = false
            globalFunc.addAnim(view: animView, name: animationText[numberOfAnime], isRepeat: false)
//            print("NO loop", numberOfAnime)
//            print("тут обновляется текст ", numberOfTextDescription)
            
            updateTextDescription(numberText: numberOfTextDescription)
            if numberOfAnime <= 14 { // original 13
                numberOfTextDescription += 1
            } else if numberOfAnime >= 20 {
                numberOfTextDescription += 1
//                print("тут должен плюсовать номер текста", numberOfTextDescription)
                updateTextDescription(numberText: numberOfTextDescription)
            }
        }
        
        if numberOfAnime <= 15 { // original 14
            numberOfAnime += 1
        }

        if numberOfAnime == 20 {
            globalFunc.addAnim(view: animView, name: animationText[numberOfAnime], isRepeat: false)
//            numberOfAnime += 1
        }
        
        if numberOfAnime == 21 {
            globalFunc.addAnim(view: animView, name: animationText[numberOfAnime], isRepeat: false)
//            numberOfAnime += 1
        }
    }
    

    
    @objc func delayNextNumber() {
        nextBtn.isEnabled = true
        numberOfAnime += 1
    }
    
    
    @IBAction func actionBtn(_ sender: Any) {
        print("actionBtn", numberOfAnime)
        
        switch numberOfAnime {
        case 8:
            setupProgress()
        case 12:
            setupProgress()
        case 22:
            setupProgress()
        case 24:
            setupProgress()
        case 27:
            setupProgress()
        default:
            break
        }
        
        if numberOfAnime < 18 {
            toNextScreen()
        }
        
//        if numberOfAnime == 15 {
//
//        }
        
                if numberOfAnime == 16 {
//        if numberOfAnime == 15 { // original
//            addTap()
            nextBtn.isEnabled = false
            nextBtn.setBackgroundImage(UIImage(named: "buttonCrey"), for: .normal)
            _ = Timer.scheduledTimer(timeInterval: 3.25, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
        }
        
        if numberOfAnime == 18 {
            addImportant()
        }
        
        if numberOfAnime == 19 {
            nextTwoText()
        }
        
        
        if numberOfAnime == 20 {
            globalFunc.onlyRemoveAnim()
            toNextScreen()
            topText.isHidden = true
            bigText.isHidden = true
            animView.isHidden = false
            desctiprtionLabel.isHidden = false

            _ = Timer.scheduledTimer(timeInterval: 2.70, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            _ = Timer.scheduledTimer(timeInterval: 2.70, target: self, selector: #selector(delayNextNumber), userInfo: nil, repeats: false)
            
            
//            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2000)) { [self] in
//                nextBtn.isEnabled = true
//                numberOfAnime += 1
//            }
            
        }
        
        if numberOfAnime == 21 {
            globalFunc.onlyRemoveAnim()
            toNextScreen()
            
            _ = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            _ = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(delayNextNumber), userInfo: nil, repeats: false)
        }
        
        if numberOfAnime == 22 {
            
            globalFunc.onlyRemoveAnim()
            numberOfTextDescription += 1
            toNextScreen()
            crasBtn.isHidden = false
            crasBtn.setTitle("  Сколько раз вы пытались бросить курить?", for: .normal)
            
            settingButton()
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(delayNextNumber), userInfo: nil, repeats: false)
        }
        
        if numberOfAnime == 23 {
            plusBtn.isHidden = true
            minusBtn.isHidden = true
            countSiggaretLabel.isHidden = true
            crasBtn.isHidden = true
            
            globalFunc.onlyRemoveAnim()
            toNextScreen()
            let useDef = UserDefManager.shared
            let name = useDef.getName()
            desctiprtionLabel.text = name + ", Вы пытались бросить уже \(coutnOfSigars) раз и мы уверены, что вы просто выбирали неверный метод, но теперь все изменится."
            
            _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(delayNextNumber), userInfo: nil, repeats: false)

            numberOfTextDescription -= 1

            nextBtn.isEnabled = true
//            numberOfAnime += 1
        }
        
        if numberOfAnime == 24 {
            globalFunc.onlyRemoveAnim()
            toNextScreen()
            
            _ = Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(delayNextNumber), userInfo: nil, repeats: false)
        }
        
        if numberOfAnime == 25 {

            numberOfTextDescription += 1
            
            updateTextDescription(numberText: numberOfTextDescription)

            crasBtn.isHidden = false
            crasBtn.setTitle("  Отпустите нажатием эти гири!", for: .normal)
            addBobButton()
            
            nextBtn.isEnabled = false
            nextBtn.setBackgroundImage(UIImage(named: "buttonCrey"), for: .normal)
            
        }
        
        if numberOfAnime == 26 {
            numberOfTextDescription += 1
            updateTextDescription(numberText: numberOfTextDescription)
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(delayNextNumber), userInfo: nil, repeats: false)
        }
        
        if numberOfAnime == 27 {
            crasBtn.isHidden = false
            crasBtn.setTitle("  Расположите слова правильно.", for: .normal)
            numberOfTextDescription += 1
            updateTextDescription(numberText: numberOfTextDescription)
            sceneView.isHidden = false
            nextBtn.isEnabled = false
            nextBtn.setBackgroundImage(UIImage(named: "buttonCrey"), for: .normal)

        }
        
        if numberOfAnime == 30 {
            numberOfTextDescription += 1
            updateTextDescription(numberText: numberOfTextDescription)
            animView.isHidden = true
            lastImage.isHidden = false
            _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(delayNextNumber), userInfo: nil, repeats: false)
        }
        
        if numberOfAnime == 31 {
            toRemember()
        }
        
    }
    
    func toRemember() {
        guard let rememberVC = storyboard?.instantiateViewController(identifier: "rememberVC") as? RememberCollectionVC else { return }
        rememberVC.lesson = lesson
        
        navigationController?.pushViewController(rememberVC, animated: true)
    }
    
    func addBobButton() {
//        desctiprtionLabel.isHidden = true
//        nextBtn.isEnabled = false
//        nextBtn.setBackgroundImage(UIImage(named: "buttonCrey"), for: .normal)
        
        let xr = animView.frame.width / 1.7
        let yr = animView.frame.height / 1.8
        rbtn = UIButton(frame: CGRect(x: xr, y: yr, width: 88, height: 88))
//        rbtn.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 0.5729616683)
        rbtn.addTarget(self, action: #selector(rtap), for: .touchUpInside)
        animView.addSubview(rbtn)
        
        let xl = animView.frame.width / 3.8
        let yl = animView.frame.height / 1.8
        lbtn = UIButton(frame: CGRect(x: xl, y: yl, width: 88, height: 88))
//        lbtn.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 0.5729616683)
        lbtn.addTarget(self, action: #selector(ltap), for: .touchUpInside)
        animView.addSubview(lbtn)
    }
    
    @objc func rtap() {

        if countTapBob == 0 {
            globalFunc.onlyRemoveAnim()
            globalFunc.addAnim(view: animView, name: "lesson 1_25_1R", isRepeat: false)
            _ = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            desctiprtionLabel.isHidden = false
            numberOfTextDescription += 1
            updateTextDescription(numberText: numberOfTextDescription)
        } else if countTapBob == 1 {
            globalFunc.onlyRemoveAnim()
            globalFunc.addAnim(view: animView, name: "lesson 1_25_2L", isRepeat: false)
            _ = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            finishBob()
        }
        countTapBob += 1
        addBobButton()

    }
    
    @objc func ltap() {

        if countTapBob == 0 {
            globalFunc.onlyRemoveAnim()
            globalFunc.addAnim(view: animView, name: "lesson 1_25_1L", isRepeat: false)
            _ = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            desctiprtionLabel.isHidden = false
            numberOfTextDescription += 1
            updateTextDescription(numberText: numberOfTextDescription)
        } else if countTapBob == 1 {
            globalFunc.onlyRemoveAnim()
            globalFunc.addAnim(view: animView, name: "lesson 1_25_2R", isRepeat: false)
            _ = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(pauseAnim), userInfo: nil, repeats: false)
            
            finishBob()
        }
        countTapBob += 1
        addBobButton()
    }
    
    func finishBob() {
        numberOfTextDescription += 1
        numberOfAnime += 1
        updateTextDescription(numberText: numberOfTextDescription)
        lbtn.removeFromSuperview()
        rbtn.removeFromSuperview()
        nextBtn.isEnabled = true
        nextBtn.setBackgroundImage(UIImage(named: "buttonCreen"), for: .normal)
        crasBtn.isHidden = true
    }
    
    @IBAction func actionBtnPlus(_ sender: Any) {
        
        coutnOfSigars += 1
        
        countSiggaretLabel.text = "\(coutnOfSigars)"
        if coutnOfSigars > 0 {
            minusBtn.layer.borderColor = #colorLiteral(red: 0.9968038201, green: 0.6798890233, blue: 0.421605289, alpha: 1)
            minusBtn.setTitleColor(#colorLiteral(red: 0.9968038201, green: 0.6798890233, blue: 0.421605289, alpha: 1), for: .normal)
            countSiggaretLabel.layer.borderColor = #colorLiteral(red: 0.9968038201, green: 0.6798890233, blue: 0.421605289, alpha: 1)
            countSiggaretLabel.textColor = #colorLiteral(red: 0.9968038201, green: 0.6798890233, blue: 0.421605289, alpha: 1)
        }
    }
    
    @IBAction func actionBtnMinus(_ sender: Any) {
        
        if coutnOfSigars > 0 {
            coutnOfSigars -= 1
            countSiggaretLabel.text = "\(coutnOfSigars)"
        }
        
        if coutnOfSigars == 0 {
            minusBtn.layer.borderColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
            countSiggaretLabel.layer.borderColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
            countSiggaretLabel.layer.borderColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
            countSiggaretLabel.textColor = #colorLiteral(red: 0.8548267484, green: 0.8549502492, blue: 0.8547996879, alpha: 1)
        }
        countSiggaretLabel.text = "\(coutnOfSigars)"
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}


extension Lesson_1_day { // important View
    
    /*
    func addImportantOld() {
        
        importantView.frame = CGRect(x: 0,
                                     y: self.view.frame.height,
                                     width: self.view.frame.width,
                                     height: self.view.frame.height)
        let whiteView = UIView(frame: CGRect(
                                x: 0,
                                y: importantView.frame.height / 2,
                                width: self.view.frame.width,
                                height: self.view.frame.height / 2))
        whiteView.backgroundColor = .white
        whiteView.layer.cornerRadius = 18
        
        let imageView = UIImageView(frame: CGRect(x: self.view.frame.width * 0.168,
                                                  y: (importantView.frame.height / 2) - (self.view.frame.width * 0.3),
                                                  width: self.view.frame.width * 0.68,
                                                  height: self.view.frame.width * 0.6))
        imageView.image = UIImage(named: "important")
        
        let nameLabel = UILabel(frame: CGRect(x: 0,
                                              y: imageView.frame.maxY + 18,
                                              width: view.frame.width, height: 38))
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont(name: "Avenir Next Bold", size: 24)
        nameLabel.text = "Важно!"
        
        let descriptionLabel = UILabel(frame: CGRect(x: 44,
                                              y: nameLabel.frame.maxY,
                                              width: view.frame.width - 88,
                                              height: 108))
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont(name: "Avenir Next", size: 15)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = "Отнеситесь к выполнению заданий ответственно и серьезно, так как в ежедневной практике заключается движение к свободе от сигарет."
        
        let button = UIButton(frame: CGRect(x: 32, y: descriptionLabel.frame.maxY + 28, width: self.view.frame.width - 64, height: 52))
        button.setBackgroundImage(UIImage(named: "buttonCreen"), for: .normal)
        button.setTitle("ХОРОШО", for: .normal)
        button.titleLabel?.font = UIFont(name: "Avenir Next Bold", size: 18)
        button.titleLabel?.textColor = .white
        button.addTarget(self, action: #selector(closeImportant), for: .touchUpInside)
        
        
        
        let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: importantView.frame.width + 40, height: importantView.frame.height)
        importantView.addSubview(blurEffectView)
        
        importantView.addSubview(whiteView)
        importantView.addSubview(imageView)
        importantView.addSubview(nameLabel)
        importantView.addSubview(descriptionLabel)
        importantView.addSubview(button)

        view.addSubview(importantView)
        
        UIView.animate(withDuration: 1) { [self] in
            importantView.transform = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
        }
    }
    */
    
    func addImportant() {
        
        importantView.frame = CGRect(x: 0,
                                     y: 0,
                                     width: self.view.frame.width,
                                     height: self.view.frame.height)
        
        let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0
        blurEffectView.frame = CGRect(x: 0, y: 0, width: importantView.frame.width + 40, height: importantView.frame.height)
        importantView.addSubview(blurEffectView)
        
        
        let whiteView = UIView(frame: CGRect(
                                x: 0,
//                                y: importantView.frame.height / 2,
                                y: importantView.frame.height,

                                width: self.view.frame.width,
                                height: self.view.frame.height / 1.6))
        whiteView.backgroundColor = .white
        whiteView.layer.cornerRadius = 18
        
        let imageView = UIImageView(frame: CGRect(x: self.view.frame.width * 0.168,
                                                  y: -self.view.frame.width * 0.26,
                                                  width: self.view.frame.width * 0.68,
                                                  height: self.view.frame.width * 0.6))
        imageView.image = UIImage(named: "important")
        
        let nameLabel = UILabel(frame: CGRect(x: 0,
                                              y: imageView.frame.maxY + 18,
                                              width: view.frame.width, height: 38))
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont(name: "Avenir Next Demi Bold", size: 24)
        nameLabel.text = "Важно!"
        
        let descriptionLabel = UILabel(frame: CGRect(x: 44,
                                              y: nameLabel.frame.maxY,
                                              width: view.frame.width - 88,
                                              height: 108))
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont(name: "Avenir Next Regular", size: 15)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = lesson.important
        
        let button = UIButton(frame: CGRect(
                                x: 32,
//                                y: descriptionLabel.frame.maxY + 48,
                                y: self.view.frame.height / 2.2,
                                width: self.view.frame.width - 64,
                                height: 58))
        
        button.backgroundColor = #colorLiteral(red: 0.3221721649, green: 0.8322338462, blue: 0.7341098189, alpha: 1)
        button.layer.cornerRadius = 28
        button.setTitle("ХОРОШО", for: .normal)
        button.titleLabel?.font = UIFont(name: "Avenir Next Bold", size: 18)
        button.titleLabel?.textColor = .white
        button.addTarget(self, action: #selector(closeImportant), for: .touchUpInside)
        
        
        

        
        whiteView.addSubview(imageView)
        whiteView.addSubview(nameLabel)
        whiteView.addSubview(descriptionLabel)
        whiteView.addSubview(button)
        importantView.addSubview(whiteView)

        view.addSubview(importantView)
        
        UIView.animate(withDuration: 1) { [self] in
            whiteView.transform = CGAffineTransform(translationX: 0, y: -importantView.frame.height / 1.7)
            blurEffectView.alpha = 0.98
        }
    }
    
    @objc func closeImportant() {
        
        crasBtn.isHidden = true
        crasBtn.isHidden = true
        
        UIView.animate(withDuration: 1) { [self] in
            importantView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
            
        } completion: { [self] end in
            importantView.removeFromSuperview()
            nextBtn.isEnabled = true
            nextBtn.setBackgroundImage(UIImage(named: "buttonCreen"), for: .normal)
            numberOfAnime += 1
            //            toNextScreen()
            openTwoText()
            importantView.removeFromSuperview()
            setupProgress()
        }
    }
}


extension Lesson_1_day { // two text view
    
    func openTwoText() {
        animView.isHidden = true
        desctiprtionLabel.isHidden = true
        
        topText.isHidden = false
        dottedLineImage.isHidden = false
        
        bigText.isHidden = false
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 0.78
        
        
        
        bigText.attributedText = NSMutableAttributedString(string: "Отказ от предмета желания без отказа от самого желаниябесплоден, чего бы он ни стоил.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        bigText.textAlignment = .center
        
        bigText.text = """
        Единственная причина, почему люди курят — они не видят реальность
        такой, какая она есть на самом деле. Сигаретный дым окутывает их,
        очаровывает, затуманивает мысли, мешая смотреть на вещи трезво.

        Люди не видят сигареты такими, какие они есть на самом деле. Они не
        видят свои ощущения такими, какие они есть. Курящие люди пребывают в
        иллюзии.
        """
        if self.view.frame.height > 680 {
//            toTopFromBig.constant = 48
            dottedUp.constant = 28
            dottedDown.constant = 28
        }
//        else if self.view.frame.height < 680 {
//            toTopFromBig.constant = 8
//        }
        
    }
    
    func nextTwoText() {

        topText.isHidden = true
        dottedLineImage.isHidden = true
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 0.78

        bigText.attributedText = NSMutableAttributedString(string: "Отказ от предмета желания без отказа от самого желаниябесплоден, чего бы он ни стоил.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        bigText.textAlignment = .center

        bigText.text = """
        При малейшей тяге они выбирают сигареты как способ
        спрятаться от дискомфорта и погрузиться в другую реальность.

        Есть только один способ избавиться от зависимости. Нужно набраться
        мужества и увидеть реальную картину, понять, что на самом деле
        происходит с вами, когда вы курите.
        Вы не сможете продолжать курить после того, как увидите реальную
        картину.

        Если вы думаете, что видите реальную картину, но продолжаете
        курить — вы ещё не видите её.
        За 28 дней вы тщательно изучите себя и свой опыт курения. В результате
        вы просто не сможете НЕ увидеть реальность такой, какая она есть.
        """
        
        /*
        10 - 812 +
         8+ - 736 +
         8   - 667 --
        */
        if self.view.frame.height > 680 {
            toTopFromBig.constant = 48
        } else if self.view.frame.height < 680 {
            toTopFromBig.constant = 8
        }
        
        numberOfTextDescription = 11
        
        _ = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(delayNextNumber), userInfo: nil, repeats: false)
        

    }
    
}

