//
//  SceneTriger.swift
//  SmokingOff
//
//  Created by DfStudio on 23.06.2021.
//



import UIKit
import SpriteKit
import GameplayKit

class L1Scene: SKScene {
    
    var finishScene: (() -> ())?

    var sourceBtnL: ButtonSprite!
    var sourceBtnR: ButtonSprite!
    
    var targetBtnL: ButtonSprite!
    var targetBtnR: ButtonSprite!


//    var triger2: TrigerSprite2!

//    var line: LineSprite!
//    var line2: LineSprite!
//    var line3: LineSprite!
//    var line4: LineSprite!
//    var line5: LineSprite!
    
    var angular: CGFloat = 0
    var positionMen: CGPoint = CGPoint(x: 0, y: 50)
    var animationWithTextures: SKAction!
    
    
    var centerY: CGFloat!
    var bottom: CGFloat!
    var top: CGFloat!
    var left: CGFloat!
    var right: CGFloat!
    var i = 0
    var timer = Timer()
    var startPosition: CGPoint!
    var positionBtnL: CGPoint!
    var positionBtnR: CGPoint!
    var moveL = false
    var moveR = false
    
    var countOfCorrectAnswers = 0

    var namesButton = ["prohibition", "refusal", "prohibitionOk", "refusalOk", "doNot", "iWant"]
    
    //#########################################################
    
    
    override func didMove(to view: SKView) {
        physicsWorld.gravity = .init(dx: 0, dy: 0)
       
        loadContent()
    }
    
    func loadContent() {
        setupScreenLayout()
        setupNodes()
    }
    
    
    
    func setupScreenLayout() {
        left = size.width / 2
        right = left - size.width
        centerY = size.height / 2
        bottom = centerY - size.height / 2
        top = bottom + size.height
    }
    
    @objc func createSourceBtn() {
        
        sourceBtnL = ButtonSprite.createButton(name: namesButton[0])
        sourceBtnL.position = CGPoint(
            x: -self.frame.width / 4.8,
            y: self.frame.height / 3.8)
        sourceBtnL.name = "btnL"
        addChild(sourceBtnL)
        sourceBtnL.zPosition = 6
        
        sourceBtnR = ButtonSprite.createButton(name: namesButton[1])
        sourceBtnR.position = CGPoint(x: self.frame.width / 4.8, y: self.frame.height / 3.8)
        sourceBtnR.name = "btnR"
        addChild(sourceBtnR)
        sourceBtnR.zPosition = 6

        
        targetBtnL = ButtonSprite.createButton(name: namesButton[5])
        targetBtnL.position = CGPoint(x: -self.frame.width / 4.8, y: -self.frame.height / 3.8)
        targetBtnL.name = "targetBtnL"
        addChild(targetBtnL)
        targetBtnL.zPosition = 0
        
        targetBtnR = ButtonSprite.createButton(name: namesButton[4])
        targetBtnR.position = CGPoint(x: self.frame.width / 4.8, y: -self.frame.height / 3.8)
        targetBtnR.name = "targetBtnR"
        addChild(targetBtnR)
        targetBtnR.zPosition = 0

    }
    
    func setupNodes() {
        createSourceBtn()

        anchorPoint = CGPoint(x: 0.5, y: 0.5)

    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = touches.first!.location(in: self)
        let node = self.atPoint(location)
        startPosition = node.position

        if node.name == "btnL" {
//            node.removeFromParent()
//            tapOnTrigerInScene?()
            node.position = location
            positionBtnL = location
            moveL = true
            moveR = false
        }
        if node.name == "btnR" {
//            node.removeFromParent()
//            tapOnTrigerInScene?()
            node.position = location
            positionBtnR = location
            moveL = false
            moveR = true
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if moveL {
            positionBtnL = (touches.first?.location(in: self))
            sourceBtnL.position = positionBtnL
        }
        if moveR {
            positionBtnR = (touches.first?.location(in: self))
            sourceBtnR.position = positionBtnR
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if moveL {
            if !sourceBtnL.intersects(targetBtnR){
                let move = SKAction.move(to: startPosition, duration: 0.48)
                sourceBtnL.run(move)
            } else {
                let move = SKAction.move(to: targetBtnR.position, duration: 0.18)
                sourceBtnL.run(move)
                _ = Timer.scheduledTimer(withTimeInterval: 0.58, repeats: false, block: { [self] finish in
                    let newBtn = ButtonSprite.createButton(name: namesButton[2])
                    newBtn.position = sourceBtnL.position
                    sourceBtnL.removeFromParent()
                    targetBtnR.removeFromParent()
                    addChild(newBtn)
                    countOfCorrectAnswers += 1
                    if countOfCorrectAnswers == 2 {
                        finishScene?()
                    }
                })
            }
        }
        
        if moveR {
            if !sourceBtnR.intersects(targetBtnL){
                let move = SKAction.move(to: startPosition, duration: 0.48)
                sourceBtnR.run(move)
            } else {
                let move = SKAction.move(to: targetBtnL.position, duration: 0.18)
                sourceBtnR.run(move)
                _ = Timer.scheduledTimer(withTimeInterval: 0.58, repeats: false, block: { [self] finish in
                    let newBtn = ButtonSprite.createButton(name: namesButton[3])
                    newBtn.position = sourceBtnR.position
                    sourceBtnR.removeFromParent()
                    targetBtnL.removeFromParent()
                    addChild(newBtn)
                    countOfCorrectAnswers += 1
                    if countOfCorrectAnswers == 2 {
                        finishScene?()
                    }
                })
            }
        }
    }
}


//extension GameScene: SKPhysicsContactDelegate {
//    func didBegin(_ contact: SKPhysicsContact) {
//        let contactCategory: BitMaskCategory = [contact.bodyA.category, contact.bodyB.category]
//
//    }
//
//
//    func didEnd(_ contact: SKPhysicsContact) {
//
//    }
//}
