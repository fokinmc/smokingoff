//
//  CounterTableViewCell.swift
//  SmokingOff
//
//  Created by DfStudio on 10.08.2021.
//

import UIKit

class TableCounterViewCell: UITableViewCell {

    @IBOutlet weak var openImage: UIImageView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var cellLine: UIView!
    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var descriptionCell: UILabel!

    
    //for open
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!

    @IBOutlet weak var label11: UILabel!
    @IBOutlet weak var label22: UILabel!
    @IBOutlet weak var label33: UILabel!
    @IBOutlet weak var label44: UILabel!
    
    @IBOutlet weak var toBottom: NSLayoutConstraint!
    @IBOutlet weak var secondToBottom: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        close()
        GlobalFunc.shadow(view: backView)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func open() {
        label1.isHidden = false
        label2.isHidden = false
        label3.isHidden = false
        label4.isHidden = false

        label1.isHidden = false
        label2.isHidden = false
        label3.isHidden = false
        label4.isHidden = false
        
        openImage.image = UIImage(named: "toClose")

        cellLine.isHidden = false
        toBottom.priority = UILayoutPriority.defaultLow
        secondToBottom.priority = UILayoutPriority.defaultHigh
    }
    
    func close() {
        label1.isHidden = true
        label2.isHidden = true
        label3.isHidden = true
        label4.isHidden = true

        label1.isHidden = true
        label2.isHidden = true
        label3.isHidden = true
        label4.isHidden = true
        
        cellLine.isHidden = true
        openImage.image = UIImage(named: "toOpen")
        
        toBottom.priority = UILayoutPriority.defaultHigh
        secondToBottom.priority = UILayoutPriority.defaultLow
    }
    
}
