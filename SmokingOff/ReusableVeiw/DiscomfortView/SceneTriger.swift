//
//  SceneTriger.swift
//  SmokingOff
//
//  Created by DfStudio on 23.06.2021.
//



import UIKit
import SpriteKit
import GameplayKit
let perTime: TimeInterval = 0.05

class GameScene: SKScene {
    
    var tapOnTrigerInScene: (() -> ())?

    var triger: TrigerSprite1!
//    var triger2: TrigerSprite2!

    var line: LineSprite!
    var line2: LineSprite!
    var line3: LineSprite!
    var line4: LineSprite!
    var line5: LineSprite!
    
    var angular: CGFloat = 0
    var positionMen: CGPoint = CGPoint(x: 0, y: 50)
    var animationWithTextures: SKAction!
    
    
    var centerY: CGFloat!
    var bottom: CGFloat!
    var top: CGFloat!
    var left: CGFloat!
    var right: CGFloat!
    var i = 0
    var timer = Timer()

    var namesButton = ["RoundButton0", "RoundButton1", "RoundButton2", "RoundButton3", "RoundButton4", "RoundButton5", "RoundButton6", "RoundButton7", "RoundButton8", "RoundButton9", "RoundButton12", "RoundButton10", "RoundButton11",  "RoundButton12"]
    
    //#########################################################
    
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = .init(dx: 0, dy: -0.2)
       
        loadContent()
    }
    
    func loadContent() {
        setupScreenLayout()
        setupNodes()
    }
    
    
    
    func setupScreenLayout() {
        left = size.width / 2
        right = left - size.width
        centerY = size.height / 2
        bottom = centerY - size.height / 2
        top = bottom + size.height
    }
    
    @objc func createTriger() {
        triger = TrigerSprite1.createTriger(name: namesButton[i])
        triger.position = CGPoint(x: 20, y: 150)
        triger.name = "triger"
        addChild(triger)
        i += 1
        if i == 8 {
            timer.invalidate()
        }
    }
    
    func setupNodes() {
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(createTriger), userInfo: nil, repeats: true)
        
//        triger2 = TrigerSprite2.createTriger(name: namesButton[i+1])
//        triger2.position = CGPoint(x: 20, y: -50)
//        addChild(triger2)
        
        line = LineSprite.createLine(name: "line")
        line.position = CGPoint(x: 20, y: -(self.view?.frame.height)! / 3 - 18)
//        line.zRotation = 5 * (.pi / 180)

        line4 = LineSprite.createLine(name: "info")
        
        line4.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 40, height: 40))
        line4.position = CGPoint(x: 0, y: -(self.view?.frame.height)! / 3 - 18)
        line4.zRotation = -5 * (.pi / 180)
        line4.alpha = 0
        
        line5 = LineSprite.createLine(name: "info")
        line5.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 40, height: 40))
        line5.position = CGPoint(x: 0.5, y: -(self.view?.frame.height)! / 3 - 18)
        line5.zRotation = -5 * (.pi / 180)
        line5.alpha = 0
        
        line2 = LineSprite.createLine(name: "line")
        line2.alpha = 0
        line2.position = CGPoint(x: -((self.view?.frame.width)! / 2), y: -(self.view?.frame.height)! / 3 - 18)
        line2.zRotation = 90 * (.pi / 180)
        
        line3 = LineSprite.createLine(name: "line")
        line3.alpha = 0
        line3.zRotation = 90 * (.pi / 180)
        line3.position = CGPoint(x: ((self.view?.frame.width)! / 2), y: -(self.view?.frame.height)! / 3 - 18)

        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        addChild(line)
        addChild(line2)
        addChild(line3)
        addChild(line4)
        addChild(line5)

    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = touches.first!.location(in: self)
        let node = self.atPoint(location)
        if node.name == "triger" {
            print("tap in triger")
            node.removeFromParent()
            tapOnTrigerInScene?()
        }
    }
    
}


extension GameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        let contactCategory: BitMaskCategory = [contact.bodyA.category, contact.bodyB.category]

    }
    
    
    func didEnd(_ contact: SKPhysicsContact) {

    }
}
