//
//  LineSprite.swift
//  SmokingOff
//
//  Created by DfStudio on 24.06.2021.
//

import SpriteKit
import GameplayKit

class LineSprite: SKSpriteNode {
    
    static var position: CGPoint!
//    static let imageName1 = ["1map1", "1map2", "1map3", "1map4"]
//    static let imageName2 = ["2map1", "2map2", "2map3", "2map4"]
//    static let imageName3 = ["3map1", "3map2", "3map3", "3map4"]
//    static let imageName4 = ["4map1", "4map2", "4map3", "4map4"]


//    static let imageNames = [imageName3]
    
//    static func createWall(at point: CGPoint) -> Wall {
    static func createLine(name: String) -> LineSprite {

        let texture = SKTexture(imageNamed: name)
        let line = LineSprite(imageNamed: name)
        line.zPosition = 1
        
        
        line.physicsBody = SKPhysicsBody(texture: texture, alphaThreshold: 0.9, size: line.size)
 
        line.physicsBody?.isDynamic = false
        line.physicsBody?.categoryBitMask = BitMaskCategory.line.rawValue
        
        line.physicsBody?.collisionBitMask = BitMaskCategory.triger1.rawValue
        line.physicsBody?.contactTestBitMask = BitMaskCategory.triger1.rawValue

        line.physicsBody?.collisionBitMask = BitMaskCategory.triger2.rawValue
        line.physicsBody?.contactTestBitMask = BitMaskCategory.triger2.rawValue

        return line
    }
    
    static func setupWallPosition(wall: TrigerSprite1) -> CGPoint {
        position = CGPoint(x: 0.5, y: wall.frame.height + 100)
        return position
    }
    
    /*
    static func configureWallName() -> String {
        let distribution = GKRandomDistribution(lowestValue: 1, highestValue: 2)
        let randomNumber = distribution.nextInt()
        let imageName = "is1" //+ "\(randomNumber)"
        
        return imageName
    }
    */
    
    static var randomScaleFactor: CGFloat {
        let distribution = GKRandomDistribution(lowestValue: 1, highestValue: 10)
        let randomNumber = CGFloat(distribution.nextInt()) / 10
        
        return randomNumber
    }
    
    static func roteteForRandomAngle() -> SKAction {
        let distribution = GKRandomDistribution(lowestValue: 1, highestValue: 360)
        let randomNumber = CGFloat(distribution.nextInt())
        
        
        return SKAction.rotate(toAngle: randomNumber * CGFloat(Double.pi / 180), duration: 0)
    }
}
