//
//  InfoSmokingViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 21.04.2021.
//

import UIKit

class InfoSmokingViewController: UIViewController {

    @IBOutlet weak var numberInPack: UITextField!
    @IBOutlet weak var pricePack: UITextField!
    @IBOutlet weak var countCigarettsOfDay: UITextField!

    let useDef = UserDefManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupInterface()
        getSmokingInfo()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if let numberPack = Int(numberInPack.text ?? "0") {
            useDef.saveNumberInPack(number: numberPack)
            print("1")
        }
        
        if let pricePack = Int(pricePack.text ?? "0") {
            useDef.savePricePack(number: pricePack)
            print("2")
        }
        
        if let countOfDay = Int(countCigarettsOfDay.text ?? "0") {
            useDef.saveCountOfDay(number: countOfDay)
            print("3")
        }
        
    }
    
    func getSmokingInfo() {
        
        if useDef.getNumberInPack() != 0 {
            numberInPack.text = "\(useDef.getNumberInPack())"
        }
        if useDef.getPricePack() != 0 {
            pricePack.text = "\(useDef.getPricePack())"
        }
        if useDef.getCountOfDay() != 0 {
            countCigarettsOfDay.text = "\(useDef.getCountOfDay())"
        }
        
    }
    
    func setupInterface() {
        title = "Smoking data"
        if useDef.getNumberInPack() == 0 {
            numberInPack.font = UIFont(name: "Avenir Next Bold", size: 19)
        } else {
            numberInPack.font = UIFont(name: "Avenir Next Bold", size: 19)
        }
        tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func saveData(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
