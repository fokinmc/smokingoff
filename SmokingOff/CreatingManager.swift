//
//  TestCreatingManager.swift
//  SmokingOff
//
//  Created by DfStudio on 29.04.2021.
//

import Foundation

struct CreatingManager {
    
    private let jsonManager = JSONParserManager()
    
    func getTest(file: String, completion: @escaping ([Test]) -> ()) {
        var testArray = [Test]()

        jsonManager.fetchDataJsonTest(file: file) { (tests) in

            testArray = tests.tests
            DispatchQueue.main.async {
                completion(testArray)
            }
        }
    }
        
    func getWeeks(file: String, completion: @escaping ([Week]) -> ()) {
        var weekArray = [Week]()

        jsonManager.fetchDataJsonWeek(file: file) { (result) in

            weekArray = result.weeks
            DispatchQueue.main.async {
                completion(weekArray)
            }
        }
    }
}
