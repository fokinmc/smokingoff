//
//  TrigerSprite.swift
//  SmokingOff
//
//  Created by DfStudio on 23.06.2021.
//

import SpriteKit
import GameplayKit

class TrigerSprite1: SKSpriteNode {
    
    static var position: CGPoint!
    static func createTriger(name: String) -> TrigerSprite1 {
        print("createTriger old")

        let texture = SKTexture(imageNamed: name)
        let triger = TrigerSprite1(imageNamed: name)
        triger.zPosition = 1
        triger.setScale(0.4)
        triger.physicsBody = SKPhysicsBody(texture: texture, alphaThreshold: 0.9, size: triger.size)
        
        triger.physicsBody?.isDynamic = true
        triger.physicsBody?.categoryBitMask = BitMaskCategory.line.rawValue
        
        triger.physicsBody?.collisionBitMask = BitMaskCategory.triger2.rawValue
        triger.physicsBody?.contactTestBitMask = BitMaskCategory.triger2.rawValue

        
        triger.physicsBody?.collisionBitMask = BitMaskCategory.line.rawValue
        triger.physicsBody?.contactTestBitMask = BitMaskCategory.line.rawValue
        triger.run(roteteForRandomAngle())
        return triger
    }
    
    static func setupWallPosition(wall: TrigerSprite1) -> CGPoint {
        position = CGPoint(x: 0.5, y: wall.frame.height + 100)
        return position
    }
    
    static func roteteForRandomAngle() -> SKAction {
        let distribution = GKRandomDistribution(lowestValue: 1, highestValue: 360)
        let randomNumber = CGFloat(distribution.nextInt())
        return SKAction.rotate(toAngle: randomNumber * CGFloat(Double.pi / 180), duration: 0)
    }
}

class TrigerSprite2: SKSpriteNode {
    
    static var position: CGPoint!
    static func createTriger(name: String) -> TrigerSprite2 {
        let texture = SKTexture(imageNamed: name)
        let triger = TrigerSprite2(imageNamed: name)
        triger.zPosition = 1
        triger.setScale(0.4)
        triger.physicsBody = SKPhysicsBody(texture: texture, alphaThreshold: 0.9, size: triger.size)
        
        triger.physicsBody?.isDynamic = true
        triger.physicsBody?.categoryBitMask = BitMaskCategory.line.rawValue
        
        triger.physicsBody?.collisionBitMask = BitMaskCategory.triger1.rawValue
        triger.physicsBody?.contactTestBitMask = BitMaskCategory.triger1.rawValue

        
        triger.physicsBody?.collisionBitMask = BitMaskCategory.line.rawValue
        triger.physicsBody?.contactTestBitMask = BitMaskCategory.line.rawValue
        triger.run(roteteForRandomAngle())
        return triger
    }
    
    static func setupWallPosition(wall: TrigerSprite2) -> CGPoint {
        position = CGPoint(x: 0.5, y: wall.frame.height + 100)
        return position
    }
    
    static func roteteForRandomAngle() -> SKAction {
        let distribution = GKRandomDistribution(lowestValue: 1, highestValue: 360)
        let randomNumber = CGFloat(distribution.nextInt())
        return SKAction.rotate(toAngle: randomNumber * CGFloat(Double.pi / 180), duration: 0)
    }
}
