//
//  SourseBotMessages.swift
//  SmokingOff
//
//  Created by DfStudio on 05.07.2021.
//

import Foundation

struct NewBotMessage {
    let useDef = UserDefManager.shared
    
    static let startOfChoose = [
        BotMessage(
            optionBtn: .threeBtn,
            text: "Что будем делать?",
            topBtnName: "ДОБАВИТЬ СИГАРЕТУ",
            middleBtnName: "СТАТИСТИКА",
            buttomBtnName: "ЗАКРЫТЬ ДЕНЬ",
            nextSection: 0,
            topIsSend: true,
            middleIsSend: false,
            buttomIsSend: false,
            endOfSection: false
            //            currentAction: .next
        ),
    ]
    

    static let addSigarette = [
        BotMessage( //0
            optionBtn: .twoBtn,
//            text: "За сегодня записей нет. Хочешь добавить сигарету?",
            text: "Принято! Напоминаю, что надо курить не торопясь, размышляя о том, что ты чувствуешь в этот момент.",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "НАЗАД",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage(//1
            optionBtn: .twoBtn,
//            text: "Сегодня было выкурено сигарет: 1. Хочешь добавить ещё?",
            text: "2____________",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "НАЗАД",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //2 //второй экран цикла 1день
            optionBtn: .twoBtn,
            text: "Принято! Напоминаю, что надо курить не торопясь, размышляя о том, что ты чувствуешь в этот момент.",
            topBtnName: "top",
            middleBtnName: "ХОРОШО",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //3 //второй экран цикла 2 день
            optionBtn: .twoBtn,
            text: "Готово! Пожалуйста, используй каждый перекур как шанс подумать о том, зачем тебе вообще сигареты.",
            topBtnName: "top",
            middleBtnName: "ХОРОШО",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //4 второй экран цикла 3-9 день
            optionBtn: .twoBtn,
            text: "Пожалуйста, не забывай подышать перед тем, как курить. Медленно и глубоко. Три вдоха и три выдоха.",
            topBtnName: "top",
            middleBtnName: "ХОРОШО",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( // 5 второй экран цикла 10 день
            optionBtn: .twoBtn,
            text: "Пожалуйста, первым делом попей водички.",
            topBtnName: "top",
            middleBtnName: "ХОРОШО",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //6 второй экран цикла 11-18 день
            optionBtn: .messageBtn,
            text: "Напиши мне, какую награду ты хочешь получить от курения?",
            topBtnName: "top",
            middleBtnName: "ХОРОШО",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( // 7 второй экран цикла 19 день
            optionBtn: .messageBtn,
            text: "Я понимаю, что сделать этот шаг непросто. Но я рядом и знаю, что у тебя всё получится.",
            topBtnName: "top",
            middleBtnName: "ХОРОШО",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //8
            optionBtn: .oneBtn,
            text: "Отмечено! Не забывай, что курить надо не на автомате, а внимательно, вдумчиво отслеживая процесс.",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ПОНЯЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //9 третий экран перед клавиат
            optionBtn: .messageBtn,
            text: "Напиши мне, в каком контексте ты собираешься курить?",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: false,
            endOfSection: false
        ),
        BotMessage( //10 после клавиатуры 5 день
            optionBtn: .oneBtn,
            text: "Старайся хотя бы полминутки поразмышлять над каждой сигаретой. Включай осознанность!",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ХОРОШО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: false,
            endOfSection: false
        ),
        BotMessage( //11 после клавиатуры 6 день
            optionBtn: .oneBtn,
            text: "Есть! Не забудь, что ты теперь куришь осознанно, отдавая себе полный отчёт в том, что происходит!",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ХОРОШО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //12 после клавиатуры 7 день
            optionBtn: .oneBtn,
            text: "Добавил! Напоминаю, что надо курить не торопясь, размышляя о том, что ты чувствуешь в этот момент.",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ХОРОШО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //13 после клавиатуры 7 день переход на оценку
            optionBtn: .twoBtnEndScores,
            text: "Насколько сильно тебе сейчас хочется курить? Оцени тягу от 1 до 5 (1 — «не хочется, но покурю через силу». 5 — «умру, если сейчас не покурю»)",
            topBtnName: "top",
            middleBtnName: "ОЦЕНИТЬ",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //14 после клавиатуры 7 день
            optionBtn: .oneBtn,
            text: "Записал! Старайся хотя бы полминутки поразмышлять над каждой сигаретой. Включай осознанность!",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ХОРОШО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( // 15 второй экран цикла 9 день
            optionBtn: .messageBtn,
            text: "Напиши мне, какую награду ты хочешь получить от курения?",
            topBtnName: "top",
            middleBtnName: "ХОРОШО",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( // 16 второй экран цикла 9 день
            optionBtn: .oneBtn,
            text: "Хорошо, тогда предлагаю повторить 2-3 раза фразу: «Мне кажется, что мне хочется курить... но на самом деле то, что мне нужно — это отдохнуть».",
            topBtnName: "top",
            middleBtnName: "ХОРОШО",
            buttomBtnName: "ГОТОВО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //17 после после отдохнуть 11 день
            optionBtn: .oneBtn,
            text: "Сохранил! Не забывай, что курить надо не на автомате, а внимательно, вдумчиво отслеживая процесс.",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ХОРОШО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //18 после после отдохнуть 14 день
            optionBtn: .oneBtn,
            text: "Окей! Пожалуйста, используй каждый перекур как шанс подумать о том, зачем тебе вообще сигареты.",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ХОРОШО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //19 после после отдохнуть 14 день
            optionBtn: .twoBtn,
            text: "Теперь выполни подходящую альтернативу. Возможно, ты передумаешь курить, а возможно — нет. И то, и другое нас полностью устраивает. После практики альтернативы возвращайся и сообщи о своём решении.",
            topBtnName: "top",
            middleBtnName: "ПОЙДУ КУРИТЬ",
            buttomBtnName: "НЕ ПОЙДУ КУРИТЬ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //20 после после отдохнуть 14 день
            optionBtn: .oneBtn,
            text: """
                Ничего страшного, мы всё равно продвинулись ещё на шаг вперёд! Можешь идти курить.
                Принято! Напоминаю, что надо курить не торопясь, размышляя о том, что ты чувствуешь в этот момент.
                """,
            topBtnName: "top",
            middleBtnName: "_",
            buttomBtnName: "ХОРОШО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //21 после после отдохнуть 14 день
            optionBtn: .oneBtn,
            text: "Вот и отлично, горжусь тобой!",
            topBtnName: "top",
            middleBtnName: "ПОЙДУ КУРИТЬ",
            buttomBtnName: "ДАЛЕЕ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //22 последний день начало цикла
            optionBtn: .oneBtn,
            text: "Ну что, прощальная церемония состоялась? Хочешь поставить точку и отметить последнюю сигарету?",
            topBtnName: "top",
            middleBtnName: "__",
            buttomBtnName: "ЕЩЕ НЕ ВРЕМЯ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //23 последний день начало цикла
            optionBtn: .twoBtn,
            text: "Я понимаю, что сделать этот шаг непросто. Но я рядом и знаю, что у тебя всё получится.",
            topBtnName: "top",
            middleBtnName: "ДОБАВИТЬ ОДНУ",
            buttomBtnName: "ПЕРЕДУМАЛ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //24 последний день начало цикла
            optionBtn: .oneBtn,
            text: "Наберись смелости и попрощайся с сигаретами. С нетерпением жду этого момента!",
            topBtnName: "top",
            middleBtnName: "__",
            buttomBtnName: "ПОСТАВИТЬ ТОЧКУ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //25 последний день начало цикла
            optionBtn: .oneBtn,
            text: "Поздравляю! Наконец-то этот день настал, а ты просто молодчина! Я верил в тебя с самого начала!",
            topBtnName: "top",
            middleBtnName: "__",
            buttomBtnName: "ДАЛЕЕ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
        BotMessage( //26 последний день начало цикла
            optionBtn: .oneBtn,
            text: "Желаю тебе завтра отлично провести время в поездке! Наслаждайся своей новой жизнью — без сигарет!",
            topBtnName: "top",
            middleBtnName: "__",
            buttomBtnName: "СПАСИБО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: true,
            buttomIsSend: true,
            endOfSection: false
        ),
    ] // Конец цикла +++++++++++++++++++++++
        
    // ++++++++++++++++++++++++++++++++++++++++++++++
    // ++++++++++++++++++++++++++++++++++++++++++++++
    // ++++++++++++++++++++++++++++++++++++++++++++++
    // ++++++++++++++++++++++++++++++++++++++++++++++

    static let closeDay = [
        BotMessage(
            optionBtn: .twoBtn,
            text:
                """
            Закрываем День 1. На данный момент выкурено сигарет: 16.
            
            Всё правильно или мы что-то забыли? Можем добавить сейчас!
            """,
            //            topBtnName: "top",
            middleBtnName: "НЕ ВСË ДОБАВИЛ",
            buttomBtnName: "ВСË ПРАВИЛЬНО",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: false,
            buttomIsSend: true,
            endOfSection: false
//            nextAction: .startNewDay
        ),
        BotMessage(
            optionBtn: .oneBtn,
            text: "Продолжай в том же духе и всё получится!",
            //            topBtnName: "top",
            middleBtnName: "НЕ ВСË ДОБАВИЛ",
            buttomBtnName: "НАЧАТЬ НОВЫЙ ДЕНЬ",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: false,
            buttomIsSend: true,
            endOfSection: false
//            nextAction: .startNewDay
        ),
    ]
    
    
    static let messages_1Day = [
        BotMessage(
            optionBtn: .oneBtn,
            text: "Привет! Меня зовут Helper. Я буду вести дневник твоего курения.",
            buttomBtnName: "ПРИВЕТ, HELPER!",
            nextSection: 0,
            buttomIsSend: true,
            endOfSection: false
            //            currentAction: .next
        ),
        BotMessage(
            optionBtn: .oneBtn,
            text: "Каждый день я буду присылать инструкцию. Будем двигаться от простого к сложному!",
            buttomBtnName: "ДАЛЕЕ",
            nextSection: 0,
            buttomIsSend: false,
            endOfSection: false
            //            currentAction: .next
        ),
        BotMessage(
            optionBtn: .oneBtn,
            text: "Перед тем, как покурить, нажми на кнопку ДОБАВИТЬ СИГАРЕТУ и следуй моим советам.",
            buttomBtnName: "ДАЛЕЕ",
            nextSection: 0,
            buttomIsSend: false,
            endOfSection: false
            //            currentAction: .next
        ),
        BotMessage(
            optionBtn: .oneBtn,
            text: "Когда решишь, что перекуров больше не будет, нажми ЗАКРЫТЬ ДЕНЬ. Я подведу итоги. Договорились?",
            buttomBtnName: "ДА, ВСË ПОНЯТНО",
            nextSection: 0,
            buttomIsSend: false,
            endOfSection: false
            //            currentAction: .next
        ),
        // 1 day
        BotMessage(
            optionBtn: .oneBtn,
            text: "Тогда добро пожаловать на День 1. А вот и инструкция! Сегодня правила предельно простые.",
            buttomBtnName: "ДАЛЕЕ",
            nextSection: 0,
            buttomIsSend: false,
            endOfSection: false
            //            currentAction: .next
        ),
        BotMessage(
            optionBtn: .oneBtn,
            text: "Сигнализируй мне каждый раз перед тем, как пойдёшь курить. Для этого нажми ДОБАВИТЬ СИГАРЕТУ.",
            buttomBtnName: "ДАЛЕЕ",
            nextSection: 0,
            buttomIsSend: false,
            endOfSection: false
            //            currentAction: .next
        ),
        BotMessage(
            optionBtn: .oneBtn,
            text: "Там же найдёшь кнопку ДОБАВИТЬ ЗАБЫТЫЕ на случай, если не отметишь сигарету вовремя.",
            buttomBtnName: "ПОНЯЛ",
            nextSection: 3,
            buttomIsSend: true,
            //            currentAction: .next,
            endOfSection: true
        )
    ]
    
    static let messages_2Day =         // 2 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 2. Сегодня те же правила, что и вчера.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Каждый раз перед тем, как покурить, нажимай на кнопку ДОБАВИТЬ СИГАРЕТУ.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Маленький лайфхак: чтобы не забывать про меня, напиши на бумажке Помощник и положи её в пачку сигарет.",
                buttomBtnName: "ПОНЯЛ",
                nextSection: 3,
                buttomIsSend: true,
                //            currentAction: .next,
                endOfSection: true
            )
            
        ]

    static let messages_3Day =         // 3 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 3! Сегодня задание чуть-чуть усложняется.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Теперь перед каждой сигаретой делай 3 глубоких вдоха и 3 медленных выдоха. Попробуй прямо сейчас!",
                buttomBtnName: "ГОТОВО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Успокаивает, да? В остальном правила не меняются. Не забывай нажимать ДОБАВИТЬ СИГАРЕТУ и дышать.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),

        ]
    
    static let messages_4Day =         // 4 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 4. Будем курить по тем же правилам, что и вчера.",
                buttomBtnName: "ПОНЯЛ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Перед каждой сигаретой нажимаешь ДОБАВИТЬ СИГАРЕТУ, потом глубоко дышишь, потом куришь. Хорошо?",
                buttomBtnName: "ХОРОШО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Старайся дышать осознанно! Не торопись, прочувствуй каждый вдох и выдох, наслаждайся ими.",
                buttomBtnName: "ПОНЯЛ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),
        ]

    static let messages_5Day =         // 5 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 5! Сегодня усложняем практику. Отмечаем сигарету, дышим, а ещё...",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Обозначаем контекст (обстановку), в котором ты будешь курить: где находишься и что происходит.",
                buttomBtnName: "ХОРОШО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),
        ]
    
    static let messages_6Day =         // 6 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Ура, суббота, и это День 6! Правила прежние. Дышим, отмечаем контекст, а только потом идём курить.",
                buttomBtnName: "ХОРОШО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),

        ]
    
    static let messages_7Day =         // 7 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 7. И с этого момента ты больше не куришь!",
                buttomBtnName: "НЕОЖИДАННО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Шучу! 😂 Всему своё время. А сегодня курим, как вчера. Дышим и отмечаем контекст. Хорошего дня!",
                buttomBtnName: "ХОРОШО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),
        ]
    
    static let messages_8Day =         // 8 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 8! Сегодня правила снова немного усложняются (совсем чуть-чуть, правда).",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text:
                    """
                Теперь тебе, в дополнение к дыханию и контексту, предстоит каждый раз оценивать своё желание курить.
                
                В общем, ничего страшного. Как и раньше, шаг за шагом двигаемся с тобой к нашей заветной цели!
                """,
                buttomBtnName: "ПОНЯЛ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),
        ]
    
    static let messages_9Day =         // 9 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Обращай особое внимание на моменты, когда оцениваешь тягу в 1 балл. Зачем курить, если не хочется?",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),
            
        ]
    
    static let messages_10Day =         // 10 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 10. Сейчас я расскажу о двух небольших изменениях в правилах.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: """
                Теперь перед каждой сигаретой вместо глубоких вдохов делай 3 глотка воды. Попей прямо сейчас!
                
                А ещё вместо контекста теперь надо писать награду, которую ты рассчитываешь получить от сигареты.
                """,
                buttomBtnName: "ХОЧУ ПРИМЕРЫ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Например, отвлечься, отдохнуть, уединиться, поставить точку в работе, успокоиться, обдумать идею.",
                buttomBtnName: "ПОНЯТНО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),
        ]
    
    static let messages_11Day =         // 11 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Здравствуй! Добро пожаловать на День 11. Сегодня правила остаются без изменений.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Маленький лайфхак: поставь на рабочий стол графин с водой. Тогда точно не забудешь попить!",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),
        ]
    
    static let messages_12Day =         // 12 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 12! Больше не будем оценивать силу тяги. Просто пей воду и пиши награды.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "И ещё кое-что! Постарайся с этого дня курить вне привычного контекста. Хотя бы не там, где обычно.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "И каждый раз уделяй внимание процессу курения. Не забывай, что твоя задача — повышать осознанность.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),
        ]
    
    static let messages_13Day =         // 13 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 13. Правила те же. Продолжаем изучать награды, которые дают тебе сигареты.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: """
                Суббота — самый подходящий день, чтобы уделить время занятиям, которые приносят тебе радость!
                
                Я, как ты понимаешь, всего лишь программа и не умею радоваться, так что отдохни за нас двоих.
                """,
                buttomBtnName: "ХОРОШО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),
        ]
    
    static let messages_14Day =         // 14 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Привет, сегодня День 14! Половина программы позади, до начала свободной жизни осталось чуть-чуть!",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Как поживает твоя решимость? Крепчает? Если нет, то советую налегать на изучение допматериалов.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Напоминаю, что надо курить не торопясь, осознанно. И желательно — вне привычного контекста!",
                buttomBtnName: "ХОРОШО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),

        ]
    
    static let messages_15Day =         // 15 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Наступил День 15! Сегодня отмечаться нужно, как вчера, но не торопись курить сразу после этого.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: """
                Сначала попробуй получить награду, на которую ты рассчитываешь, с помощью альтернативного действия.
                
                Если желание курить не пройдёт, то отправляйся на перекур! А если пройдёт — поздравь себя с победой!
                """,
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),
        ]
    
    static let messages_16Day =         // 16 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Встречай День 16! Попробуй хотя бы раз в качестве альтернативы курению сделать осознанную паузу.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Как только поднимется волна желания, просто жди и внимательно следи за ощущениями. Волна отступит!",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),
        ]
    
    static let messages_17Day =         // 17 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 17. Надеюсь, поездка на выходные уже запланирована?",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Если нет, то у тебя ещё есть время, чтобы организовать что-то приятное! Нужно сменить контекст.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),
        ]
    
    static let messages_18Day =         // 18 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Привет! Сегодня уже День 18. Быстро время летит, да?",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Не забудь сегодня устроить «тотальное наслаждение» сигаретой. Покури, как в последний раз!",
                buttomBtnName: "ХОРОШО",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: true,
                endOfSection: true
            ),
        ]
    
    static let messages_19Day =         // 19 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Добро пожаловать на День 19! Чувствуешь? Ты уже стоишь на пороге свободной жизни!",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Сегодня у тебя очень простая задача: сообщить мне про одну-единственную, самую последнюю сигарету.",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "Надеюсь, ты не заставишь меня ждать до самого вечера? Смелее, всё получится!",
                buttomBtnName: "ДАЛЕЕ",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),
        ]
    
    static let messages_20Day =         // 20 day
        [
            BotMessage(
                optionBtn: .oneBtn,
                text: "Сегодня 0 день твоей новой жизни! Чем я могу тебе помочь?",
                buttomBtnName: "buttom",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: false
            ),
            BotMessage(
                optionBtn: .oneBtn,
                text: "День 20",
                buttomBtnName: "buttom",
                nextSection: 0,
                topIsSend: false,
                middleIsSend: false,
                buttomIsSend: false,
                endOfSection: true
            ),
        ]
    
    
    
    static let messagesAllDays = [

        messages_1Day, //0
        messages_2Day, //1
        messages_3Day, //2
        messages_4Day, //3
        messages_5Day, //4
        messages_6Day, //5
        messages_7Day, //6
        messages_8Day, //7
        messages_9Day, //8
        messages_10Day, //9
        messages_11Day, //10
        messages_12Day, //11
        messages_13Day, //12
        messages_14Day, //13
        messages_15Day, //14
        messages_16Day, //15
        messages_17Day, //16
        messages_18Day, //17
        messages_19Day, //18
        messages_20Day, //19

        startOfChoose, //3 20
        addSigarette, //4 21
        closeDay, //5 22
    ]
    
    

    
    static let sourse = [ //образец
        BotMessage(
            optionBtn: .oneBtn,
            text: "Образец",
            topBtnName: "top",
            middleBtnName: "Middle",
            buttomBtnName: "buttom",
            nextSection: 0,
            topIsSend: false,
            middleIsSend: false,
            buttomIsSend: false,
            endOfSection: true
        ),
    ]
}
