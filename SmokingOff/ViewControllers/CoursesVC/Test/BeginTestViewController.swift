//
//  BeginTestViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 17.05.2021.
//

import UIKit

class BeginTestViewController: UIViewController {

    @IBOutlet weak var labelTestNumber: UILabel!
    var test: [Question]!
    var currentTestNumber: Int!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        sutupView()
        
    }

    func sutupView() {
        labelTestNumber.text = "1"//"\( currentTestNumber! + 1)"
        title = ""
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTest" {
            let vc = segue.destination as! TestViewController
            vc.currentTestNumber = currentTestNumber
            vc.test = test
        }
    }
}
