//
//  LessonViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 16.06.2021.
//

import UIKit
import SpriteKit
import GameplayKit


class LessonViewController: UIViewController {
    @IBOutlet weak var triggerButton: UIButton!
    @IBOutlet weak var menImage: UIImageView!
    @IBOutlet weak var triggerView: RoundAboutButton!
    @IBOutlet weak var descriptionView: LessonDescriptionView!
    @IBOutlet weak var setTriggerView: SetTriggerView!
    @IBOutlet weak var chooseTrigerVeiw: ChooseTriger!
    @IBOutlet weak var presentationTrigger: PresentationTrigger!
    @IBOutlet weak var discomfortView: DiscomfortView!
    @IBOutlet weak var withTime: WithTime!
    @IBOutlet weak var constraint: NSLayoutConstraint!
    
    
//    "Today we will continue to study the physiology of addiction and talk about dopamine, which is called the \"hormone of happiness.\" You will find out why some habits quickly build in, while others constantly fly off. ",
    
    var numberOfPart = 1
    let textArray = [

                     "Когда человек решает покурить, он обычно делает это после или во время какого-то события. Такое событие мы будем называть триггером.",
                     // ===
                     "Поссорились с коллегой.",
                     "Закончилось совещание.",
                     "Сели в машину или ждёте свой заказ в кафе.",
                     "Закончили работу над важной задачей.",
                     "Досмотрели кино.",
                     "Встретили или проводили гостей.",
                     "Отправились в магазин за продуктами или выкидывать мусор.",
                     "Встретили старинного приятеля на улице",
                     "",
                     "Вышли на балкон у себя дома.",
                     "Уложили детей спать.",
                     "Получили выговор от начальника.",
                     "Пришли на автобусную остановку.",
                     "Выбивали деньги из должника.",
                     "Триггером может быть не только внешнее, но и внутреннее событие. Например, вам стало скучно, или вы перенервничали, или устали.",
                     "Укажите топ-5 ваших триггеров, когда Вам больше всего хочется курить.",
                     
                     "Триггер — это событие, которое меняет ваше состояние, то есть нарушает “равновесие” организма.",
                     
                     "Для этого мозг заставляет вас совершать действие, которое уже помогало вам раньше в подобной ситуации. И вы достаёте из пачки сигарету.",
                     
                     "Дополнительно к этому добавляется \"никотиновый голод\" и сам по себе никотиновый голод является триггером, потому что он тоже меняет ваше состояние.",
                     
                     "Триггер — это событие, которое меняет ваше состояние, то есть нарушает “равновесие” организма.",
                     "Триггер — это событие, которое меняет ваше состояние, то есть нарушает “равновесие” организма.",
                     "Триггер — это событие, которое меняет ваше состояние, то есть нарушает “равновесие” организма.",
                     "Триггер — это событие, которое меняет ваше состояние, то есть нарушает “равновесие” организма.",
                     "Триггер — это событие, которое меняет ваше состояние, то есть нарушает “равновесие” организма.",
                     "Триггер — это событие, которое меняет ваше состояние, то есть нарушает “равновесие” организма.",
                     "Триггер — это событие, которое меняет ваше состояние, то есть нарушает “равновесие” организма.",
                     "Со временем привычка курить по малейшему поводу закрепляется. Сигарета начинает ассоциироваться с самыми разными жизненными моментами. Так, вы думаете, что она усиливает приятные ощущения, например, от еды, общения или отдыха.",
                     "Со временем привычка курить по малейшему поводу закрепляется. Сигарета начинает ассоциироваться с самыми разными жизненными моментами. Так, вы думаете, что она усиливает приятные ощущения, например, от еды, общения или отдыха.",
                     "Со временем привычка курить по малейшему поводу закрепляется. Сигарета начинает ассоциироваться с самыми разными жизненными моментами. Так, вы думаете, что она усиливает приятные ощущения, например, от еды, общения или отдыха.",
                     "Со временем привычка курить по малейшему поводу закрепляется. Сигарета начинает ассоциироваться с самыми разными жизненными моментами. Так, вы думаете, что она усиливает приятные ощущения, например, от еды, общения или отдыха.",
                     "Ваше состояние нормализуется.  Мозг запоминает связанное с наградой действие.",
                     """
Если схема начинается с триггера, то почему бы просто не убрать их все?

Убрать абсолютно все триггеры раз и навсегда не получится.

Даже если старые триггеры исчезли из жизни, мозг не знает, как действовать, если внутреннее состояние вдруг опять изменится.
""",
"""
Будем прорабатывать привычку полностью. Не только триггеры, но и все остальные элементы алгоритма: состояние, действие и награду.
                     
Если не проработать остальные элементы алгоритма, то курение вернётся, как только вы окажетесь в привычном контексте.

""",
                     "6",
                     
                     "9"]
    var textDescription: String!
    var titleDescription: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Appearance.navigationClear(vc: self)

        loadTriggerRoundAbout()
        
        setTriggerView.isHidden = true
        chooseTrigerVeiw.isHidden = true
        presentationTrigger.isHidden = true
        discomfortView.isHidden = true
        withTime.isHidden = true
        
        
        tabBarController?.tabBar.isHidden = true

        tapNext()
        countPressed()
//        setupSK()
    }
    
    func loadTriggerRoundAbout() {
        triggerView.createButton(runAnim: true)
        textDescription = textArray[0]
        descriptionView.textLabel.text = textDescription
    }
    
//    func setupSK() {
//        if let view = self.view as! SKView? {
//
//            let scene = GameScene(size: self.view.bounds.size)
//            scene.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
//            scene.scaleMode = .aspectFill
//            scene.openQuestion = { [self] in
//
//            }
//
//
//            // Present the scene
//            view.presentScene(scene)
//            view.ignoresSiblingOrder = true
//            view.showsFPS = true
//            view.showsNodeCount = true
//        }
//    }
    
    func countPressed() {
        print("presses")
        chooseTrigerVeiw.countButton = {
            self.descriptionView.enableButton()
        }
    }
  
    // actions
    func tapNext() {

        descriptionView.tapButton = { [self] in
            
            textDescription = textArray[numberOfPart - 1]
            descriptionView.textLabel.text = textDescription
            
            switch numberOfPart {
            case 1:
                self.triggerView.stopButtonAbout()
                descriptionView.title.text = "Все начинается с тригера"
            case 2:
                triggerView.isHidden = true
                menImage.isHidden = true
                setTriggerView.isHidden = false
                setTriggerView.createButton()
                descriptionView.title.text = "Примеры тригеров"
            case 3:
                setTriggerView.createButton()
            case 4:
                setTriggerView.createButton()
            case 5:
                setTriggerView.createButton()
            case 6:
                setTriggerView.createButton()
            case 7:
                setTriggerView.createButton()
            case 8:
                setTriggerView.createButton()
            case 9:
                setTriggerView.createButton()
            case 10:
                setTriggerView.createButton()
            case 11:
                setTriggerView.createButton()
            case 12:
                setTriggerView.createButton()
            case 13:
                setTriggerView.createButton()
            case 14:
                setTriggerView.createButton()
            case 15:
                setTriggerView.createButton()
            case 16:
                descriptionView.title.text = "Внешние и внутренние триггеры"
                setTriggerView.isHidden = true
                presentationTrigger.isHidden = false
            case 17:
                descriptionView.title.text = "Топ-10 триггеров для курения"
                presentationTrigger.isHidden = true
                chooseTrigerVeiw.isHidden = false
                descriptionView.disableButton()
            case 18:
                descriptionView.title.text = "Вы курите не из-за триггера"
                
                chooseTrigerVeiw.isHidden = true
                discomfortView.isHidden = false
                descriptionView.disableButton()
                discomfortView.changeContent = { // df
                    numberOfPart += 1
                    if numberOfPart != 22 {
                        print(numberOfPart)
                        textDescription = textArray[numberOfPart - 1]
                        descriptionView.textLabel.text = textDescription
                    }
                    if numberOfPart == 27{
                        descriptionView.enableButton()
                        numberOfPart += 1
                    }
                }
            case 28:
                descriptionView.disableButton()
                descriptionView.title.text = "Со временем"
                discomfortView.isHidden = true
                withTime.isHidden = false
                withTime.tapInWithTime = {
                    numberOfPart += 1
                    print("tap", numberOfPart)
                    if numberOfPart == 32 {
                        descriptionView.enableButton()
                        withTime.nextView()
                        descriptionView.title.text = "Петля привычки"
                        textDescription = textArray[numberOfPart - 1]
                        descriptionView.textLabel.text = textDescription
                    }
                }
            case 32:
                numberOfPart += 1
                constraint.constant = -188
                withTime.isHidden = true
                descriptionView.title.text = "Что если убрать все триггеры?"
                textDescription = textArray[numberOfPart - 1]
                descriptionView.textLabel.text = textDescription

                print("32", numberOfPart)
            case 33:
                print("tap33", numberOfPart)
            case 34:
                print("tap34", numberOfPart)
            case 35:
                print("tap35", numberOfPart)
                navigationController?.popViewController(animated: true)
                
                
            default:
                break
            }
            numberOfPart += 1
        }
    }
}
