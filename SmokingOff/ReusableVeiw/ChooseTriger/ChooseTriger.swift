//
//  ChooseTriger.swift
//  SmokingOff
//
//  Created by DfStudio on 21.06.2021.
//

import UIKit

class ChooseTriger: UIView {


    //    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var btn8: UIButton!
    @IBOutlet weak var btn9: UIButton!

    //    @IBOutlet weak var textLabel: UILabel!

        var namesRedButton = [ "RoundButton1Red", "RoundButton2Red", "RoundButton0Red",  "RoundButton8Red",  "RoundButton7Red", "RoundButton9Red", "RoundButton4Red", "RoundButton5Red", "RoundButton13Red"]
        
        var countButton: (() -> ())?
        var button = UIButton()
        let size = CGSize(width: 60, height: 60)
        var view: UIView!
        var nibName: String = "ChooseTriger"
        
    
        var c: Int = 0
        var countOfChoose = 0
         
        override init(frame: CGRect) {
            super.init(frame: frame)
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            setup()
        }
        
        func loadFromNib() -> UIView {
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: nibName, bundle: bundle)
            let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
            return view
        }

        func setup() {
            view = loadFromNib()
            view.frame = bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.layer.cornerRadius = 10
            view.clipsToBounds = false
            shadow(view: view)
            addSubview(view)
            


        }
        
        func shadow(view: UIView) {
                view.layer.shadowColor = UIColor.black.cgColor
                view.layer.shadowOpacity = 0.1
                view.layer.shadowOffset = CGSize(width: 0, height: 6)
                view.layer.shadowRadius = 8
        }
        
        
        /*
        func createButton() {
    //        namesButton = namesButton.shuffled()
            let x: CGFloat = view.frame.width / 2 - 30
            let y: CGFloat = view.frame.height / 2 - 45

            let point = CGPoint(x: x, y: y)
            button = UIButton(frame: CGRect(origin: point, size: size))
            button.setImage(UIImage(named: namesButton[Int(c)]), for: .normal)
            self.view.addSubview(button)

            
            
//            UIView.animate(withDuration: 0) { [self] in
//                button.transform = CGAffineTransform(scaleX: 0, y: 0)
//            } completion: {_ in
//                UIView.animate(withDuration: 2) { [self] in
//                    button.transform = CGAffineTransform(scaleX: 2, y: 2)
//                } completion: {_ in
//                    UIView.animate(withDuration: 1) { [self] in
//                        button.transform = CGAffineTransform(a: 1, b: 0.0, c: 0.0, d: 1, tx: toX, ty: toY)
//                    } completion: {_ in
//                        print("askjd;akl f")
//                    }
//                }
//            }
        }
        */
    

        //actions
    func nameForButton(number: Int) -> String{
        let name = namesRedButton[number - 1]
        return name
    }
    
    func countTap(button: UIButton, number: Int) {
        button.setImage(UIImage(named: nameForButton(number: number)), for: .normal)
        countOfChoose += 1
        
        if countOfChoose == 5 {
            countButton?()
        }
    }
    
    @IBAction func action1(_ sender: Any) {
        countTap(button: btn1, number: 1)
    }
    @IBAction func action2(_ sender: Any) {
        countTap(button: btn2, number: 2)
    }
    @IBAction func action3(_ sender: Any) {
        countTap(button: btn3, number: 3)
    }
    @IBAction func action4(_ sender: Any) {
        countTap(button: btn4, number: 4)
    }
    @IBAction func action5(_ sender: Any) {
        countTap(button: btn5, number: 5)
    }
    @IBAction func action6(_ sender: Any) {
        countTap(button: btn6, number: 6)
    }
    @IBAction func action7(_ sender: Any) {
        countTap(button: btn7, number: 7)
    }
    @IBAction func action8(_ sender: Any) {
        countTap(button: btn8, number: 8)
    }
    @IBAction func action9(_ sender: Any) {
        countTap(button: btn9, number: 9)
    }
    

}
