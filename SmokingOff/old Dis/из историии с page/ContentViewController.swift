//
//  ContentViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 06.04.2021.
//

import UIKit

class ContentViewController: UIViewController {
    
    @IBOutlet weak var nameLessonLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var header = ""
    var textOfPage = ""
    var imageFile = ""
    var social = ""
    var index = 0
     
    @IBAction func pageButtonPressed(_ sender: UIButton) {
        switch index {
        case 0:
            let pageVC = parent as! PageWeekViewController //чтоб вызвать следующий VC надо добраться до PVC
            pageVC.nextVC(atIndex: index)
        case 1:
            let pageVC = parent as! PageWeekViewController //чтоб вызвать следующий VC надо добраться до PVC
            pageVC.nextVC(atIndex: index)
        case 2:
            let pageVC = parent as! PageWeekViewController //чтоб вызвать следующий VC надо добраться до PVC
            pageVC.nextVC(atIndex: index)
        case 3:
            let userDefaults = UserDefaults.standard
            userDefaults.set(true, forKey: "isWatchedInstruction")
            userDefaults.synchronize()
            
            dismiss(animated: true, completion: nil)
        default: break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        nameLessonLabel.text = header
        imageView.image = UIImage(named: imageFile)
        pageControl.numberOfPages = 4
        pageControl.currentPage = index
        
        
    }
    
}
