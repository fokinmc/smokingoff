//
//  CoursesViewController.swift
//  SmokingOff
//
//  Created by DfStudio on 13.04.2021.
//

import UIKit

class CoursesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var table: UITableView!
    
    var manager = CreatingManager()
    var nameJSONWeeks: String!
    var weeks: [Week]!

    let useDef = UserDefManager.shared
    let weekStatus = WeekStatus.self
    let weekMult = ["weekOfPreparation", "positiveWeek", "awarenessWeek", "newLife"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        Appearance.navigationDecor(vc: self)
        Appearance.navigationClear(vc: self)
        table.separatorStyle = .none
        table.clipsToBounds = false
//        title = ""
        tabBarController?.tabBar.isHidden = false
        
//        UITabBar.appearance().shadowImage = UIImage()
//        tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
//        tabBarController?.tabBar.layer.shadowRadius = 0
//        tabBarController?.tabBar.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
//        tabBarController?.tabBar.layer.shadowOpacity = 0
        
        
        if useDef.getStage() == 3 {
            self.tabBarController?.selectedIndex = 1
        }
        
        //вернуть проверку приветствия
        if !useDef.getGreetingStatus() {
            let greetingVC = storyboard?.instantiateViewController(identifier: "nameVC")
            navigationController?.pushViewController(greetingVC!, animated: true)
        }
        
        let locale = NSLocale.preferredLanguages.first!
        print(locale)
        if locale.hasPrefix("ru") { //может сделать сюда не выбор по local а оставить только en и перереводить как стандартный текст !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           nameJSONWeeks = "WeeksRU"
        } else {
           nameJSONWeeks = "WeeksEN"
            nameJSONWeeks = "WeeksRU"

        }
        
        manager.getWeeks(file: nameJSONWeeks) { [self] result in
            weeks = result
            table.reloadData()
        }
        
//        table.selectRow(at: IndexPath(row: 1, section: 0), animated: false, scrollPosition: .none)
//        table.selectionFollowsFocus = true
        
//        table.allowsSelection = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)

    }
    
    
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "coursesCell", for: indexPath) as! CoursesTableViewCell
        
        if weeks != nil {
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 0.75

            cell.weekLabel.attributedText = NSMutableAttributedString(string: weeks[indexPath.row].weekName, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            
//            cell.weekLabel.text = weeks[indexPath.row].weekName
        }
//        cell.configure(status: 2, multName: weekMult[indexPath.row], indexPath: indexPath, progress: 0.5)
        cell.configure(status: indexPath.row, multName: weekMult[indexPath.row], indexPath: indexPath, progress: 0.5)
        //поменять логику присванивания status
        
        
            // хранить статус в юзер деф по умолчанию будет 0 для желтых текущий и пройденный можно присваивать
        
//        if indexPath.row == 3 {
//            cell.layer.zPosition = 10
//        }
        
        return cell

    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let vc = storyboard?.instantiateViewController(identifier: "LessonsViewController") as? LessonsViewController else { return }
        if indexPath.row == 0 { // блокировка других недель
            navigationController?.pushViewController(vc, animated: true)
            vc.weekName = weeks[indexPath.row].weekName
            vc.weekNumber = indexPath.row
            vc.lessons = weeks[indexPath.row].lessons
        }
        
        /* для выбра разных старт экранов
        
        switch indexPath.row {
        case 0:
            guard let vc = storyboard?.instantiateViewController(identifier: "startLesson_1_day") else { return }
            navigationController?.pushViewController(vc, animated: true)
        case 1:
            guard let vc = storyboard?.instantiateViewController(identifier: "StartLesson") else { return }
            navigationController?.pushViewController(vc, animated: true)
        case 2:
            break
        case 3:
            break
        default:
            break
        }
        */
        
//        if indexPath.row == 0 {
//            guard let vc = storyboard?.instantiateViewController(identifier: "StartLesson") else { return }
//            navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if segue.identifier == "toLesson" {
//            guard let index = table.indexPathForSelectedRow else { return }
//            let vc = segue.destination as! LessonsViewController
//            vc.weekName = weeks[index.row].weekName
//            vc.weekNumber = index.row
//            vc.lessons = weeks[index.row].lessons
//        }
//    }
    
        
}
