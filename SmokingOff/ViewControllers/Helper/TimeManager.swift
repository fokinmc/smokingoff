//
//  File.swift
//  SmokingOff
//
//  Created by DfStudio on 11.08.2021.
//

import Foundation

class TimeMeneger {
    let useDef = UserDefManager.shared

    
    let calendar = Calendar.current
    
    var elapsedDay: Int!
    var elapsedHour: Int!
    var elapsedMinute: Int!
    var elapsedSecond: Int!
    
    var dateStart: Date!
    let dateNow = Date()

    func useCalendar() {

        var dateStartComponent = DateComponents()
        dateStartComponent.year = 2021
        dateStartComponent.month = 6
        dateStartComponent.day = 1
        dateStartComponent.hour = 0
        dateStartComponent.minute = 0
        dateStartComponent.second = 0
        dateStart = calendar.date(from: dateStartComponent)!
        

        let dateComponentNow = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateNow)
//        let dateComponentNow_ = calendar.dateComponents([.year, .month, .day], from: dateNow)

        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "EEEE" // полное название дня недели
        dateFormatter.dateFormat = "yyyy-MM-dd 'at' HH:mm" // 2020-03-22 at 13:55

        let weekDay = dateFormatter.string(from: dateNow)
        
        let dateNow_ = calendar.date(from: dateComponentNow)
        let timeInterval = dateNow.timeIntervalSince(dateNow_!) / 60 / 60  // интервал между датами в часах
        
        let appTimeInterval = dateNow - dateStart
        
        useDef.saveStartDateOfSmoking(date: appTimeInterval.stringFromTimeInterval())
    }
    
    func getElapsedTime(time: Double) -> [Int] {
        var timeUse = NSInteger(time)
        
        let days = (timeUse / 86400) % 86400
        timeUse = timeUse - days * 86400
        let hours = (timeUse / 3600) % 3600
        let minutes = (timeUse / 60) % 60
        let seconds = timeUse % 60
        
        return [days, hours, minutes, seconds]
    }
}

extension Date { // позволяет вычитать и складывать даты
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
}

extension TimeInterval {
    func stringFromTimeInterval() -> String {
        
        var time = NSInteger(self)
        
        let days = (time / 86400) % 86400
        time = time - days * 86400
        let hours = (time / 3600) % 3600
        let minutes = (time / 60) % 60
        let seconds = time % 60
        
        
//        print(minutes, "minutes")
//        print(days, "days")
//        print(hours, "hours")
//        print(time, "time")
        
        
        return String(format: "%0.2d day & %0.2d:%0.2d:%0.2d",days,hours,minutes,seconds)
    }
}



extension TimeMeneger {
    
    func unixTimeConvertion(unixTime: Double) -> String {
        
        let time = NSDate(timeIntervalSince1970: unixTime)
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = NSTimeZone(name: timeZoneInfo)
        dateFormatter.locale = NSLocale(localeIdentifier: NSLocale.system.identifier) as Locale
        dateFormatter.dateFormat = "hh:mm a"
        let dateAsString = dateFormatter.string(from: time as Date)
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "HH:mm"
        let date24 = dateFormatter.string(from: date!)
        
        return date24
    }
    
    func forDate1() {
        let now = Date()
        let startDay = Calendar.current.date(byAdding: .day, value: -63, to: now)!
        
        let seconds = now - startDay
        let month = now.offset(from: startDay) // прошло месяцев
        

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.doesRelativeDateFormatting = true
        
        let nowString = dateFormatter.string(from: now)
        let yesterdayString = dateFormatter.string(from: startDay)
        
        let unixTimeConvertion = unixTimeConvertion(unixTime: seconds)
        

    }
    
    func date2() {
        
        let dateComponentsFormatter = DateComponentsFormatter()
        dateComponentsFormatter.allowedUnits = [.second, .minute, .hour, .day, .weekOfMonth, .month, .year]
        dateComponentsFormatter.maximumUnitCount = 1
        dateComponentsFormatter.unitsStyle = .full
        dateComponentsFormatter.string(from: Date(), to: Date(timeIntervalSinceNow: 4000000))  // "1 month"

        let date1 = DateComponents(calendar: .current, year: 2014, month: 11, day: 28, hour: 5, minute: 9).date!
        let date2 = DateComponents(calendar: .current, year: 2015, month: 8, day: 28, hour: 5, minute: 9).date!

        
        let years = date2.years(from: date1)     // 0
        let months = date2.months(from: date1)   // 9
        let weeks = date2.weeks(from: date1)     // 39
        let days = date2.days(from: date1)       // 273
        let hours = date2.hours(from: date1)     // 6,553
        let minutes = date2.minutes(from: date1) // 393,180
        let seconds = date2.seconds(from: date1) // 23,590,800

        let timeOffset = date2.offset(from: date1) // "9M"

        let date3 = DateComponents(calendar: .current, year: 2014, month: 11, day: 28, hour: 5, minute: 9).date!
        let date4 = DateComponents(calendar: .current, year: 2015, month: 11, day: 28, hour: 5, minute: 9).date!

        let timeOffset2 = date4.offset(from: date3) // "1y"

        let date5 = DateComponents(calendar: .current, year: 2017, month: 4, day: 28).date!
        let now = Date()
        let timeOffset3 = now.offset(from: date5) // "1w"
        
        
    }
}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}
