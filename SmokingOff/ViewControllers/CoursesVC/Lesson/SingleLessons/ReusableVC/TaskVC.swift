//
//  TaskVC.swift
//  SmokingOff
//
//  Created by DfStudio on 17.08.2021.
//

import UIKit

class TaskVC: UIViewController {

    @IBOutlet weak var textLessonButton: UIButton!
    @IBOutlet weak var HTMLTextView: UITextView!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var taskTextLabel: UILabel!

    var lesson: Lesson!
    
    let htmlText = "<!DOCTYPE html><html><body><p style=\"text-align: center;\">Статья</p><p>Данная документация является переводом официальной книги \"Swift Programming Language\" от Apple. Работа была проделана</p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><ol><li>профессионалами, но в связи с огромным объемом текста и постоянным обновлением книги, мы не исключаем незначительных ошибок. Если вы вдруг обнаружили то, что на ваш взгляд нуждается в корректировке, то напишите нам об этом либо в комментариях, либо на почту и мы</li><li>обязательно примем это во <b>bold</b>. Кроме того, мы стараемся постоянно обновлять документацию, как только выходят официальные правки английского варианта книги. В конечном итоге, нашей целью является создание качественного и постоянно обновляемого источника информации о языке Swift.</li></ol><ol style=\"list-style-type: lower-alpha;\"><li>Мы хотим помочь начинающим разработчикам, сделать уверенные шаги на пути изучения Swift. Мы считаем, что языковой барьер не должен быть препятствием для изучения. Мы искренне верим, что такой мощный, быстрый и современный язык программирования как Swift, должен быть доступен каждому.</li><li>Если вам нужно быстро пройтись по возможностям языка, то начните с главы \"Знакомство со Swift\". Если же требуется углубиться в Swift, пропустите главу \"Знакомство со Swift\" и сразу начинайте читать с главы \"Основы\".</li></ol></body></html>"
    
//    let htmlText = """
//        <!DOCTYPE html>
//        <html>
//        <body>
//
//        <p style="text-align: center;">Статья  </p>
//        <p>Данная документация является переводом официальной книги "Swift Programming Language" от Apple. Работа была проделана</p>
//        <p></p>
//        <p></p>
//        <p></p>
//        <p></p>
//        <p></p>
//        <p></p>
//        <p></p>
//        <ol>
//        <li>профессионалами, но в связи с огромным объемом текста и постоянным обновлением книги, мы не исключаем незначительных ошибок. Если вы вдруг обнаружили то, что на ваш взгляд нуждается в корректировке, то напишите нам об этом либо в комментариях, либо на почту и мы</li>
//        <li>обязательно примем это во <b>bold</b>. Кроме того, мы стараемся постоянно обновлять документацию, как только выходят официальные правки английского варианта книги. В конечном итоге, нашей целью является создание качественного и постоянно обновляемого источника информации о языке Swift.</li>
//        </ol>
//        <ol style="list-style-type: lower-alpha;">
//        <li>Мы хотим помочь начинающим разработчикам, сделать уверенные шаги на пути изучения Swift. Мы считаем, что языковой барьер не должен быть препятствием для изучения. Мы искренне верим, что такой мощный, быстрый и современный язык программирования как Swift, должен быть доступен каждому.</li>
//        <li>Если вам нужно быстро пройтись по возможностям языка, то начните с главы "Знакомство со Swift". Если же требуется углубиться в Swift, пропустите главу "Знакомство со Swift" и сразу начинайте читать с главы "Основы".</li>
//        </ol>
//
//        </body>
//        </html>
//        """
    let hhh = """
  
          <!DOCTYPE html>
          <html>
          <body>
  
       <h1 style="text-align: center;">&nbsp;<span style="text-decoration: underline;">Статья</span></h1>
       <h3 style="text-align: center; color: #3f7320;"><span style="border-bottom: 4px solid #c82828;">Нажмите здесь </span> чтобы отредактировать этот текст или вставьте сюда ваш документ, чтобы преобразовать его в HTML 😁</h3>
       <!-- Этот комментарий виден только в редакторе исходного кода -->
       <p><br /><strong>Это демо позволяет протестировать возможности этого редактора. Введите текст в одном из полей и увидьте, как другое меняется в реальном времени! </strong></p>
       <p><strong>Настройте параметры очистки и нажмите <span class="redButton">▼ Очистить </span></strong></p>
       <p>Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из ре</p>
       <ol>
       <li>ном времени:Р</li>
       <li>ном времени:Р</li>
       <li>ном времени:Р</li>
       <li>ном времени:Р</li>
       <li>ном времени:Р</li>
       <li>ном времени:Р</li>
       </ol>
       <p></p>
       <p>дакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из редакторов и ув</p>
       <p><strong>из</strong></p>
       <p>идьте, как другой меняется в реальном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном</p>
       <p>времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном</p>
       <p><strong>из</strong></p>
       <p>времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:РаботайРаботайте с любым из редакторов и увидьте, как другой меняется в реаль</p>
       <p></p>
       <p><strong>из</strong></p>
       <p>ном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:те с любым из редакторов и увидьте, как другой меняется в реальном времени:Работайте с любым из редакторов и увидьте, как другой меняется в реальном времени:</p>
       <table class="demoTable" style="height: 54px;">
       <thead>
       <tr>
       <td><span style="color: #c82828;">Налево </span>: Предварительный просмотр</td>
       <td><span style="color: #c82828;">Направо </span>: Исходный код</td>
       </tr>
       </thead>
       <tbody>
       <tr>
       <td>Посмотреть, как ваш документ будет выглядеть после публикации.</td>
       <td>Настроить HTML-код с выделенным синтаксисом.</td>
       </tr>
       </tbody>
       </table>
       <p>&nbsp;</p>
       <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Завершить <a target="_blank" rel="nofollow noopener" href="https://pranx.com/">Pranx.com</a> для хорошей онлайн шалости.</p>
      
        </body>
        </html>

  """
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textLessonButton.layer.borderWidth = 1
        textLessonButton.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        taskLabel.text = lesson.task
        taskTextLabel.text = lesson.taskDescription
        
        HTMLTextView.isHidden = true
    }
    
    
    func openTextLesson() {
        
//        let textView = UITextView()
//        textView.frame = self.view.frame
        
//        HTMLTextView.text = htmlText.htmlToString // похже это не учитывается
//        HTMLTextView.font = UIFont(name: "Avenir Next Demi Bold", size: 20)
        
        var htmlContent = hhh//lesson.html
        let ddd = htmlContent.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        let attrStr = try! NSAttributedString(
                    data: htmlContent.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        
        HTMLTextView.attributedText = attrStr
        HTMLTextView.isHidden = false

//        textView.text = htmlText.htmlToString

//        self.view .addSubview(textView)
    }
    
    
//    func rrr() {
//        let htmlString = htmlText//"This is a <b>bold</b> text."
//        let data = htmlString.data(using: .utf8)!
//
//        let attributedString = try? NSAttributedString(
//            data: data,
//            options: [.documentType: NSAttributedString.DocumentType.html],
//            documentAttributes: nil)
//
//        textView.attributedText = attributedString
//
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFinishLesson" {
            let vc = segue.destination as! FinishLessonVC
            vc.lessonId = lesson.id
        }
    }
    
    @IBAction func openHTML(_ sender: Any) {
        openTextLesson()
//        rrr()
    }

}
